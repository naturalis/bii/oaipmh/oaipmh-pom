SELECT * FROM(
	SELECT a.id, a.regno, a.producer, a.www_dir, a.www_file, a.date_deleted as date_modified, true as deleted
	  FROM deleted_media a
	  LEFT JOIN media b ON(a.id=b.id)
	 WHERE b.id IS NULL
	 UNION ALL
	SELECT a.id, a.regno, a.producer, a.www_dir, a.www_file, a.www_published, false
	  FROM media a
	  LEFT JOIN deleted_media b ON(a.id=b.id)
	 WHERE (b.id IS NULL OR b.source_file_created > b.date_deleted) 
	   AND a.www_ok = 1) AS m
 WHERE TRUE
~%%begin:producers%
   AND m.producer IN(~%value%)
~%%end:producers%
~%%begin:from%
   AND m.date_modified >= ~%value%
~%%end:from%
~%%begin:until%
   AND m.date_modified <= ~%value%
~%%end:until%
~%%begin:regno%
   AND (m.regno = :exact OR m.regno LIKE :like)
~%%end:regno%
~%%begin:limit%
 LIMIT ~%from%, ~%to%
~%%end:limit%
