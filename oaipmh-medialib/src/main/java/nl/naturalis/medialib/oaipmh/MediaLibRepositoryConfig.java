package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.DbRepositoryConfig;

final class MediaLibRepositoryConfig extends DbRepositoryConfig {

  private String medialibBaseUrl;

  String getMedialibBaseUrl() {
    return medialibBaseUrl;
  }

  void setMedialibBaseUrl(String medialibBaseUrl) {
    this.medialibBaseUrl = medialibBaseUrl;
  }

}
