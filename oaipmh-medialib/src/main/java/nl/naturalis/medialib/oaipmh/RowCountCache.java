package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.OaiRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class RowCountCache {

  private record Key(Instant from, Instant until, List<String> set) {
    Key(OaiRequest request) {
      this(request.getFrom(), request.getUntil(), request.getSet());
    }
  }

  static final RowCountCache INSTANCE = new RowCountCache();


  private static final Logger logger = LoggerFactory.getLogger(ListRecordsHandler.class);
  private static final long EXPIRE_INTERVAL = 60 * 30; // 30 minutes

  private final Map<Key, long[]> cache = new HashMap<>();

  private RowCountCache() { }

  int getRowCount(OaiRequest request) {
    logger.debug("Retrieving row count for this harvest from cache");
    Key key = new Key(request);
    long[] data = cache.get(key);
    if (data == null) {
      logger.debug("Row count not cached yet");
      return -1;
    }
    long now = Instant.now().getEpochSecond();
    long then = data[0];
    if (now - then > EXPIRE_INTERVAL) {
      logger.debug("Cached row count too old. Removing cache entry");
      cache.remove(key);
      return -1;
    }
    int rowCount = (int) data[1];
    logger.debug("Cached row count: {}", rowCount);
    return rowCount;
  }

  void setRowCount(OaiRequest request, int rowCount) {
    long now = Instant.now().getEpochSecond();
    long[] data = new long[]{now, rowCount};
    cache.put(new Key(request), data);
  }
}
