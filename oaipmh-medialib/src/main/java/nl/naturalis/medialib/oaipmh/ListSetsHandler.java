package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.util.ObjectFactories;
import org.apache.commons.lang3.time.StopWatch;
import org.klojang.check.Check;
import org.openarchives.oai._2.ListSetsType;
import org.openarchives.oai._2.OAIPMHtype;
import org.openarchives.oai._2.SetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.List;

import static nl.naturalis.oaipmh.api.CannotDisseminateFormatError.cannotDisseminateFormat;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHSkeleton;
import static org.klojang.check.CommonChecks.equalTo;

final class ListSetsHandler {

  private static final Logger logger = LoggerFactory.getLogger(ListRecordsHandler.class);

  private final MediaLibRepositoryConfig config;

  public ListSetsHandler(MediaLibRepositoryConfig config) {
    this.config = config;
  }

  OAIPMHtype handleRequest(OaiRequest request) throws OaiProtocolException {
    StopWatch stopwatch = StopWatch.createStarted();
    DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config);
    List<String> sets = queryExecutor.listSets();
    OAIPMHtype oaipmh = createOAIPMHSkeleton(request);
    ListSetsType list = ObjectFactories.OAI.createListSetsType();
    oaipmh.setListSets(list);
    sets.stream().map(this::newSet).forEach(set -> list.getSet().add(set));
    String secs = new DecimalFormat("0.00").format(stopwatch.getTime() / 1000D);
    logger.info("{} request completed successfully in {} seconds.",
          request.getVerb().value(),
          secs);
    return oaipmh;
  }

  private SetType newSet(String name) {
    SetType set = ObjectFactories.OAI.createSetType();
    set.setSetName(name);
    set.setSetSpec(name);
    return set;
  }
}
