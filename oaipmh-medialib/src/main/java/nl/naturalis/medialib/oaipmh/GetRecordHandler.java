package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.util.ObjectFactories;
import org.klojang.check.Check;
import org.openarchives.oai._2.*;
import org.openarchives.oai._2_0.oai_dc.OaiDcType;
import org.purl.dc.elements._1.ElementType;
import org.purl.dc.elements._1.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.util.Optional;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static nl.naturalis.oaipmh.api.CannotDisseminateFormatError.cannotDisseminateFormat;
import static nl.naturalis.oaipmh.api.IdDoesNotExistError.idDoesNotExist;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHSkeleton;
import static nl.naturalis.oaipmh.api.util.ObjectFactories.OAI;
import static org.klojang.check.CommonChecks.empty;
import static org.klojang.check.CommonChecks.equalTo;
import static org.klojang.util.StringMethods.ensureSuffix;

final class GetRecordHandler {

  private static final Logger logger = LoggerFactory.getLogger(GetRecordHandler.class);

  private final MediaLibRepositoryConfig config;

  GetRecordHandler(MediaLibRepositoryConfig config) {
    this.config = config;
  }

  OAIPMHtype handleRequest(OaiRequest request) throws OaiProtocolException {
    Check.notNull(request.getMetadataPrefix()).is(equalTo(), "oai_dc",
          () -> cannotDisseminateFormat(request));
    DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config);
    Optional<MediaEntity> opt = queryExecutor.getRecord();
    Check.that(opt).isNot(empty(), idDoesNotExist(request.getIdentifier()));
    OAIPMHtype oaipmh = createOAIPMHSkeleton(request);
    GetRecordType record = new GetRecordType();
    record.setRecord(createRecord(opt.get()));
    oaipmh.setGetRecord(record);
    return oaipmh;
  }

  private RecordType createRecord(MediaEntity entity) {
    RecordType rt = OAI.createRecordType();
    String id = constructIdentifier(entity);
    rt.setHeader(createHeader(entity, id));
    if (!entity.isDeleted()) {
      rt.setMetadata(createMetadata(entity, id));
    }
    return rt;
  }

  private static HeaderType createHeader(MediaEntity entity, String id) {
    HeaderType header = OAI.createHeaderType();
    header.setIdentifier(id);
    header.setDatestamp(
          ISO_INSTANT.format(entity.getDateModified().toInstant(ZoneOffset.UTC)));
    if (entity.isDeleted()) {
      header.setStatus(StatusType.DELETED);
    }
    return header;
  }

  private static MetadataType createMetadata(MediaEntity entity, String id) {
    MetadataType metadata = ObjectFactories.OAI.createMetadataType();
    OaiDcType oaiDc = ObjectFactories.OAI_DC.createOaiDcType();
    metadata.setAny(oaiDc);
    ObjectFactory dc = ObjectFactories.DC;
    ElementType et = new ElementType();
    et.setValue(id);
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createIdentifier(et));
    et = new ElementType();
    et.setValue(entity.getRegno());
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createDescription(et));
    et = new ElementType();
    et.setValue(entity.getProducer());
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createCreator(et));
    return metadata;
  }

  private String constructIdentifier(MediaEntity entity) {
    return new StringBuilder(80)
          .append(ensureSuffix(config.getMedialibBaseUrl(), "/")).append("file/id/")
          .append(URLEncoder.encode(entity.getRegno(), StandardCharsets.UTF_8))
          .append("/format/large").toString();
  }


}
