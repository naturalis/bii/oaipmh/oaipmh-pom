package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.util.ObjectFactories;
import org.apache.commons.lang3.time.StopWatch;
import org.klojang.check.Check;
import org.openarchives.oai._2.*;
import org.openarchives.oai._2_0.oai_dc.OaiDcType;
import org.purl.dc.elements._1.ElementType;
import org.purl.dc.elements._1.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.time.ZoneOffset;
import java.util.List;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static nl.naturalis.oaipmh.api.BadResumptionTokenError.recordOffsetOutOfRange;
import static nl.naturalis.oaipmh.api.CannotDisseminateFormatError.cannotDisseminateFormat;
import static nl.naturalis.oaipmh.api.NoRecordsMatchError.noRecordsMatch;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createDefaultResumptionToken;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHSkeleton;
import static nl.naturalis.oaipmh.api.util.ObjectFactories.OAI;
import static org.klojang.check.CommonChecks.*;
import static org.klojang.util.ObjectMethods.isEmpty;
import static org.klojang.util.StringMethods.ensureSuffix;

final class ListRecordsHandler {

  private static final Logger logger = LoggerFactory.getLogger(ListRecordsHandler.class);

  private final MediaLibRepositoryConfig config;

  ListRecordsHandler(MediaLibRepositoryConfig config) {
    this.config = config;
  }

  OAIPMHtype handleRequest(OaiRequest request) throws OaiProtocolException {
    Check.notNull(request.getMetadataPrefix()).is(equalTo(), "oai_dc",
          () -> cannotDisseminateFormat(request));
    if (isEmpty(request.getCustomParameter("__specimenId"))) {
      return listRecords(request);
    }
    return listRecordsBySpecimenId(request);
  }

  private OAIPMHtype listRecords(OaiRequest request) throws OaiProtocolException {
    StopWatch stopwatch = StopWatch.createStarted();
    DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config);
    int pageSize = request.getPageSize(config);
    int firstRecord = request.getRecordOffset(pageSize);
    int rowCount = getRowCount(request);
    Check.that(firstRecord).is(lt(), rowCount, recordOffsetOutOfRange());
    List<MediaEntity> media = queryExecutor.listRecords();
    logger.debug("Generating XML");
    OAIPMHtype oaipmh = createOAIPMHSkeleton(request);
    ListRecordsType records = OAI.createListRecordsType();
    oaipmh.setListRecords(records);
    media.forEach(m -> records.getRecord().add(createRecord(m)));
    if (!request.isHarvestComplete(pageSize, rowCount)) {
      records.setResumptionToken(
            createDefaultResumptionToken(request, config, rowCount));
    }
    stopwatch.stop();
    logSummary(request, stopwatch, rowCount);
    return oaipmh;
  }

  private int getRowCount(OaiRequest request) throws OaiProtocolException {
    int rowCount = RowCountCache.INSTANCE.getRowCount(request);
    if (rowCount == -1) {
      DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config);
      rowCount = queryExecutor.getRowCount();
      Check.that(rowCount).is(gt(), 0, noRecordsMatch());
    }
    // set outside if statement to refresh whenever row count is requested
    // (apparently we're still alive)
    RowCountCache.INSTANCE.setRowCount(request, rowCount);
    return rowCount;
  }

  private OAIPMHtype listRecordsBySpecimenId(OaiRequest request)
        throws OaiProtocolException {
    StopWatch stopwatch = StopWatch.createStarted();
    DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config);
    List<MediaEntity> media = queryExecutor.listRecords();
    Check.that(media).isNot(empty(), noRecordsMatch());
    logger.debug("Generating XML");
    OAIPMHtype oaipmh = createOAIPMHSkeleton(request);
    ListRecordsType records = OAI.createListRecordsType();
    oaipmh.setListRecords(records);
    media.forEach(m -> records.getRecord().add(createRecord(m)));
    stopwatch.stop();
    logSummary(request, stopwatch, media.size());
    return oaipmh;
  }


  private RecordType createRecord(MediaEntity entity) {
    RecordType rt = OAI.createRecordType();
    String id = constructIdentifier(entity);
    rt.setHeader(createHeader(entity, id));
    if (!entity.isDeleted()) {
      rt.setMetadata(createMetadata(entity, id));
    }
    return rt;
  }

  private static HeaderType createHeader(MediaEntity entity, String id) {
    HeaderType header = OAI.createHeaderType();
    header.setIdentifier(id);
    header.setDatestamp(
          ISO_INSTANT.format(entity.getDateModified().toInstant(ZoneOffset.UTC)));
    header.getSetSpec().add(entity.getProducer());
    if (entity.isDeleted()) {
      header.setStatus(StatusType.DELETED);
    }
    return header;
  }

  private static MetadataType createMetadata(MediaEntity entity, String id) {
    MetadataType metadata = ObjectFactories.OAI.createMetadataType();
    OaiDcType oaiDc = ObjectFactories.OAI_DC.createOaiDcType();
    metadata.setAny(oaiDc);
    ObjectFactory dc = ObjectFactories.DC;
    ElementType et = new ElementType();
    et.setValue(id);
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createIdentifier(et));
    et = new ElementType();
    et.setValue(entity.getRegno());
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createDescription(et));
    et = new ElementType();
    et.setValue(entity.getProducer());
    oaiDc.getTitleOrCreatorOrSubject().add(dc.createCreator(et));
    return metadata;
  }

  private String constructIdentifier(MediaEntity entity) {
    return new StringBuilder(80)
          .append(ensureSuffix(config.getMedialibBaseUrl(), "/")).append("file/id/")
          .append(URLEncoder.encode(entity.getRegno(), StandardCharsets.UTF_8))
          .append("/format/large").toString();
  }

  private void logSummary(OaiRequest request, StopWatch stopwatch, int rowCount) {
    long time = stopwatch.getTime();
    int pageSize = request.getPageSize(config);
    int lastRecord = request.getStartIndexOfNextPage(pageSize, rowCount);
    int documentsToGo = rowCount - lastRecord;
    int requestsToGo = (int) Math.ceil((double) documentsToGo / (double) pageSize);
    logger.info("Complete list size: {}", rowCount);
    if (documentsToGo == 0) {
      logger.info("Harvest complete!");
    } else {
      logger.info("Records served .....................: {}", lastRecord);
      logger.info("Records remaining ..................: {}", documentsToGo);
      logger.info("Page size ..........................: {}", pageSize);
      logger.info("Requests needed for full harvest ...: {}", requestsToGo);
    }
    String secs = new DecimalFormat("0.00").format(time / 1000D);
    logger.info("{} request completed successfully in {} seconds.",
          request.getVerb().value(), secs);
  }

}
