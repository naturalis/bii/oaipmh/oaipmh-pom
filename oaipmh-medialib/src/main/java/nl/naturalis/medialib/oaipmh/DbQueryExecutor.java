package nl.naturalis.medialib.oaipmh;

import nl.naturalis.common.Bool;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.util.Database;
import org.klojang.jdbc.*;
import org.klojang.jdbc.x.Utils;
import org.klojang.util.ExceptionMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static org.klojang.check.CommonChecks.notNull;
import static org.klojang.jdbc.BatchQuery.QueryId;
import static org.klojang.util.CollectionMethods.implode;
import static org.klojang.util.ObjectMethods.isEmpty;

final class DbQueryExecutor {

  private static final Logger logger = LoggerFactory.getLogger(DbQueryExecutor.class);

  private static final SessionConfig SESSION_CONFIG
        = SessionConfig.getDefaultConfig().withLowerCaseColumnNames();

  private final OaiRequest request;
  private final MediaLibRepositoryConfig config;
  private final int pageSize;

  DbQueryExecutor(OaiRequest request, MediaLibRepositoryConfig config) {
    this.request = request;
    this.config = config;
    this.pageSize = request.getPageSize(config);
  }

  Optional<MediaEntity> getRecord() {
    SQL sql = SQL.simple(SESSION_CONFIG, getClass(), "/get_record.sql");
    try (Connection conn = getConnection()) {
      conn.setReadOnly(true);
      try (SQLQuery query = sql.session(conn).prepareQuery()) {
        query.bind("regno", request.getIdentifier());
        return query.extract(this::createMediaEntity);
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
      throw ExceptionMethods.uncheck(e);
    }
  }

  List<MediaEntity> listRecords() {
    if (request.isStayAlive()) {
      return listRecordsAndStayAlive();
    }
    SQL skeleton = SQL.skeleton(SESSION_CONFIG, getClass(), getListRecordsQuery(false));
    try (Connection conn = getConnection()) {
      conn.setReadOnly(true);
      SQLSession session = skeleton.session(conn);
      bindRequest(session);
      List<String> list0 = request.getCustomParameter("__specimenId");
      if (isEmpty(list0)) {
        int offset = request.getRecordOffset(pageSize);
        logger.debug("---> LIMIT {}, {}", offset, pageSize);
        session.setNestedValue("limit.from", offset).setNestedValue("limit.to", pageSize);
      }
      try (SQLQuery query = session.prepareQuery()) {
        logger.debug("Creating MediaEntity objects");
        List<MediaEntity> result = query.extractAll(pageSize, this::createMediaEntity);
        logger.debug("Number of MediaEntity objects created: {}", result.size());
        return result;
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
      throw ExceptionMethods.uncheck(e);
    }
  }


  private List<MediaEntity> listRecordsAndStayAlive() {
    QueryId queryId;
    if (isEmpty(request.getResumptionToken())) {
      SQL skeleton = SQL.skeleton(SESSION_CONFIG, getClass(), getListRecordsQuery(false));
      try {
        Connection conn = getConnection();
        SQLSession session = skeleton.session(conn);
        bindRequest(session);
        SQLQuery query = session.prepareQuery();
        queryId = BatchQuery.register(query);
        request.addCustomParameter("__queryId", List.of(queryId.toString()));
      } catch (SQLException e) {
        logger.error(e.toString(), e);
        throw ExceptionMethods.uncheck(e);
      }
    } else {
      queryId = Utils.check(request.getQueryId())
            .is(notNull(), "cannot stay alive without query ID")
            .ok(QueryId::of);
    }
    BeanExtractorFactory<MediaEntity> factory
          = new BeanExtractorFactory<>(this::createMediaEntity);
    BatchQuery<MediaEntity> batchQuery = new BatchQuery<>(queryId, factory);
    logger.debug("Creating MediaEntity objects");
    List<MediaEntity> result = batchQuery.nextBatch(pageSize);
    logger.debug("Number of MediaEntity objects created: {}", result.size());
    return result;
  }

  /*
   * Returns total number of records satisfying request without LIMIT clause.
   */
  int getRowCount() {
    SQL skeleton = SQL.skeleton(SESSION_CONFIG, getClass(), getListRecordsQuery(true));
    try (Connection conn = getConnection()) {
      conn.setReadOnly(true);
      SQLSession session = skeleton.session(conn);
      bindRequest(session);
      try (SQLQuery query = session.prepareQuery()) {
        int count = query.getInt().get();
        logger.debug("Row count: {}", count);
        return count;
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
      throw ExceptionMethods.uncheck(e);
    }
  }

  private void bindRequest(SQLSession session) {
    if (request.getSet() != null) {
      logger.debug("---> m.producer IN ({})", implode(request.getSet(), ", "));
      session.setNestedValue("producers.value", request.getSet());
    }
    if (request.getFrom() != null) {
      String from = ISO_INSTANT.format(request.getFrom());
      logger.debug("---> m.date_modified >= '{}'", from);
      session.setNestedValue("from.value", from);
    }
    if (request.getUntil() != null) {
      String until = ISO_INSTANT.format(request.getUntil());
      logger.debug("---> m.date_modified <= '{}'", until);
      session.setNestedValue("until.value", until);
    }
    List<String> list0 = request.getCustomParameter("__specimenId");
    if (!isEmpty(list0)) {
      String id = list0.get(0);
      String like = id + "\\_%";
      logger.debug("---> m.regno = '{}' OR m.regno LIKE '{}\\_%'", id, id);
      session.setNestedValue("regno.exact", id).setNestedValue("regno.like", like);
    }
  }


  List<String> listSets() {
    logger.debug("Retrieving datasets (a.k.a. \"producers\") from medialib");
    SQL sql = SQL.staticSQL("SELECT DISTINCT(a.producer) FROM media a");
    try (Connection conn = getConnection()) {
      SQLSession session = sql.session(conn);
      try (SQLQuery query = session.prepareQuery()) {
        List<String> sets = query.firstColumn();
        logger.debug("Number of datasets: {}", sets.size());
        return sets;
      }
    } catch (SQLException e) {
      logger.error(e.toString(), e);
      throw ExceptionMethods.uncheck(e);
    }
  }


  private String getListRecordsQuery(boolean forCountQuery) {
    List<String> list = request.getCustomParameter("__specimenId");
    boolean ignore = !isEmpty(list);
    if (!ignore) {
      list = request.getCustomParameter("__ignoreDeleted");
      ignore = !isEmpty(list) && (isEmpty(list.get(0)) || Bool.from(list.get(0)));
    }
    if (ignore) {
      if (forCountQuery) {
        logger.debug("Ignoring deleted media");
        return "/count_records_ignore_deleted.sql";
      }
      return "/list_records_ignore_deleted.sql";
    } else if (forCountQuery) {
      logger.debug("Including deleted media");
      return "/count_records.sql";
    }
    return "/list_records.sql";
  }


  @SuppressWarnings({"unused"})
  private static final Pattern UNPRINTABLE = Pattern.compile(".*[^\\p{Print}]+.*");

  private MediaEntity createMediaEntity(ResultSet rs) throws SQLException {
    MediaEntity entity = new MediaEntity();
    entity.setId(rs.getInt("id"));
    String regno = rs.getString("regno");
    StringBuilder sb = new StringBuilder(regno.length());
    // Can you believe it - there is a bell character in one of the regnos!
    // Search for WAG.116407. That should not be a problem, were it not that JAXB
    // turns out to be a sloppy encoder, resulting in invalid, unparsable XML.
    for (int i = 0; i < regno.length(); ++i) {
      char c = regno.charAt(i);
      // This character set is based on what I encountered in the regno
      // column of the media table
      if (Character.isLetterOrDigit(c)
            || c == '.'
            || c == ' '
            || c == '-'
            || c == '_'
            || c == '('
            || c == ')'
            || c == '&'
            || c == '#'
            || c == ','
            || c == '\''
            || c == '+'
            || c == '='
      ) {
        sb.append(c);
      } else {
        logger.warn("Illegal character found in \"{}\": '{}'. Replaced with '?'",
              regno,
              c);
        sb.append('?');
      }
    }
    entity.setRegno(sb.toString());
    entity.setProducer(rs.getString("producer"));
    entity.setWwwDir(rs.getString("www_dir"));
    entity.setWwwFile(rs.getString("www_file"));
    entity.setDateModified(rs.getObject("date_modified", LocalDateTime.class));
    entity.setDeleted(false);
    return entity;
  }

  private Connection getConnection() throws SQLException {
    return Database.withConfig(config.getDatabase())
          .getConnectionPool()
          .getConnection();
  }


}
