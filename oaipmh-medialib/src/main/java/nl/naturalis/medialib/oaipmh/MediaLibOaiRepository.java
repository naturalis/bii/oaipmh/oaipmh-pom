package nl.naturalis.medialib.oaipmh;

import nl.naturalis.oaipmh.api.AbstractOaiRepository;
import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.StandAloneRepository;
import nl.naturalis.oaipmh.api.util.*;
import org.klojang.check.Check;
import org.openarchives.oai._2.OAIPMHtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static nl.naturalis.oaipmh.api.DatabaseType.MARIADB;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.configurationError;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.missingProperty;
import static org.klojang.check.CommonChecks.notNull;

public final class MediaLibOaiRepository extends AbstractOaiRepository
      implements StandAloneRepository {

  public static final String NAME = "medialib";

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(MediaLibOaiRepository.class);

  private MediaLibRepositoryConfig config;

  public MediaLibOaiRepository() { }

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public void configure(Map<String, Object> configData)
        throws RepositoryInitializationException {
    Check.that(configData).is(notNull(), () -> missingProperty("repositories.medialib"));
    try {
      this.config = OaiUtil.parseConfig(configData, MediaLibRepositoryConfig.class);
    } catch (IllegalArgumentException e) { // Deserialization error
      throw configurationError(
            "Error parsing configuration for repository medialib: %s", e.getMessage());
    }
    Check.that(config.getDatabase()).is(notNull(),
          () -> missingProperty("repositories.medialib.database"));
    if (config.getDatabase().getType() == null) {
      config.getDatabase().setType(MARIADB);
    }
  }

  @Override
  public void listRecords(OutputStream out) throws OaiProtocolException {
    ListRecordsHandler handler = new ListRecordsHandler(config);
    OAIPMHtype oaipmh = handler.handleRequest(oaiRequest);
    OaiWriter writer = new OaiWriter();
    writer.write(oaipmh, out, oaiRequest.isExplain());
  }

  @Override
  public void getRecord(OutputStream out) throws OaiProtocolException {
    GetRecordHandler handler = new GetRecordHandler(config);
    OAIPMHtype oaipmh = handler.handleRequest(oaiRequest);
    OaiWriter writer = new OaiWriter();
    writer.write(oaipmh, out, oaiRequest.isExplain());
  }

  @Override
  public void listSets(OutputStream out) throws OaiProtocolException {
    ListSetsHandler handler = new ListSetsHandler(config);
    OAIPMHtype oaipmh = handler.handleRequest(oaiRequest);
    OaiWriter writer = new OaiWriter();
    writer.write(oaipmh, out, oaiRequest.isExplain());
  }

  @Override
  public List<SimpleHealthCheck> getHealthChecks() {
    return Collections.singletonList(new DbHealthCheck(config.getDatabase(), NAME));
  }

  @Override
  public BuildInfo getBuildInfo() {
    return new BuildInfo("Media Library OAI-PMH interface up and running!");
  }

  @Override
  public void tearDown() {
    Database.withConfig(config.getDatabase()).shutDown();
  }

}
