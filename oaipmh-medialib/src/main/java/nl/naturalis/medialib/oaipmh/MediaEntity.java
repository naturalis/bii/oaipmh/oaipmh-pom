package nl.naturalis.medialib.oaipmh;

import java.time.LocalDateTime;

final class MediaEntity {

  private int id;
  private String regno;
  private String producer;
  private String wwwDir;
  private String wwwFile;
  private LocalDateTime dateModified;
  private boolean deleted;

  int getId() {
    return id;
  }

  void setId(int id) {
    this.id = id;
  }

  String getRegno() {
    return regno;
  }

  void setRegno(String regno) {
    this.regno = regno;
  }

  String getProducer() {
    return producer;
  }

  void setProducer(String producer) {
    this.producer = producer;
  }

  String getWwwDir() {
    return wwwDir;
  }

  void setWwwDir(String wwwDir) {
    this.wwwDir = wwwDir;
  }

  String getWwwFile() {
    return wwwFile;
  }

  void setWwwFile(String wwwFile) {
    this.wwwFile = wwwFile;
  }

  LocalDateTime getDateModified() {
    return dateModified;
  }

  void setDateModified(LocalDateTime dateModified) {
    this.dateModified = dateModified;
  }

  boolean isDeleted() {
    return deleted;
  }

  void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

}
