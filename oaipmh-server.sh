#!/bin/bash

basedir=$(dirname "${0}")
basedir=$(realpath "${basedir}")


if [[ ! -f "${basedir}/.env" ]]; then
  echo "Missing file ${basedir}/.env"
  echo "Consult README.md for how to create this file"
  exit 1
fi

source "${basedir}/.env"

# Create version.properties inside docker/dependencies
source "${basedir}/docker/create-version-file.sh"


export CONFIG_DIR="${basedir}/docker/dependencies"

java -cp \
  "${basedir}/docker/dependencies/*" \
	"-Xmn2048m" \
  "-Dfile.encoding=UTF-8" \
  "nl.naturalis.oaipmh.server.OaiServer" \
  "server" \
  "${basedir}/docker/oai-config.yml"
