#!/bin/bash
echo '**************************************************************************'
echo 'This script issues sudo commands, so you may have to provide your password'
echo '**************************************************************************'

docker compose down
sudo rm -rf './medialib/dbfs' './medialib/tmp' './geneious/dbfs'
mkdir './medialib/dbfs' './medialib/tmp' './geneious/dbfs'
chmod -R 0777 './medialib/dbfs' './medialib/tmp' './geneious/dbfs'
docker compose up -d