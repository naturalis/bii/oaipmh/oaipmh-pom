A [DropWizard](https://www.dropwizard.io/en/stable/) application that exposes OAI repositories through a REST interface.

Currently, the following OAI repositories have been adapted to, and run inside the OAI-PMH server:
- The [Geneious OAI-PMH interface](https://gitlab.com/naturalis/bii/geneious/geneious-oaipmh) which moves DNA related data from Geneious to CRS
- The [MediaLib OAI-PMH interface](https://gitlab.com/naturalis/bii/oaipmh/medialib-oaipmh) which moves multimedia metadata from the Media Library to CRS
- The [CRS OAI-PMH interface](https://gitlab.com/naturalis/bii/geneious/crs-oaipmh) which moves specimen and multimedia metadata from CRS to the NBDS. This is currently just a stub implementation.


# Dependencies
The oaipmh-server library has compile-time dependencies on [naturalis-common](https://github.com/naturalis/naturalis-common) and [oaipmh-api](https://gitlab.com/naturalis/bii/oaipmh/oaipmh-api). Besides that it has runtime dependencies on all concrete OAI repository implementations defined in the configuration file for the OAIPMH server ([oaipmh-server.yaml](oaipmh-server.yaml)). These are all separate Maven projects residing in the [Naturalis Maven repository](http://mvnrepository.naturalis.io:8081/#browse/search/maven), so as a developer you need to make Maven aware of this Maven repository. This is explained [here](https://gitlab.com/naturalis/sd/documentatie/-/blob/master/content/tools/maven-repository.md).

# Creating a distributable
Just run:

```
mvn clean install
```

This will create an "uber jar" that can be run directly from the command line. The uber jar will be created in the "target" directory (directly under the project root) and will be named _oaipmh-server-&lt;version&gt;.jar_.

# Running the server
- Copy oaipmh-server.jar to a directory of your choosing
- Copy [oaipmh-server.yaml](oaipmh-server.yaml) to a directory of your choosing
- Set required and optional environment variables. See oaipmh-server.yaml for which environment variables can and must to be set
- Change directory to the location of oaipmh-server.jar and run:
```
java -cp oaipmh-server.jar nl.naturalis.oaipmh.ws.OaiServer server  /path/to/oaipmh-server.yaml
```
