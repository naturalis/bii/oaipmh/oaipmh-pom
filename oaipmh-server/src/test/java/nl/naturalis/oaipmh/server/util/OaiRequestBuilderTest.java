package nl.naturalis.oaipmh.server.util;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OaiRequestBuilderTest {

  @Test
  public void parseDate() {
    String from = "2019-05-29T23:15:00Z";
    TemporalAccessor ta = DateTimeFormatter.ISO_INSTANT.parse(from);
    Instant in = Instant.from(ta);
    System.out.println(in.toString());
    assertEquals(from,in.toString());
  }

}
;