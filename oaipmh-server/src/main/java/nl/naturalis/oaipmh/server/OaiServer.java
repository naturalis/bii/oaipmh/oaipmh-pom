package nl.naturalis.oaipmh.server;

import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;
import io.dropwizard.lifecycle.Managed;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.util.SimpleHealthCheck;
import nl.naturalis.oaipmh.server.resource.OaiMessageBodyWriter;
import nl.naturalis.oaipmh.server.resource.RepoGroupResource;
import nl.naturalis.oaipmh.server.resource.RepositoryResource;
import nl.naturalis.oaipmh.server.resource.TestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static java.lang.System.getenv;
import static nl.naturalis.oaipmh.server.util.OaiServerUtil.broadcast;
import static nl.naturalis.oaipmh.server.util.OaiServerUtil.broadcastError;

/**
 * Main class for the OAI-PMH server. Start here.
 *
 * @author Ayco Holleman
 */
public class OaiServer extends Application<OaiConfig> {

  public static OaiConfig OAI_CONFIG;

  static {
    // Allows us to include the name of the repository in log messages
    MDC.put("repo", "");
  }

  private static final Logger logger = LoggerFactory.getLogger(OaiServer.class);

  public static void main(String[] args) throws Exception {
    logger.info("Running version \"{}\" of the OAI-PMH server", getenv("IMAGE_VERSION"));
    new OaiServer().run(args);
  }

  @Override
  public String getName() {
    return "OAI-PMH server";
  }

  @Override
  public void initialize(Bootstrap<OaiConfig> bootstrap) {
    bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
          bootstrap.getConfigurationSourceProvider(),
          new EnvironmentVariableSubstitutor(false)));
  }

  @Override
  public void run(OaiConfig cfg, Environment env) throws Exception {

    OAI_CONFIG = cfg;

    try {

      // Locate and instantiate OAI repositories
      RepositoryInitializer initializer = new ClassGraphRepositoryInitializer(cfg);
      initializer.initialize();

      initializer.getManagedRepositories().forEach(repo -> {
        env.lifecycle().manage(new Managed() {
          @Override
          public void start() throws Exception { }

          @Override
          public void stop() throws Exception {
            repo.tearDown();
          }
        });
      });

      // Add health checks provided by the OAI repositories
      initializer.getManagedRepositories()
            .stream()
            .flatMap(repo -> repo.getHealthChecks().stream())
            .forEach(c -> {
              // Health checks aren't actually carried out when registering them
              // You have to ping them on port 8086. But we want to broadcast
              // failed health checks as soon as possible.
              if (!c.isHealthy()) {
                logger.error(c.getMessage());
              }
              env.healthChecks().register(c.getName(), toNativeHealthCheck(c));
            });

      // JAX-RS providers
      env.jersey().register(new OaiMessageBodyWriter());


      // Resource classes
      // env.jersey().register(new HomeResource());
      env.jersey().register(new TestResource());
      env.jersey().register(new RepositoryResource(
            initializer.getStandAloneRepositories(),
            initializer.getRepositoryGroups(),
            cfg.baseUrl));
      env.jersey().register(new RepoGroupResource(
            initializer.getRepositoryGroups(),
            cfg.baseUrl));

      broadcast(":v:", "OAIPMH server has come online");

    } catch (RepositoryInitializationException e) {
      broadcastError(e.getMessage());
      logger.error(e.getMessage());
      throw e;
    } catch (Throwable t) {
      String msg = "Error while initializing OAI-PMH server";
      broadcastError(msg, t);
      logger.error(msg, t);
      throw t;
    }
  }

  private static HealthCheck toNativeHealthCheck(SimpleHealthCheck check) {
    return new HealthCheck() {
      @Override
      protected Result check() throws Exception {
        if (check.isHealthy()) {
          return Result.builder()
                .healthy()
                .withMessage(check.getMessage())
                .build();
        }
        return Result.builder()
              .unhealthy(check.getError())
              .withMessage(check.getMessage())
              .build();
      }
    };
  }

}
