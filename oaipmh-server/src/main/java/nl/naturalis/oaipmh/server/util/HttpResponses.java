package nl.naturalis.oaipmh.server.util;

import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.util.OaiWriter;
import org.klojang.util.exception.ExceptionOrigin;
import org.openarchives.oai._2.OAIPMHerrorType;
import org.openarchives.oai._2.OAIPMHtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import static jakarta.ws.rs.core.Response.Status.*;
import static nl.naturalis.common.ExceptionMethods.getRootStackTraceAsString;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHErrorResponse;

/**
 * Shortcuts for generating {@link Response} objects.
 *
 * @author Ayco Holleman
 */
public class HttpResponses {

  private static final Logger logger = LoggerFactory.getLogger(HttpResponses.class);

  private HttpResponses() { }

  /**
   * Generates an HTTP response with status 404 (Not Found) and the specified message in
   * the response body. The content type of the response body is set to text/plain.
   *
   * @param message
   * @return
   */
  public static Response notFound(String message) {
    String msg = getMessage(NOT_FOUND, message);
    logger.error(msg);
    return plainTextResponse(NOT_FOUND, msg);
  }

  /**
   * Generates an HTTP response with status 404 (Not Found) and the specified message in
   * the response body. The content type of the response body is set to text/plain.
   */
  public static Response notImplemented(UnsupportedOperationException e) {
    String msg = getMessage(NOT_IMPLEMENTED, e.getMessage());
    logger.error(msg);
    return plainTextResponse(NOT_IMPLEMENTED, msg);
  }

  /**
   * Generates an HTTP response with status 500 (Internal Server Error) and the specified
   * message in the response body. The content type of the response body is set to
   * text/plain.
   *
   * @param message
   * @return
   */
  public static Response serverError(String message) {
    String msg = getMessage(INTERNAL_SERVER_ERROR, message);
    logger.error(msg);
    return plainTextResponse(INTERNAL_SERVER_ERROR, msg);
  }

  /**
   * Generates an HTTP response with status 500 (INTERNAL SERVER ERROR) and the stack
   * trace of the <b>root cause</b> of the specified exception in the response body.
   *
   * @param t
   * @return
   */
  public static Response serverError(Throwable t) {
    ExceptionOrigin es = new ExceptionOrigin(t, "nl.naturalis");
    String msg = getMessage(INTERNAL_SERVER_ERROR, es.getDetailedMessage());
    logger.error(msg);
    return plainTextResponse(INTERNAL_SERVER_ERROR,
          msg + '\n' + getRootStackTraceAsString(t));
  }


  public static Response oaipmhErrorResponse(OaiRequest request, List<OAIPMHerrorType> errors) {
    OaiWriter writer = new OaiWriter();
    OAIPMHtype oaipmh = createOAIPMHErrorResponse(request, errors);
    ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
    writer.write(oaipmh, out);
    return xmlResponse(Response.Status.BAD_REQUEST, out);
  }


  /**
   * Generates a 200 (OK) response with the specified message in the response body and a
   * Content-Type header of text/plain.
   *
   * @param entity
   * @return
   */
  public static Response plainTextResponse(Object entity) {
    return plainTextResponse(OK, entity);
  }

  /**
   * Generates a response with the specified HTTP status code, the specified message in
   * the reponse body and a Content-Type header of text/plain.
   *
   * @param status
   * @param entity
   * @return
   */
  public static Response plainTextResponse(Response.Status status, Object entity) {
    return Response.status(status).entity(entity).type(MediaType.TEXT_PLAIN).build();
  }

  /**
   * Generate a 200 (OK) response with a Content-Type header of application/xml and the
   * specified XML string in the response body.
   *
   * @param xml
   * @return
   */
  public static Response xmlResponse(String xml) {
    return xmlResponse(OK, xml);
  }

  /**
   * Generate a 200 (OK) response with a Content-Type header of application/xml and the
   * specified JAXB object converted to XML in the response body.
   *
   * @param entity
   * @return
   */
  public static Response xmlResponse(Object entity) {
    return Response.ok(entity, MediaType.APPLICATION_XML).build();
  }

  /**
   * Returns XML content with the provided (possibbly non-OK) status. This can be used to
   * apply HTTP semantics for invalid OAI-PMH request.
   *
   * @param entity
   * @return
   */
  public static Response xmlResponse(Response.Status status, Object entity) {
    return Response
          .status(status)
          .entity(entity)
          .type(MediaType.APPLICATION_XML)
          .build();
  }

  /**
   * Reads the contents of the specified input stream and puts it on the HTTP
   * outputstream.
   *
   * @param is
   * @param mediaType
   * @return
   */
  public static Response streamingResponse(InputStream is, String mediaType) {
    return Response.ok().type(mediaType).entity(is).build();
  }

  /**
   * Reads the contents of the specified input stream and puts it on the HTTP
   * outputstream.
   *
   * @param is
   * @param mediaType
   * @return
   */
  public static Response streamingResponse(InputStream is, MediaType mediaType) {
    return Response.ok().type(mediaType).entity(is).build();
  }

  public static String getMessage(Response.Status status, String message) {
    return String.format("%d (%s)%n%s",
          status.getStatusCode(),
          status.getReasonPhrase(),
          message);
  }
}
