package nl.naturalis.oaipmh.server;

import io.dropwizard.core.Configuration;
import nl.naturalis.oaipmh.api.RepositoryConfig;

import javax.ws.rs.core.UriInfo;
import java.util.Map;

/**
 * Represents the general configuration for the OAI server. The general configuration
 * contains repository-specific blocks, which are modeled by {@link RepositoryConfig}
 * implementations.
 *
 * @author Ayco Holleman
 */
public class OaiConfig extends Configuration {

  /**
   * The base URL under which the OAI server runs. Ordinarily the service can figure this
   * out itself (by calling {@link UriInfo#getBaseUri() UriInfo.getBaseUri}). However, if
   * the service runs behind a load balancer or firewall, it could be that the code sees a
   * different base URL than the client. You must then explicitly configure the base URL
   * that the client sees in the server configuration file because OAI repositories need
   * it to generate xsi:schemaLocation attributes.
   */
  public String baseUrl;

  /**
   * Endpoint for the InstantMessenger class
   */
  public String messagingWebHook;

  /**
   * Configuration data for the individual OAI repositories <b>and</b> repository groups.
   * See docker/oai-config.yml
   */
  public Map<String, Map<String, Object>> repositories;

}
