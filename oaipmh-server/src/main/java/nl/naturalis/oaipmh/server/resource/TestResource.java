package nl.naturalis.oaipmh.server.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import nl.naturalis.oaipmh.server.util.HttpResponses;
import org.klojang.util.StringMethods;

import static nl.naturalis.oaipmh.server.util.OaiServerUtil.broadcast;

@Path("/test")
public class TestResource {

  @GET
  @Path("/mattermost")
  public Response error(@QueryParam("message") String message,
        @Context UriInfo uriInfo) {
    if (StringMethods.isBlank(message)) {
      message = "Please say something. Like: /?message=this is a test";
    }
    broadcast(":v", message);
    return HttpResponses.plainTextResponse("Just sent this message to mattermost: \""
          + message
          + "\"");
  }

}
