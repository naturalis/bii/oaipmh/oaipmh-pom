package nl.naturalis.oaipmh.server.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import nl.naturalis.oaipmh.api.*;
import nl.naturalis.oaipmh.server.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;

import static java.util.stream.Collectors.joining;
import static nl.naturalis.oaipmh.server.util.HttpResponses.*;
import static nl.naturalis.oaipmh.server.util.OaiServerUtil.*;
import static org.klojang.util.CollectionMethods.implode;

/**
 * REST resource handling requests for OAI repositories within a repository group.
 *
 * @author Ayco Holleman
 */
@Path("/{group}/{repo}")
public class RepoGroupResource {

  private static final Logger logger = LoggerFactory.getLogger(RepoGroupResource.class);

  private final Map<String, RepositoryGroup> repoGroups;
  private final String baseUrl;

  public RepoGroupResource(Map<String, RepositoryGroup> repoGroups,
        String baseUrlOverride) {
    this.repoGroups = repoGroups;
    this.baseUrl = baseUrlOverride;
  }

  /**
   * Handles requests for repositories within a repository group.
   *
   * @param groupName
   * @param repoName
   * @return
   */
  @GET
  public Response handleRequest(@PathParam("group") String groupName,
        @PathParam("repo") String repoName,
        @Context UriInfo uriInfo) {
    logger.info(uriInfo.getRequestUri().toString());
    if (!uriInfo.getQueryParameters().isEmpty()) {
      if (uriInfo.getQueryParameters().getFirst("resumptionToken") == null) {
        broadcastRequest(uriInfo.getRequestUri().toString());
      }
    }
    MDC.put("repo", "");
    try {
      RepositoryGroup group = repoGroups.get(groupName);
      if (group == null) {
        String msg = noSuchGroup(groupName);
        broadcastError(msg);
        return notFound(noSuchGroup(groupName));
      } else if (group.getRepository(repoName).isEmpty()) {
        String msg = noSuchRepo(groupName, repoName);
        broadcastError(msg);
        return notFound(noSuchRepo(groupName, repoName));
      }
      if (uriInfo.getQueryParameters().isEmpty()) {
        return OaiServerUtil.showHomePage(group);
      }
      String repoId = groupName + "/" + repoName;
      MDC.put("repoPrefix", "[" + repoId + "] ");
      OaiRepository repo;
      // we now lock the repository b/c we don't want other threads to get their
      // hands on it between repo.newRequest(), which sets the OaiRequest, and
      // repo.listRecords(), which processes the OaiRequest. And while we're at it,
      // why not just lock it now already.
      synchronized (repo = group.getRepository(repoName).get()) {
        ResumptionTokenParser parser = repo.getResumptionTokenParser();
        OaiRequestBuilder orb = new OaiRequestBuilder(parser, baseUrl, uriInfo);
        OaiRequest request = orb.build();
        if (!orb.getErrors().isEmpty()) {
          String msg = implode(orb.getErrors(), e -> e.getValue(), "  ***  ", 0, -1);
          broadcastWarning(msg);
          return oaipmhErrorResponse(request, orb.getErrors());
        }
        logger.info("Executing {} on repository {}", request.getVerb(), repoId);
        repo.newRequest(request);
        if (request.isExplain()) {
          // __detail: TRACE, DEBUG, etc.
          String detail = uriInfo.getQueryParameters().getFirst("__detail");
          return new OaiRequestDebugger(request, repo, detail).getResponse();
        }
        return new OaiRequestExecutor(request, repo, repoId).getResponse();
      }
    } catch (UnsupportedOperationException e) {
      return HttpResponses.notImplemented(e);
    } catch (Throwable t) {
      return HttpResponses.serverError(t);
    }
  }

  /**
   * Returns the XSD for the specified metadataPrefix.
   *
   * @param repoGroup
   * @param repoName
   * @param prefix
   * @return
   */
  @GET
  @Path("/{prefix}.xsd")
  public Response getXsd(@PathParam("group") String groupName,
        @PathParam("repo") String repoName,
        @PathParam("prefix") String prefix,
        @Context UriInfo uriInfo) {
    logger.info(uriInfo.getRequestUri().toString());
    MDC.put("repo", "");
    RepositoryGroup group = repoGroups.get(groupName);
    if (group == null) {
      return notFound(noSuchGroup(groupName));
    } else if (group.getRepository(repoName).isEmpty()) {
      return notFound(noSuchRepo(groupName, repoName));
    }
    String repoId = groupName + "/" + repoName;
    MDC.put("repoPrefix", "[" + repoId + "] ");
    OaiRepository repo = group.getRepository(repoName).get();
    try {
      return streamingResponse(repo.getXsdForMetadataPrefix(prefix),
            MediaType.APPLICATION_XML);
    } catch (XsdNotFoundException e) {
      return notFound(e.getMessage());
    } catch (Throwable t) {
      OaiServerUtil.broadcastError(createErrorMessage(t, uriInfo));
      return serverError(t);
    }
  }

  private String noSuchGroup(String groupName) {
    String available = repoGroups.keySet().stream().collect(joining(", "));
    return String.format(
          "No such repository group: \"%s\". Available repository groups: %s",
          groupName,
          available);
  }

  private String noSuchRepo(String groupName, String repoName) {
    RepositoryGroup group = repoGroups.get(groupName);
    String available = group.getRepositoryNames().stream().collect(joining(", "));
    return String.format(
          "No such repository in group %s: \"%s\". Available repositories: %s",
          groupName,
          repoName,
          available);
  }

}
