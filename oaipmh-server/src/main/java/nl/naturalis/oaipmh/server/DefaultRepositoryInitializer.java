package nl.naturalis.oaipmh.server;

import nl.naturalis.oaipmh.api.ManagedRepository;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;

import java.util.List;
import java.util.ServiceLoader;
import java.util.ServiceLoader.Provider;
import java.util.stream.Collectors;

/**
 * Initializes OAI repositories and repository groups using the Java 9+
 * ServiceLoader mechanism, which we can't use as long as we work with shaded jars.
 *
 * @author Ayco Holleman
 */
final class DefaultRepositoryInitializer extends RepositoryInitializer {

  DefaultRepositoryInitializer(OaiConfig cfg) {
    super(cfg);
  }

  @Override
  List<ManagedRepository> findImplementations()
      throws RepositoryInitializationException {
    return ServiceLoader.load(ManagedRepository.class)
        .stream()
        .map(Provider::get)
        .collect(Collectors.toList());
  }

}
