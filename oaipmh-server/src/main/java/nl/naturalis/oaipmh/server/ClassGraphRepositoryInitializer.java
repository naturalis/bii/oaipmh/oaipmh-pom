package nl.naturalis.oaipmh.server;

import java.util.ArrayList;
import java.util.List;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.ManagedRepository;

/**
 * Initializes OAI repositories and repository groups using the ClassGraph library.
 * 
 * @author Ayco Holleman
 *
 */

final class ClassGraphRepositoryInitializer extends RepositoryInitializer {

  ClassGraphRepositoryInitializer(OaiConfig cfg) {
    super(cfg);
  }

  @Override
  List<ManagedRepository> findImplementations() throws RepositoryInitializationException {
    try (ScanResult sr = new ClassGraph().enableClassInfo().scan()) {
      ClassInfoList cil = sr.getClassesImplementing(ManagedRepository.class.getName());
      List<ManagedRepository> repos = new ArrayList<>(cil.size());
      for (ClassInfo ci : cil) {
        try {
          Class<ManagedRepository> cls = ci.loadClass(ManagedRepository.class);
          if (cls.isInterface()) { // That must be StandAloneRepository or RepositoryGroup
            continue;
          }
          ManagedRepository repo = cls.getDeclaredConstructor().newInstance();
          repos.add(repo);
        } catch (Exception e) {
          throw new RepositoryInitializationException(e);
        }
      }
      return repos;
    }
  }

}
