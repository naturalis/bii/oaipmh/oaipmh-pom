package nl.naturalis.oaipmh.server.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.OutputStreamAppender;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Sends log messages for a particular logger to a newly created output stream. This
 * allows us to "redirect" log messages to the HTTP outputstream. You <b>must</b> use this
 * class in a try-with-resource block because it will add an appender to the logger that
 * only gets removed again when the <code>LiveLogStream</code> closes.
 */
public final class LiveLogStream implements AutoCloseable {

  private final OutputStream out;
  private final PatternLayoutEncoder encoder;
  private final OutputStreamAppender<ILoggingEvent> appender;

  private Level origLevel;

  public LiveLogStream(OutputStream out) {
    this(out, null);
  }

  public LiveLogStream(OutputStream out, String logLevel) {
    this.out = out;
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    encoder = new PatternLayoutEncoder();
    encoder.setPattern("%d{HH:mm:ss.SSS}  %msg%n");
    encoder.setContext(context);
    encoder.start();
    appender = new OutputStreamAppender<>();
    appender.setContext(context);
    appender.setEncoder(encoder);
    appender.setImmediateFlush(true);
    appender.setOutputStream(out);
    appender.start();
    Logger logger = context.getLogger("nl.naturalis");
    logger.addAppender(appender);
    if (logLevel != null && Level.toLevel(logLevel) != logger.getLevel()) {
      this.origLevel = logger.getLevel(); // cache original value
      logger.setLevel(Level.toLevel(logLevel));
    }
  }

  @Override
  public void close() throws IOException {
    out.flush();
    LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    Logger logger = context.getLogger("nl.naturalis");
    logger.detachAppender(appender);
    if (origLevel != null) {
      logger.setLevel(origLevel);
    }
    appender.stop();
    encoder.stop();
  }

}
