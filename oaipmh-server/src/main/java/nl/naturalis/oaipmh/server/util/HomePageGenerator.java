package nl.naturalis.oaipmh.server.util;

import nl.naturalis.oaipmh.api.ManagedRepository;
import nl.naturalis.oaipmh.api.util.BuildInfo;
import org.klojang.check.Check;
import org.klojang.templates.ParseException;
import org.klojang.templates.RenderSession;
import org.klojang.templates.Template;
import org.klojang.util.ExceptionMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.klojang.check.CommonChecks.empty;
import static org.klojang.check.CommonChecks.fileExists;
import static org.klojang.check.CommonExceptions.illegalState;

public class HomePageGenerator {

  private static final Map<Class<?>, String> homePages = new HashMap<>();

  public HomePageGenerator() { }

  /**
   * Get home page for OAI-PMH server itself.
   *
   * @return
   */
  public String getHomePage() {
    String html = homePages.get(getClass());
    if (html == null) {
      BuildInfo info = new BuildInfo("Naturalis OAI-PMH Server up and running!");
      html = getHomePage(getClass(), info);
      homePages.put(getClass(), html);
    }
    return html;
  }

  /**
   * Get home page for a repository.
   *
   * @param repo
   * @return
   */
  public String getHomePage(ManagedRepository repo) {
    String html = homePages.get(repo.getClass());
    if (html == null) {
      html = getHomePage(repo.getClass(), repo.getBuildInfo());
      homePages.put(repo.getClass(), html);
    }
    return html;
  }

  private static String getHomePage(Class<?> cls, BuildInfo buildInfo) {
    try {
      Template template = Template.fromResource(cls, "/welcome.html");
      RenderSession session = template.newRenderSession();
      session.insert(buildInfo);
      String cfgDir = System.getenv("CONFIG_DIR");
      Check.that(cfgDir).isNot(empty(), illegalState("missing env var: CONFIG_DIR"));
      String path = cfgDir + "/version.properties";
      File f = new File(path);
      Check.that(f).is(fileExists(), illegalState("missing file: " + path));
      try (InputStream in = new FileInputStream(f)) {
        Properties properties = new Properties();
        properties.load(in);
        session.insert(properties);
      }
      return session.render();
    } catch (ParseException | IOException e) {
      throw ExceptionMethods.uncheck(e);
    }
  }

}
