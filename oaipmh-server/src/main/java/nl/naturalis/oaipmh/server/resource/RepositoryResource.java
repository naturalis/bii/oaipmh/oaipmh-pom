package nl.naturalis.oaipmh.server.resource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import nl.naturalis.oaipmh.api.*;
import nl.naturalis.oaipmh.server.util.*;
import org.klojang.util.ExceptionMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;

import static jakarta.ws.rs.core.Response.Status.BAD_REQUEST;
import static nl.naturalis.oaipmh.server.util.HttpResponses.*;
import static nl.naturalis.oaipmh.server.util.OaiServerUtil.*;
import static org.klojang.util.CollectionMethods.implode;

/**
 * REST resource handling requests for stand-alone OAI repositories.
 *
 * @author Ayco Holleman
 */
@Path("/")
public class RepositoryResource {

  private static final Logger logger = LoggerFactory.getLogger(RepositoryResource.class);

  private final Map<String, StandAloneRepository> repos;
  private final Map<String, RepositoryGroup> repoGroups;
  private final String baseUrl;

  public RepositoryResource(Map<String, StandAloneRepository> repos,
        Map<String, RepositoryGroup> repoGroups,
        String baseUrl) {
    this.repos = repos;
    this.repoGroups = repoGroups;
    this.baseUrl = baseUrl;
  }

  /**
   * Show welcome content plus build info.
   *
   * @return
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String welcome() {
    return new HomePageGenerator().getHomePage();
  }

  /**
   * Intercepts requests for a favicon in case we are running under "/".
   *
   * @return
   */
  @GET
  @Path("/favicon.ico")
  public Response favicon(@Context UriInfo uriInfo) {
    logger.info(uriInfo.getRequestUri().toString());
    return streamingResponse(getClass().getResourceAsStream("/favicon.ico"),
          "image/x-icon");
  }

  @GET
  @Path("/{repo}")
  public Response handleRequest(@PathParam("repo") String repoName,
        @Context UriInfo uriInfo) {
    logger.info(uriInfo.getRequestUri().toString());
    if (!uriInfo.getQueryParameters().isEmpty()) {
      if (uriInfo.getQueryParameters().getFirst("resumptionToken") == null) {
        broadcastRequest(uriInfo.getRequestUri().toString());
      }
    }
    MDC.put("repo", "");
    try {
      StandAloneRepository repo = repos.get(repoName);
      if (repo == null) {
        // Maybe client meant to access a repository group?
        RepositoryGroup group = repoGroups.get(repoName);
        if (group != null) {
          if (uriInfo.getQueryParameters().isEmpty()) {
            return OaiServerUtil.showHomePage(group);
          } else {
            String repos = implode(group.getRepositoryNames());
            String msg = "Please specify a repository (" + repos + ")";
            broadcastError(msg);
            return plainTextResponse(BAD_REQUEST, msg);
          }
        }
        String msg = String.format("No such repository: \"%s\"", repoName);
        broadcastError(msg);
        return notFound(msg);
      }
      if (uriInfo.getQueryParameters().isEmpty()) {
        return OaiServerUtil.showHomePage(repo);
      }
      MDC.put("repoPrefix", "[" + repoName + "] ");
      synchronized (repo) {
        ResumptionTokenParser parser = repo.getResumptionTokenParser();
        OaiRequestBuilder orb = new OaiRequestBuilder(parser, baseUrl, uriInfo);
        OaiRequest request = orb.build();
        if (!orb.getErrors().isEmpty()) {
          String msg = implode(orb.getErrors(), e -> e.getValue(), "  ***  ", 0, -1);
          broadcastWarning(msg);
          return oaipmhErrorResponse(request, orb.getErrors());
        }
        logger.info("Executing {} on repository {}", request.getVerb(), repoName);
        repo.newRequest(request);
        if (request.isExplain()) {
          // __detail: TRACE, DEBUG, etc.
          String detail = uriInfo.getQueryParameters().getFirst("__detail");
          return new OaiRequestDebugger(request, repo, detail).getResponse();
        }
        return new OaiRequestExecutor(request, repo, repoName).getResponse();
      }
    } catch (UnsupportedOperationException e) {
      broadcastError(e.getMessage());
      return HttpResponses.notImplemented(e);
    } catch (Throwable t) {
      broadcastError(ExceptionMethods.getDetailedMessage(t, "nl.naturalis"));
      return HttpResponses.serverError(t);
    }
  }

  @GET
  @Path("/{repo}/{prefix}.xsd")
  public Response getXsd(@PathParam("repo") String repoName,
        @PathParam("prefix") String prefix, @Context UriInfo uriInfo) {
    logger.info(uriInfo.getRequestUri().toString());
    MDC.put("repo", "");
    StandAloneRepository repo = repos.get(repoName);
    if (repo == null) {
      return notFound(String.format("No such repository: \"%s\"", repoName));
    }
    MDC.put("repoPrefix", "[" + repoName + "] ");
    logger.debug(">>>>>>>>>>>  NEW REQUEST  <<<<<<<<<<<");
    try {
      return streamingResponse(repo.getXsdForMetadataPrefix(prefix),
            MediaType.APPLICATION_XML);
    } catch (XsdNotFoundException e) {
      return notFound(e.getMessage());
    } catch (Throwable t) {
      OaiServerUtil.broadcastError(createErrorMessage(t, uriInfo));
      return serverError(t);
    }
  }

}
