package nl.naturalis.oaipmh.server.util;

import jakarta.ws.rs.core.UriInfo;
import nl.naturalis.fuzzydate.FuzzyDate;
import nl.naturalis.oaipmh.api.*;
import nl.naturalis.oaipmh.api.util.OaiDateParser;
import org.openarchives.oai._2.OAIPMHerrorType;
import org.openarchives.oai._2.VerbType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static nl.naturalis.oaipmh.api.Argument.*;
import static org.klojang.util.CollectionMethods.implode;
import static org.klojang.util.ObjectMethods.isEmpty;
import static org.klojang.util.StringMethods.ensureSuffix;
import static org.klojang.util.StringMethods.isBlank;

/**
 * Builds {@link OaiRequest} objects from HTTP requests.
 *
 * @author Ayco Holleman
 */
public class OaiRequestBuilder {

  private static final Logger logger = LoggerFactory.getLogger(OaiRequestBuilder.class);

  private final ResumptionTokenParser parser;
  private final String baseUrlOverride;
  private final UriInfo uriInfo;

  private OaiRequest request;
  private List<OAIPMHerrorType> errors;

  public OaiRequestBuilder(ResumptionTokenParser parser,
        String baseUrlOverride,
        UriInfo uriInfo) {
    this.parser = parser;
    this.baseUrlOverride = baseUrlOverride;
    this.uriInfo = uriInfo;
  }

  /**
   * Creates an {@code OaiRequest} instance from the specified {@link UriInfo} object.
   *
   * @param uriInfo
   * @return
   */
  public OaiRequest build() {
    this.request = new OaiRequest();
    this.errors = new ArrayList<>();
    request.setBaseUri(getBaseUri());
    request.setRequestUri(getRequestUri());
    request.setMetadataPrefix(getArg(METADATA_PREFIX));
    request.setIdentifier(getArg(IDENTIFIER));
    request.setSet(getMultiValuedArg(SET));
    request.setResumptionToken(getArg(RESUMPTION_TOKEN));
    setVerb();
    setFrom();
    setUntil();
    checkArguments();
    uriInfo.getQueryParameters().entrySet().stream()
          .filter(e -> e.getKey().startsWith("__"))
          .forEach(e -> request.addCustomParameter(e.getKey(), e.getValue()));
    String explain = uriInfo.getQueryParameters().getFirst("__explain");
    if (explain != null && (explain.isEmpty() || explain.equalsIgnoreCase("true"))) {
      request.setExplain(true);
    }
    if (request.getResumptionToken() != null) {
      parseResumptionToken();
    }
    return request;
  }

  /**
   * Return the errors encountered while building the {@code OAIPMHRequest} object.
   *
   * @return
   */
  public List<OAIPMHerrorType> getErrors() {
    return errors;
  }

  private void setVerb() {
    String arg = getArg(VERB);
    if (arg == null) {
      errors.add(new BadVerbError());
    } else {
      try {
        VerbType verb = VerbType.fromValue(arg);
        request.setVerb(verb);
      } catch (IllegalArgumentException e) {
        errors.add(new BadVerbError(arg));
      }
    }
  }

  private void setFrom() {
    String arg = getArg(FROM);
    if (arg != null) {
      FuzzyDate date;
      try {
        date = OaiDateParser.INSTANCE.parse(arg);
      } catch (OaiProtocolException e) {
        errors.add(e.getError());
        return;
      }
      request.setFrom(date.toInstant());
      request.setDateFormatFrom(date.getParseAttempt().getFormatter());
    }
  }

  private void setUntil() {
    String arg = getArg(UNTIL);
    if (arg != null) {
      FuzzyDate date;
      try {
        date = OaiDateParser.INSTANCE.parse(arg);
      } catch (OaiProtocolException e) {
        errors.add(e.getError());
        return;
      }
      request.setUntil(date.toInstant());
      request.setDateFormatUntil(date.getParseAttempt().getFormatter());
    }
  }

  /*
   * Check for duplicate and illegal parameters. Returns all valid OAI-PMH arguments in the request
   * except the verb, which is more like the name of the function to be executed using those
   * arguments.
   */
  private void checkArguments() {
    EnumSet<Argument> args = EnumSet.noneOf(Argument.class);
    for (String param : uriInfo.getQueryParameters().keySet()) {
      if (param.startsWith("__")) {
        // Special parameters that have meaning for the
        // OAIPMH server, but are outside OAI-PMH protocol
        continue;
      }
      Argument arg = Argument.parse(param);
      if (arg == null) {
        String msg = "Illegal argument: " + param;
        errors.add(new BadArgumentError(msg));
      } else {
        List<String> values = uriInfo.getQueryParameters().get(param);
        if (values == null || values.size() == 0) {
          String msg = "Empty argument: " + param;
          errors.add(new BadArgumentError(msg));
        } else if (values.size() != 1 && arg != SET) {
          // The OAI-PMH protocol does in fact not allow for multiple values of
          // the "set" parameter, but we deviate from the protocol for practical
          // reasons
          String msg = "Duplicate argument: " + param;
          errors.add(new BadArgumentError(msg));
        }
        if (arg != VERB) {
          args.add(arg);
        }
      }
    }
    if (request.getVerb() != null) {
      ArgumentValidatorFactory acf = ArgumentValidatorFactory.INSTANCE;
      ArgumentValidator validator = acf.getValidatorForVerb(request.getVerb());
      errors.addAll(validator.validate(args));
    }
  }

  private void parseResumptionToken() {
    try {
      parser.parseResumptionToken(request);
    } catch (OaiProtocolException e) {
      errors.addAll(e.getErrors());
    }
  }

  private URI getBaseUri() {
    return isBlank(baseUrlOverride) ? uriInfo.getBaseUri() : URI.create(baseUrlOverride);
  }

  private URI getRequestUri() {
    if (isBlank(baseUrlOverride)) {
      return uriInfo.getRequestUri();
    }
    /*
     * The base URL was hard-coded in the server configuration file, probably because the OAI-PMH server
     * was unable to figure out the base URL by itself. Maybe there's firewall or load balancer in front
     * of the OAIPMH server.
     */
    String s = ensureSuffix(baseUrlOverride, "/") + uriInfo.getPath();
    return URI.create(s);
  }

  private List<String> getMultiValuedArg(Argument arg) {
    List<String> values = uriInfo.getQueryParameters().get(arg.param());
    if (isEmpty(values)) {
      logger.debug("Retrieving query parameter {}: ", arg.param(), "(not present)");
    } else {
      logger.debug("Retrieving query parameter {}: \"{}\"",
            arg.param(),
            implode(values, x -> '"' + x + '"'));
    }
    return values;
  }

  private String getArg(Argument arg) {
    List<String> values = uriInfo.getQueryParameters().get(arg.param());
    if (isEmpty(values)) {
      logger.debug("Retrieving query parameter {}: ", arg.param(), "(not present)");
      return null;
    } else {
      logger.debug("Retrieving query parameter {}: \"{}\"", arg.param(), values.get(0));
      return values.get(0);
    }
  }

}
