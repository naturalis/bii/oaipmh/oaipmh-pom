#!/bin/bash

# Creates the env file necessary for running the deployment
# locally rather than inside a Gitlab pipeline. This is not
# the env file for Docker Compose or the application! Also,
# it is about deploying _from_ your local machine; not about
# deploying _to_ your local machine.

f="${BASE_DIR}/ansible/deploy.env"

echo '# Check this file before every deployment! VAULT_TOKEN may need'                    >> "${f}"
echo '# to be refreshed as it expires. Go to HC Vault web UI to get a'                    >> "${f}"
echo '# new token. VAULT_SERVER_URL, VAULT_PATH, ANSIBLE_HOSTS, and'                      >> "${f}"
echo '# ANSIBLE_GROUPS are defined in .gitlab-ci.yml. CI_REGISTRY_PASSWORD'               >> "${f}"
echo '# is your Gitlab access token. Setting IMAGE_VERSION is optional.'                  >> "${f}"
echo '# By default, the "latest" image is deployed (take note!!!).'                       >> "${f}"
echo ""                                                                                   >> "${f}"
echo "export VAULT_TOKEN="                                                                >> "${f}"
echo "export VAULT_SERVER_URL=https://vault.naturalis.io"                                 >> "${f}"
echo "export VAULT_PATH=bii/oaipmh"                                                       >> "${f}"
echo "export ANSIBLE_GROUPS="                                                             >> "${f}"
echo "export ANSIBLE_HOSTS="                                                              >> "${f}"
echo "export CI_REGISTRY=registry.gitlab.com"                                             >> "${f}"
echo "export CI_REGISTRY_USER="                                                           >> "${f}"
echo "export CI_REGISTRY_PASSWORD="                                                       >> "${f}"
echo "export IMAGE_VERSION="                                                              >> "${f}"
