#!/bin/bash
# shellcheck disable=SC2086

# Retrieves secrets from HC Vault. This script requires the
# following environment variables to be set:
#
# o VAULT_TOKEN
# o VAULT_SERVER_URL
# o VAULT_PATH
# o ANSIBLE_GROUPS
# o ANSIBLE_HOSTS
#
# When running inside a Gitlab pipeline, these variables are
# defined in .gitlab-ci.yml. When running locally, the variables
# must be defined in a file named "deploy.env"

docker pull hashicorp/vault:latest

# check if VAULT_TOKEN is empty. If so exit with error
if [[ -z ${VAULT_TOKEN} ]]; then
  echo "Empty vault token"
  exit 1
fi

rm -rf "${BASE_DIR}/ansible/inventory/group_vars"
rm -rf "${BASE_DIR}/ansible/inventory/host_vars"
rm -rf "${BASE_DIR}/ansible/secrets"

mkdir -p "${BASE_DIR}/ansible/inventory/group_vars"
mkdir -p "${BASE_DIR}/ansible/inventory/host_vars"
mkdir -p "${BASE_DIR}/ansible/secrets"

FULL_SECRET_PATH="kv2/${VAULT_PATH}/cicd/group_vars"
# ANSIBLE_GROUPS must be a space-separated list of all groups
# that the target host belongs to. Unfortunately, there is no
# easy way to determine this automatically. Therefore you must
# "manually" specify them in .gitlab-ci.yml (or deploy.env when
# running the deployment locally).
echo 'Reading group vars ...'
for config in ${ANSIBLE_GROUPS}; do
  echo "---> ${config}"
  docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault vault kv get \
      -field data \
      --format=yaml \
      -address="${VAULT_SERVER_URL}" \
      "${FULL_SECRET_PATH}/${config}" \
      > "${BASE_DIR}/ansible/inventory/group_vars/${config}.yml"
  if [[ ${?} -ne 0 ]]; then
    # Just in case some secrets _were_ read
    rm -rf "${BASE_DIR}/ansible/inventory/group_vars"
    rm -rf "${BASE_DIR}/ansible/inventory/host_vars"
    rm -rf "${BASE_DIR}/ansible/secrets"
    if [[ ${GITLAB_PIPELINE} == false ]]; then
      echo 'Is the vault token in deploy.env still valid?'
    fi
    exit 1
  fi
done

FULL_SECRET_PATH="kv2/${VAULT_PATH}/cicd/host_vars"
# ANSIBLE_HOSTS must be defined in .gitlab-ci.yml, and must be a
# space-separated list of all target hosts.
echo 'Reading host vars ...'
for config in ${ANSIBLE_HOSTS}; do
  echo "---> ${config}"
  docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault vault kv get \
      -field data \
      --format=yaml \
      -address="${VAULT_SERVER_URL}" \
      "${FULL_SECRET_PATH}/${config}" \
      > "${BASE_DIR}/ansible/inventory/host_vars/${config}.yml"
done

echo 'Reading ssh key ...'
ANSIBLE_DEPLOY_KEY=$(docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault vault kv get \
      -address="${VAULT_SERVER_URL}" \
      -field ANSIBLE_DEPLOY_KEY \
      "kv2/${VAULT_PATH}/cicd/ssh")

echo "${ANSIBLE_DEPLOY_KEY}" >> "${BASE_DIR}/ansible/secrets/id_ed25519"
chmod 0400 "${BASE_DIR}/ansible/secrets/id_ed25519"
