[![pipeline status](https://gitlab.com/naturalis/bii/oaipmh/oaipmh-pom/badges/develop/pipeline.svg)](https://gitlab.com/naturalis/bii/oaipmh/oaipmh-pom/-/commits/develop)

# OAI-PMH Server

For a functional description of the OAI-PMH server, see

## Maven Modules

- [oaipmh-api](oaipmh-api/README.md) - A Service Provider Interface and tool kit for
  implementing an OAI repositories
- [oaipmh-geneious](oaipmh-geneious/README.md) - The Geneious OAI repository group
- [oaipmh-medialib](oaipmh-medialib/README.md) - The Medialib OAI repository
- [oaipmh-server](oaipmh-server/README.md) - A REST service mediating between clients and
  OAI repositories

## Maven Build

Before you can execute a Maven build, two environment variables need to be set:
MAVEN_REPO_USER and MAVEN_REPO_PASS (see
[maven-settings.xml](docker/maven-settings.xml)). These are the username and password of
the Naturalis Maven repository. The required info is in Bitwarden. Search for "**Nexus
Maven Repository - dev**"

To start a build, simply run

```bash
mvn clean install
```

from the root of the oaipmh-pom project.

You can also mimic the Maven build as it happens within the Gitlab pipeline (see
[.gitlab-ci.yml](.gitlab-ci.yml)).

1. Go to the `docker` directory directly under the root directory.
2. Run `mvn-install.sh`. This will generate the build artifacts and copy them, along with
   all their dependencies, to the `dependencies` directory inside the docker directory.
   (Note that this in fact also happens if you just run `mvn clean install` from the
   project root, but with `mvn-install.sh` you always start with a clean slate &#8212; an
   empty Maven repository and an empty `dependencies` directory.)

## Local Databases

The OAI-PMH server connects to the Medialib database and the Geneious database. In order
to develop or test the OAI-PMH server locally, these databases need to run locally as
well, because the database ports on the remote database servers are closed except for the
production instance of the OAI-PMH server. The [db-up.sh](test-data/db-up.sh) script
starts the databases locally using Docker Compose. The databases are pre-populated with
some test data.

1. Go to the `test-data` directory directly under the root directory
2. Run `db-up.sh`

Assuming you have a mysql client, you can access the Medialib database directly using:

```
mysql --user=develop --password=develop --port=3305 medialib
```

Assuming you have a psql client, you can access the Geneious database directly using:

```
psql --dbname=geneious --user=develop --host=localhost --port=5431
```

The databases listen on non-standard ports so as not to interfere with other installations
of MySQL and PostgreSQL.

Every time you run `db-up.sh`, the databases are first brought down (if necessary), and
then up again with just the original test data. To bring down the databases, simply run
`docker compose down`.

## Run As Stand-Alone Java Application

You can run the OAI-PMH server as a stand-alone Java application (that is, without Docker
or Docker Compose). This requires a Java 21 JVM and Maven. 

1. Build the application using Maven.
2. Bring up the local databases as described above.
3. In the root of the repository, create a `.env` file with the following contents:

```
# Value can be found in HC Vault
export MESSAGING_WEB_HOOK=<enter url>

export MEDIALIB_BASE_URL=https://medialib.naturalis.nl
export MEDIALIB_DB_HOST=127.0.0.1
export MEDIALIB_DB_PORT=3305
export MEDIALIB_DB_USER=develop
export MEDIALIB_DB_PASS=develop
export MEDIALIB_DB_NAME=medialib

export GENEIOUS_DB_HOST=127.0.0.1
export GENEIOUS_DB_PORT=5431
export GENEIOUS_DB_USER=develop
export GENEIOUS_DB_PASS=develop
export GENEIOUS_DB_NAME=geneious

# log level for code outside nl.naturalis
export MAIN_LOGLEVEL=WARN
# log level for nl.naturalis code
export APP_LOGLEVEL=DEBUG
```

_(Note that, although its purpose is identical, this is **not** the typical Docker Compose
.env file: notice the `export` keyword. This file will be sourced by oaipmh-server.sh.)_

4. Run [oaipmh-server.sh](oaipmh-server.sh)

## Run Within IDE (IntelliJ)

1. If you have never run the application before within IntelliJ, navigate to
   the [OaiServer](oaipmh-server/src/main/java/nl/naturalis/oaipmh/server/OaiServer.java)
   class and run its `main` method. This will fail, but now you have a run configuration
   that you can edit.
2. Open the OaiServer run configuration and specify two program arguments: "server" and
   "docker/oai-config.yml":

![run-config](readme-assets/run-config.png)

3. Still within the OaiServer run configuration, open the `Environment variables:` panel
   and paste the following into it:

```
MESSAGING_WEB_HOOK=See HC Vault
MEDIALIB_BASE_URL=https://medialib.naturalis.nl
MEDIALIB_DB_HOST=127.0.0.1
MEDIALIB_DB_PORT=3305
MEDIALIB_DB_USER=develop
MEDIALIB_DB_PASS=develop
MEDIALIB_DB_NAME=medialib
GENEIOUS_DB_HOST=127.0.0.1
GENEIOUS_DB_PORT=5431
GENEIOUS_DB_USER=develop
GENEIOUS_DB_PASS=develop
MAIN_LOGLEVEL=WARN
APP_LOGLEVEL=DEBUG
CONFIG_DIR=docker/dependencies
```

![env vars](readme-assets/env-vars.png)

4. Now you should be able to successfully hit the Run icon next to the OaiServer run
   configuration:

![run](readme-assets/run-icon.png)

## Docker Container

The docker container really is not much else besides the "dependencies" directory. Within
the container, this directory becomes `/opt/oaipmh/`. The docker build also copies the
server configuration file
([oai-config.yaml](docker/oai-config.yaml)) into this directory. which then becomes the
one and only required classpath entry for the Java command that spins up the OAI-PMH
server. See the [Dockerfile](docker/Dockerfile).

## Docker Compose

Since the OAI-PMH server does not itself require a database, running the docker container
with Docker Compose is straight forward:

1. Clone the docker-compose-oaipmh Git repository:

```
git clone https://gitlab.com/naturalis/bii/oaipmh/docker-compose-oaipmh.git
```

2. Get the .env file from Bitwarden (search for "**env file oaipmh-server V3 PROD (
   2022-10)**") and place it in the directory where you cloned the Git repository.
3. Start the application

```bash
docker compose up -d
```

## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks). The
pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
