package nl.naturalis.geneious.oaipmh;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.w3c.dom.Element;
import org.xml.sax.SAXParseException;
import nl.naturalis.geneious.oaipmh.util.XmlUtil;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class XMLSerialisableRootElementFactoryTest {

  @Test
  public void testBuild() throws SAXParseException, IOException {
    XmlSerialisableRootElementFactory factory = new XmlSerialisableRootElementFactory();
    try (InputStream is = getClass().getResourceAsStream("/plugin_document_xml_01.xml")) {
      String xml = IOUtils.toString(is, "UTF-8");
      Element root = XmlUtil.getDocumentElement(xml);
      XmlSerialisableRootElement xsre = factory.build(root);
      assertNotNull("01", xsre.getName());
      assertEquals("02", "foo", xsre.getName());
    }
  }

}
