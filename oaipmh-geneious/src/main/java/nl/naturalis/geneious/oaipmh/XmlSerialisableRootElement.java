package nl.naturalis.geneious.oaipmh;

/**
 * Corresponds to the XML contents of the plugin_document_xml column in case the root
 * element is &lt;XMLSerialisableRootElement&gt;.
 *
 * @author Ayco Holleman
 */
public final class XmlSerialisableRootElement extends PluginDocumentXml {

  private String name;

  public XmlSerialisableRootElement() { }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
