package nl.naturalis.geneious.oaipmh.extracts;

import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.DocumentNotes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.*;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.discardOldest;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.sameNote;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.traceRecordDiscarded;

/**
 * Compares two {@link AnnotatedDocument} instances and decides which one takes precedence
 * over the other. If one is chosen, the other one is marked as discarded. If no choice
 * can be made, both remain candidates.
 *
 * @author Ayco Holleman
 */
class ExtractSelector {

  private static final Logger logger = LoggerFactory.getLogger(ExtractSelector.class);
  private static final String DUMMY_MARKER = "Dum";
  private static final String MSG0 = "{} document yields to {} document (id={})";
  private static final String MSG1 = "dummy document yields to document with marker {} (id={})";

  private ExtractSelector() { }

  static void select(AnnotatedDocument doc0, AnnotatedDocument doc1) {

    DocumentNotes notes0 = doc0.getDocumentXml().getNotes();
    DocumentNotes notes1 = doc1.getDocumentXml().getNotes();

    if (!Objects.equals(doc0.getExtractId(), doc1.getExtractId())) {
      return; // Different extracts. Can't choose. Bye.
    }

    if (!sameNote(notes0, notes1, RegistrationNumberCode_Samples)) {
      return; // Idem
    }

    if (!sameNote(notes0, notes1, PlatePositionCode_Samples)) {
      return; // Idem
    }

    if (!sameNote(notes0, notes1, ExtractPlateNumberCode_Samples)) {
      return; // Idem
    }

    if (!sameNote(notes0, notes1, MarkerCode_Seq)) {
      // Then the 2 documents have different markers. If one is the dummy marker and the
      // other is not, choose the other one (and vice versa)
      String marker0 = notes0.get(MarkerCode_Seq);
      String marker1 = notes1.get(MarkerCode_Seq);
      if (isDummyMarker(marker0) && !isDummyMarker(marker1)) {
        if (!doc0.isDiscarded() && logger.isTraceEnabled()) {
          // Only log if doc0 hasn't already been discarded in a previous round of comparisons.
          // Otherwise the log file explodes.
          traceRecordDiscarded(logger, doc0, MSG1, marker1, doc1.getId());
        }
        doc0.discard();
      } else if (isDummyMarker(marker1) && !isDummyMarker(marker0)) {
        if (!doc1.isDiscarded() && logger.isTraceEnabled()) {
          traceRecordDiscarded(logger, doc1, MSG1, marker0, doc0.getId());
        }
        doc1.discard();
      }
      return; // Documents have different markers. Can't choose. Bye.
    }

    // Can we make a decision based on "maturity" (consensus > contig > fasta etc.)?
    if (doc1.getMaturity().isHigherThan(doc0.getMaturity())) {
      if (!doc0.isDiscarded() && logger.isTraceEnabled()) {
        traceRecordDiscarded(logger,
              doc0,
              MSG0,
              doc0.getMaturity(),
              doc1.getMaturity(),
              doc1.getId());
      }
      doc0.discard();
      return;
    } else if (doc0.getMaturity().isHigherThan(doc1.getMaturity())) {
      if (!doc1.isDiscarded() && logger.isTraceEnabled()) {
        traceRecordDiscarded(logger,
              doc1,
              MSG0,
              doc1.getMaturity(),
              doc0.getMaturity(),
              doc0.getId());
      }
      doc1.discard();
      return;
    }

    // No. Fall back on choosing the record with the greatest database ID.
    discardOldest(doc0, doc1);
  }

  private static boolean isDummyMarker(String marker) {
    return marker.equals(DUMMY_MARKER);
  }

}
