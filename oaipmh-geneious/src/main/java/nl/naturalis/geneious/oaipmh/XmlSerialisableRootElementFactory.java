package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.util.XmlUtil;
import org.w3c.dom.Element;

/**
 * Factory for {@link XmlSerialisableRootElement} instances.
 *
 * @author Ayco Holleman
 */
final class XmlSerialisableRootElementFactory {

  XmlSerialisableRootElementFactory() { }

  /**
   * Converts the XML contents of the plugin_document_xml column to an instance of
   * {@link XmlSerialisableRootElement}.
   *
   * @param root
   * @return
   */
  XmlSerialisableRootElement build(Element root) {
    XmlSerialisableRootElement result = new XmlSerialisableRootElement();
    result.setName(XmlUtil.getValue(root, "name"));
    return result;
  }

}
