package nl.naturalis.geneious.oaipmh.plates;

import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.DocumentNotes;

import java.util.Comparator;

import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.ExtractPlateNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.discardOldest;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.sameNote;

/**
 * A {@link Comparator} for {@link AnnotatedDocument} instances that, per plate number,
 * selects the instance with the highest database ID.
 *
 * @author Ayco Holleman
 * @see PlateSetFilter
 */
final class PlateSelector {

  PlateSelector() { }

  static void select(AnnotatedDocument doc0, AnnotatedDocument doc1) {
    DocumentNotes notes0 = doc0.getDocumentXml().getNotes();
    DocumentNotes notes1 = doc1.getDocumentXml().getNotes();
    if (sameNote(notes0, notes1, ExtractPlateNumberCode_Samples)) {
      discardOldest(doc0, doc1);
    }
  }

}
