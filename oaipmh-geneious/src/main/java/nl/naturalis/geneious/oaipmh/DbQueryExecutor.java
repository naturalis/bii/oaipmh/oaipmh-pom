package nl.naturalis.geneious.oaipmh;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.util.stream.Collectors.joining;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.RepositoryException;
import nl.naturalis.oaipmh.api.util.Database;
import nl.naturalis.oaipmh.api.util.OaiDateParser;

/**
 * Retrieves records from the database and converts them to AnnotatedDocument
 * instances.
 *
 * @author Ayco Holleman
 */
final class DbQueryExecutor {

  private static final Logger logger = LoggerFactory.getLogger(DbQueryExecutor.class);

  private static final String BASE_QUERY =
      "SELECT a.id,\n\ta.document_xml,\n\ta.plugin_document_xml,"
          + "\n\tdsfv.value AS relevant_modified_date,"
          + "\n\tLOWER(ssfv.value) AS extract_id,\n\tCOUNT(*) OVER() as row_count"
          + "\nFROM annotated_document a "
          + "\n\tINNER JOIN folder f ON (a.folder_id = f.id) "
          + "\n\tINNER JOIN boolean_search_field_value bsfv ON (a.id = bsfv.annotated_document_id AND bsfv.search_field_code = 'DocumentNoteUtilities-CRS (CRS).CRSCode_CRS') "
          + "\n\tINNER JOIN date_search_field_value dsfv ON (a.id = dsfv.annotated_document_id AND dsfv.search_field_code='modified_date') "
          + "\n\tINNER JOIN string_search_field_value ssfv ON (a.id = ssfv.annotated_document_id AND ssfv.search_field_code='DocumentNoteUtilities-Extract ID (Samples).ExtractIDCode_Samples') "
          + "\nWHERE f.visible " // Only documents in visible folders
          + "\n\tAND bsfv.value "; // CRSCode_CRS must be true

  /*
   * It seems like we can indeed do an inner (rather than left) join with the
   * date_search_field_value table. At the time of writing that would cause us to
   * miss 20 annotated_document records, but these are anyhow unlikely candidates for
   * inclusion in the OAI-PMH output.
   *
   * Likewise we have also checked that the inner join on string_search_field_value
   * won't cause us to miss any annotated_document records that we might be
   * interested in.
   */

  private final OaiRequest request;
  private final GeneiousRepositoryConfig config;
  private final List<Validator> validators;

  DbQueryExecutor(OaiRequest request,
      GeneiousRepositoryConfig config,
      List<Validator> validators) {
    this.request = request;
    this.config = config;
    this.validators = validators;
  }

  List<AnnotatedDocument> getValidDocuments() throws RepositoryException {
    String sql = getSQL();
    try (Connection conn = Database.withConfig(config.getDatabase())
        .getConnectionPool()
        .getConnection()) {
      conn.setReadOnly(true);
      try (PreparedStatement stmt = conn.prepareStatement(sql)) {
        stmt.setFetchDirection(ResultSet.FETCH_FORWARD);
        stmt.setFetchSize(config.getDatabase().getFetchSize());
         logger.debug("Executing query:\n{}", sql);
        bindQueryParams(stmt);
        try (ResultSet rs = stmt.executeQuery()) {
          if (!rs.next()) {
            logger.debug("Query returned 0 rows");
            return Collections.emptyList();
          }
          int rowCount = rs.getInt("row_count");
          if (logger.isDebugEnabled()) {
            logger.debug("Query returned {} row(s)", rowCount);
            logger.debug("Parsing XML contents of the returned rows");
          }
          DocumentHandler handler = new DocumentHandler(validators, rowCount);
          do {
            handler.handleDocument(createDocument(rs));
          } while (rs.next());
          List<AnnotatedDocument> docs = handler.getValidDocuments();
          if (logger.isDebugEnabled()) {
            logger.debug("Number of rows discarded while parsing XML contents: {}",
                rowCount - docs.size());
            logger.debug("Number of rows remaining: {}", docs.size());
          }
          return docs;
        }
      }
    } catch (SQLException e) {
      throw new RepositoryException(e);
    }
  }

  private static AnnotatedDocument createDocument(ResultSet rs) throws SQLException {
    AnnotatedDocument doc = new AnnotatedDocument();
    doc.setId(rs.getInt("id"));
    doc.setModified(rs.getObject("relevant_modified_date", LocalDate.class));
    doc.setDocumentXmlString(rs.getString("document_xml"));
    doc.setPluginDocumentXmlString(rs.getString("plugin_document_xml"));
    doc.setExtractId(rs.getString("extract_id"));
    return doc;
  }

  private String getSQL() {
    StringBuilder sb = new StringBuilder(1024);
    sb.append(BASE_QUERY);
    if (request.getFrom() != null) {
      sb.append("\n\tAND dsfv.value >= ?");
    }
    if (request.getUntil() != null) {
      sb.append("\n\tAND dsfv.value <= ?");
    }
    List<String> extractIds = request.getCustomParameter("__extractId");
    if (extractIds != null && !extractIds.isEmpty()) {
      sb.append("\n\tAND ssfv.value = ANY (string_to_array(?, ','))");
    }
    List<String> ids = request.getCustomParameter("__id");
    if (ids != null && !ids.isEmpty()) {
      sb.append("\n\tAND a.id = ANY (string_to_array(?, ',')::int[])");
    }
    return sb.toString();
  }

  private void bindQueryParams(PreparedStatement stmt) throws SQLException {
    if (logger.isDebugEnabled()) {
      logger.debug("Binding query variables");
    }
    int paramNo = 1;
    if (request.getFrom() != null) {
      if (logger.isDebugEnabled()) {
        logger.debug("---> dsfv.value >= {}", ISO_INSTANT.format(request.getFrom()));
      }
      stmt.setObject(paramNo++, OaiDateParser.toLocalDate(request.getFrom()));
    }
    if (request.getUntil() != null) {
      if (logger.isDebugEnabled()) {
        logger.debug("---> dsfv.value <= {}",
            ISO_INSTANT.format(request.getUntil()));
      }
      stmt.setObject(paramNo++, OaiDateParser.toLocalDate(request.getUntil()));
    }
    List<String> extractIds = request.getCustomParameter("__extractId");
    if (extractIds != null && !extractIds.isEmpty()) {
      // The index table (string_search_field_value) always uppercases the indexed values
      String s = extractIds.stream().map(String::toUpperCase).collect(joining(","));
      if (logger.isDebugEnabled()) {
        logger.debug("---> ssfv.value = ANY ({})", s);
      }
      stmt.setString(paramNo++, s);
    }
    List<String> ids = request.getCustomParameter("__id");
    if (ids != null && !ids.isEmpty()) {
      String s = ids.stream().collect(joining(","));
      if (logger.isDebugEnabled()) {
        logger.debug("---> a.id = ANY ({})", s);
      }
      stmt.setString(paramNo++, s);
    }
    if (logger.isDebugEnabled() && paramNo == 1) {
      logger.debug("No variables found in query");
    }
  }

}
