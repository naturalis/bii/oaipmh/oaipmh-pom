package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.extracts.ExtractRepository;
import nl.naturalis.geneious.oaipmh.plates.PlateRepository;
import nl.naturalis.geneious.oaipmh.specimens.SpecimenRepository;
import nl.naturalis.oaipmh.api.OaiRepository;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.util.*;
import org.klojang.check.Check;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static nl.naturalis.oaipmh.api.DatabaseType.POSTGRES;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.configurationError;
import static nl.naturalis.oaipmh.api.RepositoryInitializationException.missingProperty;
import static org.klojang.check.CommonChecks.notNull;

/**
 * Geneious implementation of the {@link RepositoryGroup} interface.
 *
 * @author Ayco Holleman
 */
public final class GeneiousRepositoryGroup implements RepositoryGroup {

  public static final String NAME = "geneious";

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory
        .getLogger(GeneiousRepositoryGroup.class);
  private static final List<String> names = List.of("specimens", "extracts",
        "plates");

  private GeneiousRepositoryConfig config;
  private Map<String, OaiRepository> repos;

  @Override
  public String getName() {
    return NAME;
  }

  @Override
  public void configure(Map<String, Object> configData)
        throws RepositoryInitializationException {
    Check.that(configData).is(notNull(), () -> missingProperty("repositories.geneious"));
    try {
      this.config = OaiUtil.parseConfig(configData, GeneiousRepositoryConfig.class);
    } catch (IllegalArgumentException e) { // Deserialization error
      throw configurationError(
            "Error parsing configuration for repository geneious: %s", e.getMessage());
    }
    Check.that(config.getDatabase()).is(notNull(),
          () -> missingProperty("repositories.geneious.database"));
    if (config.getDatabase().getType() == null) {
      config.getDatabase().setType(POSTGRES);
    }
    this.repos = Map.of(names.get(0), new SpecimenRepository(config), names.get(1),
          new ExtractRepository(config), names.get(2), new PlateRepository(config));
  }

  @Override
  public List<SimpleHealthCheck> getHealthChecks() {
    return Collections.singletonList(new DbHealthCheck(config.getDatabase(), NAME));
  }

  @Override
  public BuildInfo getBuildInfo() {
    return new BuildInfo("Geneious OAI-PMH Interface up and running!");
  }

  @Override
  public Optional<OaiRepository> getRepository(String name) {
    return Optional.ofNullable(repos.get(name));
  }

  @Override
  public List<String> getRepositoryNames() {
    return names;
  }

  @Override
  public void tearDown() {
    Database.withConfig(config.getDatabase()).shutDown();
    repos = null;
  }

}
