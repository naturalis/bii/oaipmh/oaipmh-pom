package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.jaxb.DnaExtract;
import nl.naturalis.oaipmh.api.AbstractOaiRepository;
import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.RepositoryException;
import nl.naturalis.oaipmh.api.util.OaiWriter;
import org.openarchives.oai._2.OAIPMHtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;

import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.GENEIOUS_XMLNS;

/**
 * Abstract base class for the Geneious OAI repositories.
 *
 * @author Ayco Holleman
 */
public abstract class GeneiousOaiRepository extends AbstractOaiRepository {

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(GeneiousOaiRepository.class);

  private final GeneiousRepositoryConfig config;

  public GeneiousOaiRepository(GeneiousRepositoryConfig config) {
    // System.out.println(">>>> " + OaiUtil.getConfigAsString(config));
    this.config = config;
  }

  /**
   * Prepare and initialize for a new OAI-PMH request.
   *
   * @param request
   */
  @Override
  public void newRequest(OaiRequest request) {
    super.newRequest(request);
  }

  @Override
  public InputStream getXsdForMetadataPrefix(String prefix)
        throws RepositoryException {
    if (prefix.equals("geneious")) {
      return getClass().getResourceAsStream("/geneious.xsd");
    }
    return super.getXsdForMetadataPrefix(prefix);
  }

  @Override
  public void listRecords(OutputStream out)
        throws OaiProtocolException, RepositoryException {
    ListRecordsHandler listRecordsHandler = newListRecordsHandler(config);
    OAIPMHtype oaipmh = listRecordsHandler.handleRequest(oaiRequest);
    OaiWriter writer = new OaiWriter();
    writer.addJaxbPackage(DnaExtract.class.getPackage().getName());
    writer.write(oaipmh, out, oaiRequest.isExplain());
    writer.setSchemaLocation(GENEIOUS_XMLNS,
          oaiRequest.getRequestUri(true) + "geneious.xsd");
  }

  // To be implemented by subclasses. Must always return a new instance, not a cached one.
  protected abstract ListRecordsHandler newListRecordsHandler(GeneiousRepositoryConfig config);

}
