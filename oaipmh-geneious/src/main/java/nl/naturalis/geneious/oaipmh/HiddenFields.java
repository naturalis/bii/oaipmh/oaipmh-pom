package nl.naturalis.geneious.oaipmh;

import java.util.EnumMap;
import java.util.Objects;

/**
 * A {@code DocumentHiddenFields} instance maintains the values of all possibly relevant
 * elements under the &lt;hiddenFields&gt; element within the document_xml column.
 *
 * @author Ayco Holleman
 */
public final class HiddenFields {

  /**
   * Enumerates all XML element tag names underneath the &lt;hiddenFields&gt; element of
   * the document_xml column that might be used for OAI-PMH generation.
   */
  public enum HiddenField {
    cache_name, override_cache_name, override_modified_date
  }

  private final EnumMap<HiddenField, String> data = new EnumMap<>(HiddenField.class);

  /**
   * Returns the number of notes extracted from the XML in the document_xml column.
   *
   * @return
   */
  public int count() {
    return data.size();
  }

  /**
   * Whether or not the document_xml column contains the specified &lt;note&gt; element.
   *
   * @param hiddenField
   * @return
   */
  public boolean isPresent(HiddenField hiddenField) {
    return data.containsKey(hiddenField);
  }

  /**
   * Set the value of the specified note.
   *
   * @param hiddenField
   * @param value
   */
  public void set(HiddenField hiddenField, String value) {
    data.put(hiddenField, value);
  }

  /**
   * Get the value of the specified note.
   *
   * @param hiddenField
   * @return
   */
  public String get(HiddenField hiddenField) {
    return data.get(hiddenField);
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    HiddenFields other = (HiddenFields) obj;
    return Objects.equals(data, other.data);
  }

  public int hashCode() {
    return Objects.hash(data);
  }

}
