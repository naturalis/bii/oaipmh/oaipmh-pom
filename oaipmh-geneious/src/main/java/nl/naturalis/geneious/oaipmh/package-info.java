/**
 * 
 * Common classes for the three repositories in the Geneious repository group.
 * 
 * @author Ayco Holleman
 *
 */
package nl.naturalis.geneious.oaipmh;
