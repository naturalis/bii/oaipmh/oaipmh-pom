package nl.naturalis.geneious.oaipmh;

/**
 * Abstract base class for all classes modeling the contents of the plugin_document_xml
 * column of the annotated_document table.
 *
 * @author Ayco Holleman
 */
public abstract class PluginDocumentXml {

  protected PluginDocumentXml() { }

}
