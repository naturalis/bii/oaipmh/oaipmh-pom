package nl.naturalis.geneious.oaipmh.specimens;

import nl.naturalis.geneious.oaipmh.GeneiousOaiRepository;
import nl.naturalis.geneious.oaipmh.GeneiousRepositoryConfig;
import nl.naturalis.geneious.oaipmh.ListRecordsHandler;

/**
 * OAI repository for specimens.
 *
 * @author Ayco Holleman
 */
public class SpecimenRepository extends GeneiousOaiRepository {

  public SpecimenRepository(GeneiousRepositoryConfig config) {
    super(config);
  }

  protected ListRecordsHandler newListRecordsHandler(GeneiousRepositoryConfig config) {
    return new SpecimenListRecordsHandler(config);
  }

}
