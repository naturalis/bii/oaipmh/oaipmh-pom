package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.util.LogUtil;
import nl.naturalis.geneious.oaipmh.util.XmlUtil;
import org.klojang.util.StringMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.SAXParseException;

final class PluginDocumentXmlParser {

  private static final Logger logger = LoggerFactory.getLogger(PluginDocumentXmlParser.class);

  PluginDocumentXmlParser() { }

  void parsePluginDocumentXml(AnnotatedDocument doc) {
    if (StringMethods.isBlank(doc.getPluginDocumentXmlString())) {
      doc.discard();
      LogUtil.warnRecordDiscarded(logger,
            doc,
            "column plugin_document_xml must not be empty");
      return;
    }
    Element root;
    try {
      root = XmlUtil.getDocumentElement(doc.getPluginDocumentXmlString());
    } catch (SAXParseException e) {
      doc.discard();
      LogUtil.warnRecordDiscarded(logger,
            doc,
            "error while parsing contents of column plugin_document_xml ({})",
            e);
      return;
    }
    switch (root.getTagName()) {
      case "XMLSerialisableRootElement":
        doc.setPluginDocumentXml(new XmlSerialisableRootElementFactory().build(root));
        break;
      case "DefaultAlignmentDocument":
        doc.setPluginDocumentXml(new DefaultAlignmentDocumentFactory().build(root));
        break;
      case "ABIDocument":
        doc.setPluginDocumentXml(new Ab1DocumentFactory().build(root));
        break;
      default:
        doc.discard();
        LogUtil.warnRecordDiscarded(logger,
              doc,
              "cannot process element <{}> in column plugin_document_xml",
              root.getTagName());
    }
    doc.setPluginDocumentXmlString(null); // allow raw XML string to be garbage collected as quickly as possible
  }

}
