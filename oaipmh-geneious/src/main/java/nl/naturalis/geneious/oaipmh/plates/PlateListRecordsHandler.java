package nl.naturalis.geneious.oaipmh.plates;

import java.util.Collections;
import java.util.List;
import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.DocumentNotes;
import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import nl.naturalis.geneious.oaipmh.GeneiousRepositoryConfig;
import nl.naturalis.geneious.oaipmh.ListRecordsHandler;
import nl.naturalis.geneious.oaipmh.SetFilter;
import nl.naturalis.geneious.oaipmh.Validator;
import nl.naturalis.geneious.oaipmh.jaxb.DnaExtractPlate;
import nl.naturalis.geneious.oaipmh.jaxb.ExtractPlateUnit;
import nl.naturalis.geneious.oaipmh.jaxb.Geneious;

/**
 * Handles ListRecords requests for extract plates.
 * 
 * @author Ayco Holleman
 *
 */
public class PlateListRecordsHandler extends ListRecordsHandler {

  PlateListRecordsHandler(GeneiousRepositoryConfig config) {
    super(config);
  }

  @Override
  protected List<Validator> getValidators() {
    return Collections.emptyList();
  }

  @Override
  protected List<SetFilter> getSetFilters() {
    return Collections.singletonList(new PlateSetFilter());
  }

  @Override
  protected List<Note> getAdditionalRequiredNotes() {
    return Collections.emptyList();
  }

  @Override
  protected void setMetadata(Geneious geneious, AnnotatedDocument ad) {
    geneious.setDnaExtractPlate(createDnaExtractPlate(ad));
  }

  private static DnaExtractPlate createDnaExtractPlate(AnnotatedDocument ad) {
    DnaExtractPlate plate = new DnaExtractPlate();
    ExtractPlateUnit unit = new ExtractPlateUnit();
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    unit.setInstitutePlateID(notes.get(Note.ExtractPlateNumberCode_Samples));
    plate.setUnit(unit);
    return plate;
  }

}
