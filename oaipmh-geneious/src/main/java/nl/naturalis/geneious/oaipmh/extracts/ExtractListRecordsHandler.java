package nl.naturalis.geneious.oaipmh.extracts;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.DocumentNotes;
import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import nl.naturalis.geneious.oaipmh.Fields.Field;
import nl.naturalis.geneious.oaipmh.GeneiousRepositoryConfig;
import nl.naturalis.geneious.oaipmh.ListRecordsHandler;
import nl.naturalis.geneious.oaipmh.Maturity;
import nl.naturalis.geneious.oaipmh.SetFilter;
import nl.naturalis.geneious.oaipmh.Validator;
import nl.naturalis.geneious.oaipmh.jaxb.Amplification;
import nl.naturalis.geneious.oaipmh.jaxb.DnaExtract;
import nl.naturalis.geneious.oaipmh.jaxb.DnaLabProject;
import nl.naturalis.geneious.oaipmh.jaxb.ExtractUnit;
import nl.naturalis.geneious.oaipmh.jaxb.Geneious;
import nl.naturalis.geneious.oaipmh.jaxb.GeneticAccession;
import nl.naturalis.geneious.oaipmh.jaxb.Sequencing;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.AmplicificationStaffCode_FixedValue_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.BOLDIDCode_Bold;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.ConsensusSeqPassCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.DocumentVersionCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.ExtractPlateNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.GenBankIDCode_Bold;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.GenBankURICode_FixedValue_Bold;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.MarkerCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.PCRplateIDCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.PlatePositionCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.ProjectPlateNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.RegistrationNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.SequencingStaffCode_FixedValue_Seq;

/**
 * Handles ListRecords requests for DNA extracts.
 * 
 * @author Ayco Holleman
 *
 */
public class ExtractListRecordsHandler extends ListRecordsHandler {

  ExtractListRecordsHandler(GeneiousRepositoryConfig config) {
    super(config);
  }

  @Override
  protected List<Validator> getValidators() {
    return Collections.emptyList();
  }

  @Override
  protected List<SetFilter> getSetFilters() {
    return Arrays.asList(new ExtractSetFilter());
  }

  @Override
  protected List<Note> getAdditionalRequiredNotes() {
    return Collections.emptyList();
  }

  @Override
  protected void setMetadata(Geneious geneious, AnnotatedDocument ad) {
    geneious.setDnaExtract(createDnaExtract(ad));
  }

  private static DnaExtract createDnaExtract(AnnotatedDocument ad) {
    DnaExtract extract = new DnaExtract();
    extract.setUnit(createExtractUnit(ad));
    if (ad.getMaturity() != Maturity.DUMMY) {
      extract.setDnaLabProject(createDnaLabProject(ad));
    }
    return extract;
  }

  private static DnaLabProject createDnaLabProject(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    DnaLabProject project = new DnaLabProject();
    project.setBatchID(notes.get(ProjectPlateNumberCode_Samples));
    project.setVersionNumber(notes.get(DocumentVersionCode_Seq));
    project.setSequencing(createSequencing(ad));
    project.setAmplification(createAmplification(ad));
    return project;
  }

  private static ExtractUnit createExtractUnit(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    ExtractUnit unit = new ExtractUnit();
    unit.setUnitID(ad.getExtractId());
    unit.setAssociatedUnitID(notes.get(RegistrationNumberCode_Samples));
    unit.setInstitutePlateID(notes.get(ExtractPlateNumberCode_Samples));
    unit.setPlatePosition(notes.get(PlatePositionCode_Samples));
    return unit;
  }

  private static Sequencing createSequencing(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    Sequencing sequencing = new Sequencing();
    sequencing.setSequencingStaff(notes.get(SequencingStaffCode_FixedValue_Seq));
    sequencing.setGeneticAccession(createGeneticAccession(ad));
    sequencing.setConsensusSequenceQuality(notes.get(ConsensusSeqPassCode_Seq));
    switch (ad.getMaturity()) {
      case CONSENSUS:
        sequencing.setConsensusSequenceID(ad.getName());
        sequencing.setConsensusSequenceLength(getConsensusSequenceLength(ad));
        break;
      case CONTIG:
        sequencing.setConsensusSequenceID(ad.getName());
        sequencing.setConsensusSequenceLength(getConsensusSequenceLength(ad));
        break;
      case FASTA:
        sequencing.setConsensusSequenceID(ad.getName());
        sequencing.setConsensusSequenceLength(getSequenceLength(ad));
        break;
      case TRACE:
      case AB1_REVERSED:
      default:
        break;
    }
    return sequencing;
  }

  private static GeneticAccession createGeneticAccession(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    GeneticAccession ga = new GeneticAccession();
    ga.setBOLDProcessID(notes.get(BOLDIDCode_Bold));
    ga.setGeneticAccessionNumber(notes.get(GenBankIDCode_Bold));
    ga.setGeneticAccessionNumberURI(notes.get(GenBankURICode_FixedValue_Bold));
    return ga;
  }

  private static Amplification createAmplification(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    Amplification amp = new Amplification();
    amp.setAmplificationStaff(notes.get(AmplicificationStaffCode_FixedValue_Samples));
    amp.setMarker(notes.get(MarkerCode_Seq));
    amp.setPcrPlateID(notes.get(PCRplateIDCode_Seq));
    return amp;
  }

  private static String getSequenceLength(AnnotatedDocument ad) {
    return ad.getDocumentXml().getFields().get(Field.sequence_length);
  }

  private static String getConsensusSequenceLength(AnnotatedDocument ad) {
    String s = ad.getDocumentXml().getFields().get(Field.consensusSequenceLength);
    return s == null ? getSequenceLength(ad) : s;
  }

}
