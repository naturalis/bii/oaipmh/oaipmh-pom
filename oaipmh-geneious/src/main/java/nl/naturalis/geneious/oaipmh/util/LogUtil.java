package nl.naturalis.geneious.oaipmh.util;

import static nl.naturalis.common.ArrayMethods.prefix;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.MarkerCode_Seq;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;

import nl.naturalis.geneious.oaipmh.AnnotatedDocument;
import nl.naturalis.geneious.oaipmh.Validator;
import nl.naturalis.oaipmh.api.OaiRequest;

public class LogUtil {

	private static final String DEFAULT_MESSAGE_PREFIX = "document with id={}/{}: ";
	private static final String MSG_RECORD_DISCARDED = "{} document discarded (id={}/{}): ";

	public static void traceOldestRecordDiscarded(Logger logger,
			AnnotatedDocument discarded, AnnotatedDocument selected) {
		traceRecordDiscarded(logger, discarded,
				"yields to more recent {} document with id {}", selected.getMaturity(),
				selected.getId());
	}

	public static void warnRecordDiscarded(Logger logger, AnnotatedDocument ad,
			String reason, Object... msgArgs) {
		logger.warn(MSG_RECORD_DISCARDED + reason,
				prefix(msgArgs, ad.getMaturity(), ad.getId(), ad.getExtractId()));
	}

	public static void debugRecordDiscarded(Logger logger, AnnotatedDocument ad,
			String reason, Object... msgArgs) {
		logger.debug(MSG_RECORD_DISCARDED + reason,
				prefix(msgArgs, ad.getMaturity(), ad.getId(), ad.getExtractId()));
	}

	public static void traceRecordDiscarded(Logger logger, AnnotatedDocument ad,
			String reason, Object... msgArgs) {
		logger.trace(MSG_RECORD_DISCARDED + reason,
				prefix(msgArgs, ad.getMaturity(), ad.getId(), ad.getExtractId()));
	}

	public static void logWarning(Logger logger, AnnotatedDocument doc, String reason,
			Object... msgArgs) {
		logger.warn(DEFAULT_MESSAGE_PREFIX + reason,
				prefix(msgArgs, doc.getId(), doc.getExtractId()));
	}

	public static void logDebug(Logger logger, AnnotatedDocument doc, String reason,
			Object... msgArgs) {
		logger.debug(DEFAULT_MESSAGE_PREFIX + reason,
				prefix(msgArgs, doc.getId(), doc.getExtractId()));
	}

	public static void logTrace(Logger logger, AnnotatedDocument doc, String reason,
			Object... msgArgs) {
		logger.trace(DEFAULT_MESSAGE_PREFIX + reason,
				prefix(msgArgs, doc.getId(), doc.getExtractId()));
	}

	public static void logValidationResults(Logger logger,
			List<AnnotatedDocument> docs, List<Validator> validators) {
		for (Validator validator : validators) {
			String name = validator.getClass().getSimpleName();
			logger.debug("Number of documents discarded by {}: {}", name,
					validator.getNumDiscarded());
			logger.debug("Number of documents remaining: {}", validator.getNumAccepted());
		}
		logDocCountsPerMaturity(logger, docs);
		logDocCountsPerMarker(logger, docs);
	}

	public static void logSummary(Logger logger, OaiRequest request,
			StopWatch stopwatch, List<AnnotatedDocument> docs, int pageSize) {
		long time = stopwatch.getTime();
		int lastRecord = request.getStartIndexOfNextPage(pageSize, docs.size());
		int documentsToGo = docs.size() - lastRecord;
		int requestsToGo = (int) Math.ceil((double) documentsToGo / (double) pageSize);
		logger.info("Total number of documents: {}", docs.size());
		if (logger.isDebugEnabled()) {
			logDocCountsPerMaturity(logger, docs);
			logDocCountsPerMarker(logger, docs);
		}
		if (documentsToGo == 0) {
			logger.info("Harvest complete!");
		} else {
			logger.info("Number of documents served so far: {}", lastRecord);
			logger.info("Number of documents remaining: {}", documentsToGo);
			logger.info("Page size: {}", pageSize);
			logger.info("{} more request(s) needed for full harvest", requestsToGo);
		}
		String secs = new DecimalFormat("0.00").format(time / 1000D);
		logger.info("{} request completed successfully in {} seconds.",
				request.getVerb().value(), secs);
	}

	private static void logDocCountsPerMaturity(Logger logger,
			List<AnnotatedDocument> docs) {
		int consensus = 0, contig = 0, fasta = 0, trace = 0, ab1Reversed = 0, dummy = 0;
		for (AnnotatedDocument doc : docs) {
			switch (doc.getMaturity()) {
			case AB1_REVERSED:
				++ab1Reversed;
				break;
			case CONSENSUS:
				++consensus;
				break;
			case CONTIG:
				++contig;
				break;
			case DUMMY:
				++dummy;
				break;
			case FASTA:
				++fasta;
				break;
			case TRACE:
				++trace;
				break;
			case UNKNOWN:
			default:
				break;
			}
		}
		logger.debug("---> Consensus documents: {}", consensus);
		logger.debug("---> Contig documents: {}", contig);
		logger.debug("---> Fasta documents: {}", fasta);
		logger.debug("---> Trace documents (ab1/scf): {}", trace);
		logger.debug("---> Ab1 (reversed) documents: {}", ab1Reversed);
		logger.debug("---> Dummy documents: {}", dummy);
	}

	private static void logDocCountsPerMarker(Logger logger,
			List<AnnotatedDocument> docs) {
		Map<String, MutableInt> map = new TreeMap<>();
		docs.forEach(doc -> map.computeIfAbsent(getMarker(doc), k -> new MutableInt())
				.increment());
		map.entrySet().stream()
				.forEach(e -> logger.debug("---> Marker {}: {} document(s)", e.getKey(),
						e.getValue().intValue()));
	}

	private static String getMarker(AnnotatedDocument doc) {
		return doc.getDocumentXml().getNote(MarkerCode_Seq);
	}

}
