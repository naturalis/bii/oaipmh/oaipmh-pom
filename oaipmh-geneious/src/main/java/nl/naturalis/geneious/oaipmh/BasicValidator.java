package nl.naturalis.geneious.oaipmh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.DocumentVersionCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.ExtractPlateNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.MarkerCode_Seq;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.PlatePositionCode_Samples;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.RegistrationNumberCode_Samples;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.traceRecordDiscarded;

/**
 * A {@link Validator post-filter} shared by all {@link GeneiousOaiRepository geneious repositories}. Will be applied <i>before</i> any
 * repository-specific post-filters. This filters checks that
 * <ul>
 * <li>The document_xml column contains valid XML
 * <li>The plugin_document_xml columns contains valid XML
 * <li>All required notes are present. The following notes are assumed to be required: CRSCode_CRS, ExtractIDCode_Samples, MarkerCode_Seq,
 * DocumentVersionCode_Seq, RegistrationNumberCode_Samples, ExtractPlateNumberCode_Sample
 * <li>The CRSCode_CRS note has value "true"
 * <li>The is_contig attribute in the plugin_document_xml column has value "true" in case of a DefaultAlignmentDocument
 * </ul>
 * 
 * @author Ayco Holleman
 *
 */
final class BasicValidator implements Validator {

  private static final Logger logger = LoggerFactory.getLogger(BasicValidator.class);

  // Notes that must be present irrespective of request type (specimens, dna extracts, extract plates)
  private static final Note[] alwaysRequiredNotes = new Note[] {MarkerCode_Seq,
      DocumentVersionCode_Seq,
      RegistrationNumberCode_Samples,
      ExtractPlateNumberCode_Samples,
      PlatePositionCode_Samples};

  private final ArrayList<Note> allRequiredNotes;

  private AtomicInteger numAccepted = new AtomicInteger();
  private AtomicInteger numDiscarded = new AtomicInteger();

  BasicValidator() {
    allRequiredNotes = new ArrayList<>();
    allRequiredNotes.addAll(Arrays.asList(alwaysRequiredNotes));
  }

  void addExtraRequiredNotes(List<Note> notes) {
    allRequiredNotes.addAll(notes);
  }

  @Override
  public boolean accept(AnnotatedDocument ad) {
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    // notes will never be null. See GeneiousDocumentFactory.createDocument
    for (Note note : allRequiredNotes) {
      if (!notes.isPresent(note)) {
        numDiscarded.incrementAndGet();
        if (logger.isTraceEnabled()) traceRecordDiscarded(logger, ad, "missing {}", note);
        return false;
      }
    }
    if (ad.getPluginDocumentXml() instanceof DefaultAlignmentDocument) {
      DefaultAlignmentDocument dad = (DefaultAlignmentDocument) ad.getPluginDocumentXml();
      if (!dad.isContig()) {
        numDiscarded.incrementAndGet();
        if (logger.isTraceEnabled()) traceRecordDiscarded(logger, ad, "alignment documents only considered when is_contig=\"true\"");
        return false;
      }
    }
    numAccepted.incrementAndGet();
    return true;
  }

  @Override
  public int getNumAccepted() {
    // Will only be called by main thread once all worker threads are done, so no threading issues here.
    return numAccepted.intValue();
  }

  @Override
  public int getNumDiscarded() {
    return numDiscarded.intValue();
  }

}
