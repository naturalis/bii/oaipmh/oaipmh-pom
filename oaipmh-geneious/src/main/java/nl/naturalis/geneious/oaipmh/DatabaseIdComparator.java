package nl.naturalis.geneious.oaipmh;

import java.util.Comparator;

/**
 * A {@link Comparator} for {@link AnnotatedDocument} instances that selects the instance
 * with the highest database ID.
 *
 * @author Ayco Holleman
 */
final class DatabaseIdComparator implements Comparator<AnnotatedDocument> {

  DatabaseIdComparator() { }

  @Override
  public int compare(AnnotatedDocument ad0, AnnotatedDocument ad1) {
    /* Sort in reverse order */
    return ad1.getId() - ad0.getId();
  }

}
