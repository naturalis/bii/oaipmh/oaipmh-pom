package nl.naturalis.geneious.oaipmh;

/**
 * Models the contents of the plugin_document_xml column in case the root element is
 * &lt;ABIDocument&gt;.
 *
 * @author Ayco Holleman
 */
final class Ab1Document extends PluginDocumentXml {

  Ab1Document() { }

}
