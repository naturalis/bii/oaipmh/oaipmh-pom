package nl.naturalis.geneious.oaipmh;

import java.util.Objects;
import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import nl.naturalis.geneious.oaipmh.Fields.Field;
import nl.naturalis.geneious.oaipmh.HiddenFields.HiddenField;

/**
 * Models the contents of the document_xml column within the annotated_document table.
 * 
 * @author Ayco Holleman
 *
 */
public final class DocumentXml {

  private DocumentClass documentClass;
  private HiddenFields hiddenFields;
  private Fields fields;
  private DocumentNotes notes;

  public String getHiddenField(HiddenField hiddenField) {
    if (hiddenFields == null) {
      return null;
    }
    return hiddenFields.get(hiddenField);
  }

  public void setHiddenField(HiddenField hiddenField, String value) {
    if (hiddenFields == null) {
      hiddenFields = new HiddenFields();
    }
    hiddenFields.set(hiddenField, value);
  }

  public String getField(Field field) {
    if (fields == null) {
      return null;
    }
    return fields.get(field);
  }

  public void setField(Field field, String value) {
    if (fields == null) {
      fields = new Fields();
    }
    fields.set(field, value);
  }

  public String getNote(Note note) {
    if (notes == null) {
      return null;
    }
    return notes.get(note);
  }

  public void setNote(Note note, String value) {
    if (notes == null) {
      notes = new DocumentNotes();
    }
    notes.set(note, value);
  }

  /**
   * Returns the value of the "class" attribute of the root element
   * 
   * @return
   */
  public DocumentClass getDocumentClass() {
    return documentClass;
  }

  /**
   * Set the value of the "class" attribute of the root element
   * 
   * @return
   */
  public void setDocumentClass(DocumentClass documentClass) {
    this.documentClass = documentClass;
  }

  /**
   * Returns all relevant notes present in the document_xml column.
   * 
   * @return
   */
  public DocumentNotes getNotes() {
    return notes;
  }

  /**
   * Sets all relevant notes present in the document_xml column.
   * 
   * @return
   */
  public void setNotes(DocumentNotes notes) {
    this.notes = notes;
  }

  /**
   * Gets values from elements within the &lt;hiddenFields&gt; element in the document_xml column
   * 
   * @return
   */
  public HiddenFields getHiddenFields() {
    return hiddenFields;
  }

  /**
   * Sets values from elements within the &lt;hiddenFields&gt; element in the document_xml column.
   * 
   * @param hiddenFields
   */
  public void setHiddenFields(HiddenFields hiddenFields) {
    this.hiddenFields = hiddenFields;
  }

  /**
   * Gets values from elements within the &lt;fields&gt; element in the document_xml column.
   * 
   * @return
   */
  public Fields getFields() {
    return fields;
  }

  /**
   * Sets values from elements within the &lt;fields&gt; element in the document_xml column.
   * 
   * @param fields
   */
  public void setFields(Fields fields) {
    this.fields = fields;
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    DocumentXml other = (DocumentXml) obj;
    return Objects.equals(documentClass, other.documentClass)
        && Objects.equals(hiddenFields, other.hiddenFields)
        && Objects.equals(fields, other.fields)
        && Objects.equals(notes, other.notes);
  }

  public int hashCode() {
    return Objects.hash(documentClass, hiddenFields, fields, notes);
  }

}
