package nl.naturalis.geneious.oaipmh;

import org.w3c.dom.Element;

/**
 * A factory for {@link DefaultAlignmentDocument} instances.
 * 
 * @author Ayco Holleman
 *
 */
final class DefaultAlignmentDocumentFactory {

  DefaultAlignmentDocumentFactory() {}

  /**
   * Creates a new {@link DefaultAlignmentDocument} instance.
   * 
   * @param root
   * @return
   */
  DefaultAlignmentDocument build(Element root) {
    DefaultAlignmentDocument result = new DefaultAlignmentDocument();
    if (root.hasAttribute("is_contig")) {
      String s = root.getAttribute("is_contig");
      result.setContig(s.equals("true"));
    }
    return result;
  }
}
