package nl.naturalis.geneious.oaipmh.specimens;

import nl.naturalis.geneious.oaipmh.*;
import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import nl.naturalis.geneious.oaipmh.jaxb.Geneious;
import nl.naturalis.geneious.oaipmh.jaxb.Specimen;
import nl.naturalis.geneious.oaipmh.jaxb.SpecimenUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.*;

/**
 * Handles ListRecords requests for specimens.
 *
 * @author Ayco Holleman
 */
public class SpecimenListRecordsHandler extends ListRecordsHandler {

  private static final Logger logger = LoggerFactory.getLogger(SpecimenListRecordsHandler.class);

  SpecimenListRecordsHandler(GeneiousRepositoryConfig config) {
    super(config);
  }

  @Override
  protected List<Validator> getValidators() {
    return Collections.emptyList();
  }

  @Override
  protected List<SetFilter> getSetFilters() {
    return Collections.singletonList(new SpecimenSetFilter());
  }

  @Override
  protected List<Note> getAdditionalRequiredNotes() {
    return Arrays.asList(BOLDIDCode_Bold);
  }

  @Override
  protected void setMetadata(Geneious geneious, AnnotatedDocument ad) {
    Specimen specimen = new Specimen();
    geneious.setSpecimen(specimen);
    SpecimenUnit unit = new SpecimenUnit();
    specimen.setUnit(unit);
    DocumentNotes notes = ad.getDocumentXml().getNotes();
    unit.setUnitID(notes.get(RegistrationNumberCode_Samples));
    unit.setAssociatedUnitID(notes.get(BOLDIDCode_Bold));
    unit.setUri(notes.get(BOLDURICode_FixedValue_Bold));
    String s = notes.get(NumberOfImagesCode_Bold);
    if (s != null) {
      try {
        Integer i = Integer.valueOf(s);
        unit.setMultiMediaObjectComment(i);
      } catch (NumberFormatException e) {
        logger.warn("Bad number for NumberOfImagesCode_Bold: {}", s);
      }
    }
  }

}
