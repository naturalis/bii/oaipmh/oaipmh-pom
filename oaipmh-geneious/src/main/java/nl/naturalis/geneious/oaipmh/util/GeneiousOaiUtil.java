package nl.naturalis.geneious.oaipmh.util;

import static nl.naturalis.common.StringMethods.endsWith;
import static nl.naturalis.check.CommonChecks.EQ;
import static nl.naturalis.check.CommonChecks.NULL;
import static nl.naturalis.geneious.oaipmh.DocumentClass.DefaultNucleotideGraphSequence;
import static nl.naturalis.geneious.oaipmh.DocumentClass.DefaultNucleotideSequence;
import static nl.naturalis.geneious.oaipmh.DocumentClass.DummySequence;
import static nl.naturalis.geneious.oaipmh.DocumentNotes.Note.filename;
import static nl.naturalis.geneious.oaipmh.Fields.Field.consensusSequenceLength;
import static nl.naturalis.geneious.oaipmh.Maturity.*;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.logDebug;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.traceOldestRecordDiscarded;
import static nl.naturalis.oaipmh.api.CannotDisseminateFormatError.cannotDisseminateFormat;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.check.Check;
import nl.naturalis.geneious.oaipmh.*;
import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;

/**
 * Provides common functionality for Geneious OAI repositories.
 *
 * @author Ayco Holleman
 */
public class GeneiousOaiUtil {

  /**
   * XML namespace for geneious elements (http://data.naturalis.nl/geneious).
   */
  public static final String GENEIOUS_XMLNS = "http://data.naturalis.nl/geneious";
  /**
   * XML namespace prefix for geneious elements ("geneious").
   */
  public static final String GENEIOUS_XMLNS_PREFIX = "geneious";

  private static final Logger logger = LoggerFactory
      .getLogger(GeneiousOaiUtil.class);

  private static final String[] FASTA_FILE_EXTS = {".fas", ".fasta", ".txt"};
  private static final String[] TRACE_FILE_EXTS = {".ab1", ".scf"};

  private GeneiousOaiUtil() {
  }

  /**
   * <oL>
   * <li>Make sure metadataPrefix argument is "geneious"
   * <li>Make sure we don't have the "set" argument specified by the OAIPMH
   * protocol (because we don't support it yet)
   * </ol>
   *
   * @param req
   * @throws OaiProtocolException
   */
  public static void checkRequest(OaiRequest req) throws OaiProtocolException {
    Check.notNull(req);
    Check.notNull(req.getMetadataPrefix()).is(EQ(),
        "geneious",
        () -> cannotDisseminateFormat(req));
  }

  public static Maturity getMaturity(AnnotatedDocument doc) {
    if (isConsensus(doc)) {
      return CONSENSUS;
    } else if (isContig(doc)) {
      return CONTIG;
    } else if (isFasta(doc)) {
      return FASTA;
    } else if (isAb1(doc)) {
      return TRACE;
    } else if (isAb1Reversed(doc)) {
      return AB1_REVERSED;
    } else if (isDummy(doc)) {
      return DUMMY;
    }
    return UNKNOWN;
  }

  /**
   * Returns whether or not the specified document represents a consensus document
   * (a&#46;k&#46;a&#46; consensus record). This is considered to be the case if:
   * <ol>
   * <li>The plugin_xml column has a &lt;XMLSerialisableRootElement&gt; root
   * element
   * <li>The XML in plugin_xml contains a &lt;name&gt; whose value ends with
   * "consensus sequence". In other words whatever name the Geneious user wants to
   * give to the consensus sequence record, the last two words <b>must</b> be
   * "consensus sequence" (case sensitive), otherwise this module will not be able
   * to identify consensus sequence records and the OAI-PMH it generates will not
   * be correct!
   * </ol>
   *
   * @param doc
   * @return
   */
  private static boolean isConsensus(AnnotatedDocument doc) {
    if (doc.getPluginDocumentXml() instanceof DefaultAlignmentDocument) {
      return false;
    }
    String csl = doc.getDocumentXml().getField(consensusSequenceLength);
    if (csl != null) {
      return true;
    }
    String name = doc.getName();
    if (name != null && name.endsWith("consensus sequence")) {
      if (logger.isDebugEnabled()) {
        logDebug(logger,
            doc,
            "identified as consensus sequence, but it did not specify a consensus sequence length");
      }
      return true;
    }
    return false;
  }

  /**
   * Returns whether or not the specified document represents a contig document
   * (a&#46;k&#46;a&#46; contig record). This is considered to be the case if:
   * <ol>
   * <li>The plugin_xml column has a &lt;DefaultAlignmentDocument&gt; root element
   * <li>The {@code isContig} attribute of the root element is set to "true"
   * </ol>
   *
   * @param doc
   * @return
   */
  private static boolean isContig(AnnotatedDocument doc) {
    if (doc.getPluginDocumentXml() instanceof DefaultAlignmentDocument) {
      return ((DefaultAlignmentDocument) doc.getPluginDocumentXml()).isContig();
    }
    return false;
  }

  /**
   * Returns whether or not the specified document represents a fasta document
   * (a&#46;k&#46;a&#46; fasta record). This is considered to be the case if:
   * <ol>
   * <li>The document_xml column contains a &lt;note&gt; element containing a
   * &lt;filename&gt; whose value ends with ".fas"
   * </ol>
   *
   * @param doc
   * @return
   */
  private static boolean isFasta(AnnotatedDocument doc) {
    DocumentClass dc = doc.getDocumentXml().getDocumentClass();
    String importedFrom = doc.getDocumentXml().getNote(filename);
    if (dc == DefaultNucleotideSequence) {
      if (importedFrom != null) { // must be non-derivative
        if (logger.isDebugEnabled()) {
          if (null != endsWith(importedFrom, true, FASTA_FILE_EXTS)) {
            String[] ss = StringUtils
                .split(StringUtils.removeEnd(importedFrom, "..."), ',');
            if (ss.length > 0) {
              for (String s : ss) {
                if (null != endsWith(s.trim(), true, FASTA_FILE_EXTS)) {
                  logDebug(logger, doc,
                      "identified as fasta document but it was imported from {}", s);
                  break;
                }
              }
            } else {
              logDebug(logger, doc,
                  "identified as fasta document but it was imported from {}",
                  importedFrom);
            }
          }
        }
        return true;
      }
    } else if (logger.isDebugEnabled()
        && null != endsWith(importedFrom, true, FASTA_FILE_EXTS)) {
      logDebug(logger, doc,
          "disqualified as fasta document but it was imported from {}",
          importedFrom);
    }
    return false;
  }

  /**
   * Returns whether or not the specified document is an AB1 document.
   *
   * @param doc
   * @return
   */
  private static boolean isAb1(AnnotatedDocument doc) {
    DocumentClass dc = doc.getDocumentXml().getDocumentClass();
    String importedFrom = doc.getDocumentXml().getNote(filename);
    if (dc == DefaultNucleotideGraphSequence) {
      if (importedFrom != null) { // must be a non-derived document
        if (logger.isDebugEnabled()
            && null != endsWith(importedFrom, true, TRACE_FILE_EXTS)) {
          logDebug(logger, doc,
              "identified as trace file (ab1/scf) but it was imported from {}",
              importedFrom);
        }
        return true;
      }
    } else if (logger.isDebugEnabled()
        && null != endsWith(importedFrom, true, TRACE_FILE_EXTS)) {
      logDebug(logger, doc,
          "disqualified as trace file (ab1/scf) but it was imported from {}",
          importedFrom);
    }
    return false;
  }

  /**
   * Returns whether or not the specified document is an reversed AB1 document.
   *
   * @param doc
   * @return
   */
  private static boolean isAb1Reversed(AnnotatedDocument doc) {
    DocumentClass dc = doc.getDocumentXml().getDocumentClass();
    if (dc == DefaultNucleotideGraphSequence) {
      String importedFrom = doc.getDocumentXml().getNote(filename);
      String name = doc.getName();
      if (importedFrom == null) { // must be a derived document
        return null != endsWith(name, true, new String[] {".ab1 (reversed)"});
      } else if (logger.isDebugEnabled()
          && null != endsWith(name, true, new String[] {".ab1 (reversed)"})) {
        logDebug(logger, doc,
            "disqualified as ab1 (reversed) document, but its name was {}", name);
      }
    }
    return false;
  }

  /**
   * Returns whether or not the specified document is an AB1 document.
   *
   * @param doc
   * @return
   */
  private static boolean isDummy(AnnotatedDocument doc) {
    if (doc.getDocumentXml().getDocumentClass() == DummySequence) { // plugin V2
      return true;
    }
    return null != endsWith(doc.getName(), true, new String[] {".dum"});
  }

  public static boolean sameNote(DocumentNotes notes0, DocumentNotes notes1,
      DocumentNotes.Note note) {
    String s0 = notes0.get(note);
    String s1 = notes1.get(note);
    return Objects.equals(s0, s1);
  }

  public static void discardOldest(AnnotatedDocument doc0, AnnotatedDocument doc1) {
    if (doc1.getId() > doc0.getId()) {
      if (!doc0.isDiscarded() && logger.isTraceEnabled()) {
        traceOldestRecordDiscarded(logger, doc0, doc1);
      }
      doc0.discard();
    } else {
      if (!doc1.isDiscarded() && logger.isTraceEnabled()) {
        traceOldestRecordDiscarded(logger, doc1, doc0);
      }
      doc1.discard();
    }
  }

}
