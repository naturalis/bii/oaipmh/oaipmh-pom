/**
 * 
 * Classes for the DNA specimens repository.
 * 
 * @author Ayco Holleman
 *
 */
package nl.naturalis.geneious.oaipmh.specimens;