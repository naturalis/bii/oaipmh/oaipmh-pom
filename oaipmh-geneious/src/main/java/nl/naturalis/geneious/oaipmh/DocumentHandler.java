package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.util.LogUtil;
import org.klojang.check.Check;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;
import static org.klojang.check.CommonChecks.eq;

/**
 * Parses the XML columns in the annotated_document table, and then validates the record.
 * The class is meant to process the entire query result at once, spawning a new thread
 * for every 50 records. Note that all threads work on the same list of records, but this
 * is harmless as they get all passed a different segment to work on.
 *
 * @author Ayco Holleman
 */
final class DocumentHandler {

  private static final int batchSize = 50;
  private static final Logger logger = LoggerFactory.getLogger(DocumentHandler.class);

  private static final DocumentXmlParser parser1 = new DocumentXmlParser();
  private static final PluginDocumentXmlParser parser2 = new PluginDocumentXmlParser();

  private final List<Validator> validators;
  private final CountDownLatch latch;
  private final AnnotatedDocument[] docs;

  private int idx = 0;

  DocumentHandler(List<Validator> validators, int recordCount) {
    this.validators = validators;
    this.latch = new CountDownLatch(recordCount / batchSize);
    this.docs = new AnnotatedDocument[recordCount];
  }

  void handleDocument(AnnotatedDocument doc) {
    docs[idx++] = doc;
    if (idx % batchSize == 0) {
      final int from = idx - batchSize;
      final int to = idx;
      Executors.newCachedThreadPool().execute(() -> {
        process(from, to);
        latch.countDown();
      });
    }
  }

  List<AnnotatedDocument> getValidDocuments() {
    Check.that(idx).is(eq(),
          docs.length,
          "Not all documents in result set processed yet");
    process(idx - (idx % batchSize), idx);
    try {
      latch.await();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
    return Arrays.stream(docs).filter(not(AnnotatedDocument::isDiscarded)).collect(
          toList());
  }

  private void process(int from, int to) {
    if (from != 0 && from % 2000 == 0) {
      logger.trace("Number of documents processed: {}", from);
    }
    for (int i = from; i < to; ++i) {
      process(docs[i]);
    }
  }

  private void process(AnnotatedDocument doc) {
    try {
      parser1.parseDocumentXml(doc);
      if (!doc.isDiscarded()) {
        parser2.parsePluginDocumentXml(doc);
        if (!doc.isDiscarded()) {
          if (doc.getMaturity() == Maturity.UNKNOWN) {
            doc.discard();
            if (logger.isDebugEnabled()) {
              LogUtil.logDebug(logger,
                    doc,
                    "unable to determine document type (consensus, contig, etc.)");
            }
          } else {
            validators.stream()
                  .filter(v -> !v.accept(doc))
                  .findFirst()
                  .ifPresent(v -> doc.discard());
          }
        }
      }
    } catch (Exception e) {
      doc.discard();
      LogUtil.warnRecordDiscarded(logger, doc, "{}", e);
    }
  }

}
