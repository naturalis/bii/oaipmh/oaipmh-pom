/**
 * 
 * Classes for the Sample Plates repository.
 * 
 * @author Ayco Holleman
 *
 */
package nl.naturalis.geneious.oaipmh.plates;