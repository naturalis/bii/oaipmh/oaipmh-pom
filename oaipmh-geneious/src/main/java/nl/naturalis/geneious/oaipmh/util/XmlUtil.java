package nl.naturalis.geneious.oaipmh.util;

import static nl.naturalis.common.StringMethods.ifBlank;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import nl.naturalis.oaipmh.api.OaiRuntimeException;

/**
 * Simple DOM helper methods.
 * 
 * @author Ayco Holleman
 *
 */
public class XmlUtil {

  /**
   * Returns the root element.
   * 
   * @param xml
   * @return
   * @throws SAXParseException
   */
  public static Element getDocumentElement(String xml) throws SAXParseException {
    return getDocumentElement(xml, false, false);
  }

  /**
   * Returns the root element.
   * 
   * @param xml
   * @param namespaceAware
   * @param validating
   * @return
   * @throws SAXParseException
   */
  public static Element getDocumentElement(String xml, boolean namespaceAware, boolean validating) throws SAXParseException {
    DocumentBuilder docBuilder = getDocumentBuilder(namespaceAware, validating);
    Document doc;
    try {
      doc = docBuilder.parse(IOUtils.toInputStream(xml, "UTF-8"));
    } catch (SAXParseException e) { // We will handle this one
      throw e;
    } catch (IOException | SAXException e) { // Over and out
      throw new OaiRuntimeException(e);
    }
    return doc.getDocumentElement();
  }

  /**
   * Returns all child elements of the specified element which have the specified tag name. This method returns {@code null} if the
   * specified element has no child elements with the specified tag name.
   * 
   * @param parent
   * @return
   */
  public static List<Element> getChildren(Element parent, String tagName) {
    NodeList nl = parent.getElementsByTagName(tagName);
    Element[] elems = new Element[nl.getLength()];
    for (int i = 0; i < nl.getLength(); ++i) {
      elems[i] = (Element) nl.item(i);
    }
     return Arrays.asList(elems);
  }


  /**
   * Returns the first child element of the specified node which has the specified tag name.
   * 
   * @param parent
   * @param tagName
   * @return
   */
  public static Element getChild(Element parent, String tagName) {
    NodeList nl = parent.getElementsByTagName(tagName);
    return nl.getLength() == 0 ? null : (Element) nl.item(0);
  }

  /**
   * Get the value (i&#46;e&#46; text content) of the first child of the specified element which has the specified tag name. This method
   * returns {@code null} if the specified element has no child element with the specified tag name.
   * 
   * @param parent
   * @param tagName
   * @return
   */
  public static String getValue(Element parent, String tagName) {
    Element e = getChild(parent, tagName);
    return e == null ? null : ifBlank(e.getTextContent(), null);
  }

  /**
   * Returns the first element descending from {@code ancestor} that has the specified {@code tagName}, or null if there is no descendant
   * with the provided tag.
   * 
   * @param ancestor
   * @param tagName
   * @return
   */
  public static Element getDescendant(Element ancestor, String tagName) {
    NodeList nl = ancestor.getElementsByTagName(tagName);
    return nl.getLength() == 0 ? null : (Element) nl.item(0);
  }

  private static DocumentBuilder getDocumentBuilder(boolean namespaceAware, boolean validating) {
    DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
    bf.setNamespaceAware(namespaceAware);
    bf.setValidating(validating);
    try {
      return bf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      throw new RuntimeException(e);
    }
  }

}
