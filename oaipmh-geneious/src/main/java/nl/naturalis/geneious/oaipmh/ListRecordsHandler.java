package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.DocumentNotes.Note;
import nl.naturalis.geneious.oaipmh.jaxb.Geneious;
import nl.naturalis.oaipmh.api.OaiProtocolException;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.RepositoryException;
import org.apache.commons.lang3.time.StopWatch;
import org.klojang.check.Check;
import org.openarchives.oai._2.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static nl.naturalis.geneious.oaipmh.util.GeneiousOaiUtil.checkRequest;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.logSummary;
import static nl.naturalis.geneious.oaipmh.util.LogUtil.logValidationResults;
import static nl.naturalis.oaipmh.api.BadResumptionTokenError.recordOffsetOutOfRange;
import static nl.naturalis.oaipmh.api.NoRecordsMatchError.noRecordsMatch;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createDefaultResumptionToken;
import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHSkeleton;
import static nl.naturalis.oaipmh.api.util.ObjectFactories.OAI;
import static org.klojang.check.CommonChecks.*;
import static org.klojang.check.CommonProperties.size;

/**
 * Abstract base class for handlers for the ListRecords protocol request.
 *
 * <h3>Filtering and sorting</h3>
 *
 * <p>takesPrecedenceOver This describes how database documents are filtered and
 * sorted as part of the OAI-PMH generation process:
 *
 * <ol>
 *   <li>First, a SQL query is issued that only selects documents from the annotated_document table
 *       whose reference_count column equals 0. See {@link ReferenceComparator} for an explanantion.
 *       No ORDER BY clause is used; filters applied subsequently cannot rely on the records being
 *       sorted in any particular order.
 *   <li>Then the {@link SharedPreFilter} is applied. See {@link IAnnotatedDocumentPreFilter here}
 *       for an explanation of pre-filters.
 *   <li>Then all repository-specific pre-filters are applied. Any number of additional pre-filters
 *       can be defined for specimens, DNA extracts and DNA extract plates.
 *   <li>Then the {@link BasicValidator} is applied. See {@link Validator here} for an explanation
 *       of post-filters.
 *   <li>Then all repository-specific post-filters are applied.
 *   <li>Then the {@link DocumentVersionSetFilter} is applied. See {@link SetFilter here} for an
 *       explanation of set filters.
 *   <li>Then all repository-specific set filters are applied.
 *   <li>Then the remaining documents are sorted in descending order of their database ID.
 * </ol>
 *
 * @author Ayco Holleman
 */
public abstract class ListRecordsHandler {

  private static final Logger logger = LoggerFactory.getLogger(ListRecordsHandler.class);

  protected final GeneiousRepositoryConfig config;

  private final List<Validator> validators = new ArrayList<>(4);
  private final List<SetFilter> setFilters = new ArrayList<>(4);

  public ListRecordsHandler(GeneiousRepositoryConfig config) {
    this.config = config;
    BasicValidator basicValidator = new BasicValidator();
    basicValidator.addExtraRequiredNotes(getAdditionalRequiredNotes());
    validators.add(basicValidator);
    validators.addAll(getValidators());
    setFilters.addAll(getSetFilters());
  }

  public OAIPMHtype handleRequest(OaiRequest request)
        throws RepositoryException, OaiProtocolException {
    StopWatch stopwatch = StopWatch.createStarted();
    checkRequest(request);
    DbQueryExecutor queryExecutor = new DbQueryExecutor(request, config, validators);
    List<AnnotatedDocument> docs = queryExecutor.getValidDocuments();
    // We're done with the loader and it may have a very
    // bulky state by now - allow garbage collection
    queryExecutor = null;
    if (logger.isDebugEnabled()) {
      logValidationResults(logger, docs, validators);
    }
    if (logger.isDebugEnabled()) {
      logger.debug("Comparing and selecting documents");
    }
    docs = applySetFilters(docs);
    int pageSize = request.getPageSize(config);
    int firstRecord = request.getRecordOffset(pageSize);
    Check.that(docs).has(size(), gt(), 0, noRecordsMatch());
    Check.that(firstRecord).is(lt(), docs.size(), recordOffsetOutOfRange());
    // Contrary to firstPageThenFilter not absolutely necessary, but still nice:
    logger.debug("Sorting documents (oldest first)");
    Collections.sort(docs, Comparator.comparing(AnnotatedDocument::getId));
    logger.debug("Generating XML");
    OAIPMHtype oaipmh = createOAIPMHSkeleton(request);
    ListRecordsType records = OAI.createListRecordsType();
    oaipmh.setListRecords(records);
    int lastRecord = request.getStartIndexOfNextPage(pageSize, docs.size());
    docs.subList(firstRecord, lastRecord).forEach(doc -> addRecord(records, doc));
    if (!request.isHarvestComplete(pageSize, docs.size())) {
      records.setResumptionToken(
            createDefaultResumptionToken(request, config, docs.size()));
    }
    stopwatch.stop();
    logSummary(logger, request, stopwatch, docs, pageSize);
    return oaipmh;
  }

  private List<AnnotatedDocument> applySetFilters(List<AnnotatedDocument> docs) {
    int remaining = docs.size();
    for (SetFilter filter : setFilters) {
      docs = filter.filter(docs);
      if (logger.isDebugEnabled()) {
        String name = filter.getClass().getSimpleName();
        logger.debug("Number of documents discarded by {}: {}",
              name,
              remaining - docs.size());
        logger.debug("Number of documents remaining: {}", remaining = docs.size());
      }
    }
    return docs;
  }

  private void addRecord(ListRecordsType xml, AnnotatedDocument ad) {
    RecordType record = OAI.createRecordType();
    xml.getRecord().add(record);
    record.setHeader(createHeader(ad));
    record.setMetadata(createMetadata(ad));
  }

  private static HeaderType createHeader(AnnotatedDocument ad) {
    HeaderType header = OAI.createHeaderType();
    header.setIdentifier(String.valueOf(ad.getId()));
    if (ad.getHighResModifiedDate() == null) {
      header.setDatestamp(ISO_LOCAL_DATE.format(ad.getModified()));
    } else {
      header.setDatestamp(ISO_INSTANT.format(ad.getHighResModifiedDate()));
    }
    return header;
  }

  private MetadataType createMetadata(AnnotatedDocument ad) {
    MetadataType metadata = OAI.createMetadataType();
    Geneious geneious = new Geneious();
    metadata.setAny(geneious);
    setMetadata(geneious, ad);
    return metadata;
  }

  /**
   * Template method to be implemented by subclasses: provide extra post-filters for the
   * particular resource the subclass deals with. The extra post-filters are applied
   * <i>after</i> the {@link BasicValidator}.
   */
  protected abstract List<Validator> getValidators();

  /**
   * Template method to be implemented by subclasses: provide extra set filters for the
   * particular resource the subclass deals with.
   */
  protected abstract List<SetFilter> getSetFilters();

  /**
   * Template method to be implemented by subclasses: provide a list of document notes
   * whose presence must be verified <i>in addition to</i> the notes whose presence will
   * always be verified.
   *
   * @return
   */
  protected abstract List<Note> getAdditionalRequiredNotes();

  /**
   * Template method to be implemented by subclasses: provide the child element of the
   * &lt;geneious&gt; element.
   *
   * @param geneious
   * @param ad
   */
  protected abstract void setMetadata(Geneious geneious, AnnotatedDocument ad);

}
