package nl.naturalis.geneious.oaipmh;

/**
 * Filters records from the annotated_document table <i>after</i> they have been converted
 * to {@link AnnotatedDocument} instances. Post-filters have access to structured data
 * extracted from the document_xml and plugin_document_xml columns. Post-filters are
 * allowed to maintain state: one instance is used for all records coming back from the
 * database.
 *
 * @author Ayco Holleman
 * @see IAnnotatedDocumentPreFilter
 * @see SetFilter
 * @see ListRecordsHandler
 */
public interface Validator {

  /**
   * Whether or not to filter out the provided {@link AnnotatedDocument}.
   *
   * @param ad
   * @return
   */
  boolean accept(AnnotatedDocument ad);

  /**
   * The number of record passing the filter.
   *
   * @return
   */
  int getNumAccepted();

  /**
   * The number of records rejected by the filter.
   *
   * @return
   */
  int getNumDiscarded();

}
