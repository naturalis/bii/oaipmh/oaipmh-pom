package nl.naturalis.geneious.oaipmh;

import nl.naturalis.geneious.oaipmh.HiddenFields.HiddenField;
import nl.naturalis.geneious.oaipmh.util.LogUtil;
import nl.naturalis.geneious.oaipmh.util.XmlUtil;
import org.klojang.util.StringMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xml.sax.SAXParseException;

import java.time.Instant;

final class DocumentXmlParser {

  private static final Logger logger = LoggerFactory.getLogger(DocumentXmlParser.class);

  DocumentXmlParser() { }

  /**
   * Creates a new {@link DocumentXml} instance from the XML in the document_xml column,
   * sets the instance on the {@link AnnotatedDocument} and then nullifies the documentXml
   * field of the <code>AnnotatedDocument</code> (just to free up as much space as quickly
   * as possible).
   *
   * @param xml
   * @return
   */
  void parseDocumentXml(AnnotatedDocument doc) {
    if (StringMethods.isBlank(doc.getDocumentXmlString())) {
      /*
       * That's almost paradoxical because the AnnotatedDocument was obtained from via a
       * query on, eh, the document_xml column (albeit indirectly via the
       * boolean_search_field_value). But let's not allow this to blow up the entire
       * harvest.
       */
      doc.discard();
      LogUtil.warnRecordDiscarded(logger,
            doc,
            "column plugin_document_xml must not be empty");
      return;
    }
    Element root;
    try {
      root = XmlUtil.getDocumentElement(doc.getDocumentXmlString());
    } catch (SAXParseException e) {
      doc.discard();
      LogUtil.warnRecordDiscarded(logger,
            doc,
            "error while parsing contents of document_xml column ({})",
            e);
      return;
    } finally {
      doc.setDocumentXmlString(null);  // allow raw XML string to be garbage collected as quickly as possible
    }
    DocumentNotes notes = getDocumentNotes(root);
    if (notes == null) {
      doc.discard();
      LogUtil.warnRecordDiscarded(logger,
            doc,
            "no <note> elements present in document_xml column");
      return;
    }
    DocumentXml dx = new DocumentXml();
    dx.setDocumentClass(getDocumentType(root));
    dx.setFields(getDocumentFields(root));
    dx.setNotes(notes);
    dx.setHiddenFields(getDocumentHiddenFields(root));
    String s = dx.getHiddenField(HiddenField.override_modified_date);
    if (s != null) {
      doc.setHighResModifiedDate(Instant.ofEpochMilli(Long.parseLong(s)));
    } else {
      doc.setHighResModifiedDate(Instant.from(doc.getModified()));
    }
    doc.setDocumentXml(dx);
  }

  private static DocumentClass getDocumentType(Element root) {
    String s = root.getAttribute("class");
    DocumentClass documentClass = DocumentClass.parse(s);
    return documentClass;
  }

  private static DocumentNotes getDocumentNotes(Element root) {
    Element notesElement = XmlUtil.getChild(root, "notes");
    if (notesElement == null) {
      return null;
    }
    DocumentNotes notes = null;
    String val;
    for (DocumentNotes.Note note : DocumentNotes.Note.values()) {
      Element e = XmlUtil.getDescendant(notesElement, note.name());
      if (e != null && !(val = e.getTextContent().strip()).isEmpty()) {
        (notes = notes == null ? new DocumentNotes() : notes).set(note, val);
      }
    }
    return notes;
  }

  private static Fields getDocumentFields(Element root) {
    Element fieldsElement = XmlUtil.getChild(root, "fields");
    if (fieldsElement == null) {
      return null;
    }
    Fields fields = null;
    String val;
    for (Fields.Field field : Fields.Field.values()) {
      Element e = XmlUtil.getChild(fieldsElement, field.name());
      if (e != null && !(val = e.getTextContent().strip()).isEmpty()) {
        (fields = fields == null ? new Fields() : fields).set(field, val);
      }
    }
    return fields;
  }

  private static HiddenFields getDocumentHiddenFields(Element root) {
    Element hfElement = XmlUtil.getChild(root, "hiddenFields");
    if (hfElement == null) {
      return null;
    }
    HiddenFields fields = null;
    String val;
    for (HiddenField field : HiddenField.values()) {
      Element e = XmlUtil.getChild(hfElement, field.name());
      if (e != null && !(val = e.getTextContent().strip()).isEmpty()) {
        (fields = fields == null ? new HiddenFields() : fields).set(field, val);
      }
    }
    return fields;
  }

}
