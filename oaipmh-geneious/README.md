# geneious-oaipmh

A Java component for harvesting Geneious data using the OAI-PMH protocol. CRS harvests this data in order to enrich its specimen records with DNA-related information. The geneious-oaipmh library is meant to run inside the Naturalis OAI-PMH server, which exposes the OAI-PMH through a REST interface.

The geneious-oaipmh library has a dependency on the oaipmh-api library. It provides three implementations of the OaiRepository interface within the oaipmh-api library:
- An OAI repository for harvesting specimen-related data
- An OAI repository for harvesting DNA sample data
- An OAI repository for harvesting DNA sample plate data

