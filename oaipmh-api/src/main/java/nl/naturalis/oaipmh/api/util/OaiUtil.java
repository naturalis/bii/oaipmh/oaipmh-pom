package nl.naturalis.oaipmh.api.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.OaiRuntimeException;
import nl.naturalis.oaipmh.api.RepositoryConfig;
import nl.naturalis.oaipmh.api.ResumptionTokenGenerator;
import org.klojang.check.Check;
import org.openarchives.oai._2.OAIPMHerrorType;
import org.openarchives.oai._2.OAIPMHtype;
import org.openarchives.oai._2.RequestType;
import org.openarchives.oai._2.ResumptionTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static nl.naturalis.common.CollectionMethods.implode;
import static nl.naturalis.oaipmh.api.util.ObjectFactories.OAI;
import static org.klojang.check.CommonChecks.inArray;
import static org.klojang.util.ArrayMethods.pack;

/**
 * Utility class providing common functionality for OAI reposities.
 *
 * @author Ayco Holleman
 */
public class OaiUtil {

  private static final Logger logger = LoggerFactory.getLogger(OaiUtil.class);

  private static final ObjectMapper om = new ObjectMapper();

  private OaiUtil() { }

  /**
   * Converts the provided <code>Map</code> into an instance of the provided class. Mainly
   * here so that repository implementations can convert the raw
   * <code>Map</code> object, handed over by the OAIPMH server, into a more strongly
   * typed object without requiring a dependency on Jackson.
   *
   * @param <T>
   * @param configData
   * @param configClass
   * @return
   */
  public static <T extends RepositoryConfig> T parseConfig(Map<String, Object> configData,
        Class<T> configClass) {
    return om.convertValue(configData, configClass);
  }

  /**
   * Prints the configuration for an OAI repository. Use with care, as it will contain
   * database access data.
   *
   * @param config
   * @param <T>
   */
  public static <T extends RepositoryConfig> String getConfigAsString(T config) {
    try {
      return om.writeValueAsString(config);
    } catch (Throwable t) {
      logger.error("unable to print config", t);
      return "unable to print " + config.getClass();
    }
  }

  /**
   * Creates the outer XML elements of an OAI-PMH response. These are always the same,
   * whatever the repository, so all repositories can use this method as a starting point
   * for composing OAI-PMH content.
   *
   * @param request
   * @return
   */
  public static OAIPMHtype createOAIPMHSkeleton(OaiRequest request) {
    OAIPMHtype xml = ObjectFactories.OAI.createOAIPMHtype();
    xml.setResponseDate(newXMLGregorianCalendar());
    xml.setRequest(echoRequest(request));
    return xml;
  }

  /**
   * Creates a full-blown OAI-PMH response meant to report back one or more errors.
   *
   * @param request
   * @param errors
   * @return
   */
  public static OAIPMHtype createOAIPMHErrorResponse(OaiRequest request,
        List<OAIPMHerrorType> errors) {
    OAIPMHtype xml = createOAIPMHSkeleton(request);
    errors.forEach(e -> {
      logger.error(e.getCode().name() + ": " + e.getValue());
      xml.getError().add(e);
    });
    return xml;
  }

  /**
   * Generates the &lt;resumptionToken&gt; element within the OAI-PMH output using the
   * {@link ResumptionTokenGenerator#getDefaultTokenGenerator() default token generator}.
   *
   * @param request The OAI-PMH request containing the basic information encoded
   *       into the resumption token
   * @param pageSize The number of records per request
   * @param numRecords The total number of records satisfying the request
   *       (disregarding the SQL LIMIT clause).
   * @return
   */
  public static ResumptionTokenType createDefaultResumptionToken(OaiRequest request,
        RepositoryConfig config,
        int numRecords) {
    ResumptionTokenType xml = OAI.createResumptionTokenType();
    xml.setCompleteListSize(BigInteger.valueOf(numRecords));
    int pageSize = request.getPageSize(config);
    int offset = request.getPage() * pageSize;
    xml.setCursor(BigInteger.valueOf(offset));
    ResumptionTokenGenerator rtg = ResumptionTokenGenerator.getDefaultTokenGenerator();
    String token = rtg.nextResumptionToken(request);
    xml.setValue(token);
    return xml;
  }

  /**
   * Against protocol, we allow the "set" parameter to be multivalued. We still need to
   * populate the JAXB object generated from the XSD with a String though, not with a
   * {@code List<String>}. Thus we implode the list here using "|" as separator. Note that
   * this is a different separator then we use when encoding and decoding the list in the
   * resumption token, but that doesn't matter. There we just have to make sure that we
   * use the same separator for encoding and decoding, and use a somewhat unlikely
   * separator string. Here we just need to make it kind of look good.
   */
  private static RequestType echoRequest(OaiRequest request) {
    RequestType echo = ObjectFactories.OAI.createRequestType();
    echo.setValue(request.getRequestUri().toString());
    echo.setVerb(request.getVerb());
    if (request.getResumptionToken() != null) {
      echo.setResumptionToken(request.getResumptionToken());
    } else {
      echo.setMetadataPrefix(request.getMetadataPrefix());
      if (request.getFrom() != null) {
        echo.setFrom(echoDate(request.getFrom(), request.getDateFormatFrom()));
      }
      if (request.getUntil() != null) {
        echo.setUntil(echoDate(request.getUntil(), request.getDateFormatUntil()));
      }
      if (request.getSet() != null) {
        echo.setSet(implode(request.getSet(), ";"));
      }
      echo.setIdentifier(request.getIdentifier());
    }
    return echo;
  }

  private static String echoDate(Instant date, DateTimeFormatter formatter) {
    Check.notNull(formatter)
          .is(inArray(),
                pack(ISO_LOCAL_DATE, ISO_INSTANT),
                "Unexpected date/time formatter");
    if (formatter == ISO_LOCAL_DATE) {
      return OaiDateParser.print(LocalDate.ofInstant(date, ZoneOffset.UTC));
    }
    return ISO_INSTANT.format(date);
  }

  private static XMLGregorianCalendar newXMLGregorianCalendar() {
    try {
      XMLGregorianCalendar xgc = DatatypeFactory.newInstance()
            .newXMLGregorianCalendar(new GregorianCalendar());
      xgc.setMillisecond(DatatypeConstants.FIELD_UNDEFINED);
      // Hack:
      xgc.setTimezone(0);
      return xgc.normalize();
    } catch (DatatypeConfigurationException e) {
      throw new OaiRuntimeException(e);
    }
  }

}
