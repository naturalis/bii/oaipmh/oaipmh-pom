package nl.naturalis.oaipmh.api;

import org.klojang.util.ArrayMethods;

/**
 * Thrown upon failure in the initialization phase of an
 * {@link StandAloneRepository OAI repository} or
 * {@link RepositoryGroup repository group}.
 *
 * @author Ayco Holleman
 */
public class RepositoryInitializationException extends RepositoryException {

  public static RepositoryInitializationException noSuchRepository(String name) {
    return configurationError("Missing repository: \"%s\"", name);
  }

  public static RepositoryInitializationException duplicateRepositoryName(String name) {
    return configurationError("Duplicate repository name: " + name);
  }

  public static RepositoryInitializationException missingProperty(String name) {
    return configurationError("Missing property: " + name);
  }

  public static RepositoryInitializationException implementationError(ManagedRepository repo) {
    String msg = String.format("Bad repository: %s", repo.getClass().getName());
    return new RepositoryInitializationException(msg);
  }

  public static RepositoryInitializationException implementationError(ManagedRepository repo,
        String msg,
        Object... msgArgs) {
    msg = "Bad repository: %s. " + msg;
    msgArgs = ArrayMethods.prefix(msgArgs, repo.getClass().getName());
    return new RepositoryInitializationException(String.format(msg, msgArgs));
  }

  public static RepositoryInitializationException configurationError(String msg,
        Object... msgArgs) {
    return new RepositoryInitializationException("Server configuration error: " + String.format(
          msg,
          msgArgs));
  }

  public RepositoryInitializationException(String message) {
    super(message);
  }

  public RepositoryInitializationException(Throwable cause) {
    super(cause);
  }

  public RepositoryInitializationException(String message, Throwable cause) {
    super(message, cause);
  }

}
