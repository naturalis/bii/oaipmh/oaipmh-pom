package nl.naturalis.oaipmh.api;

import nl.naturalis.oaipmh.api.util.OaiUtil;

/**
 * Interface defining the capacity to generate a resumption token from the information in
 * an {@link OaiRequest}. This interface is here only for completeness' sake. OAI
 * repositories don't need to implement it, even if they use non-default resumption
 * tokens, in which case they
 * <i>would</i> need to implement {@link ResumptionTokenParser}. This is because, while
 * OAIPMH server needs to know how to parse a
 * resumption token, generating one is left to the OAI repository itself. Nevertheless,
 * classes that implement {@link ResumptionTokenParser} should probably also implement
 * {@link ResumptionTokenGenerator}, because parsing a resumption token mirrors generating
 * one.
 *
 * @author Ayco Holleman
 */
public interface ResumptionTokenGenerator {

  /**
   * Returns the default resumption token generator, which encodes the from, until and
   * metadataPrefix arguments as well as the extra-protocolar page parameter, which is the
   * requested page within the result set. Note that it does not encode the absolute
   * record offset within the result set because only the repositories themselves know how
   * many records to serve per request (the page size). However, repository
   * implementations can use the utility method
   * {@link OaiUtil#createDefaultResumptionToken(OaiRequest, int, int)
   * OaiUtil.createDefaultResumptionToken} to generate the &lt;resumptionToken&gt;
   * element.
   *
   * @return
   */
  static ResumptionTokenGenerator getDefaultTokenGenerator() {
    return new ResumptionToken();
  }

  /**
   * Generate a resumption token for the request <i>following</i> the provided request.
   *
   * @param request The create from which to create the token
   * @return The resumption token to be used by clients in the next request.
   */
  String nextResumptionToken(OaiRequest request);

}
