package nl.naturalis.oaipmh.api;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import nl.naturalis.oaipmh.api.util.BuildInfo;
import nl.naturalis.oaipmh.api.util.OaiUtil;
import nl.naturalis.oaipmh.api.util.SimpleHealthCheck;

/**
 * Specifies what it takes to be managed by the OAI-PMH server. Instances of
 * {@link StandAloneRepository} and {@link RepositoryGroup} are server managed objects,
 * but plain implementations of {@link OaiRepository} are not. The
 * <code>OaiRepository</code> interface only codifies the OAI-PMH protocol. On the other
 * hand, a <code>RepositoryGroup</code> is a server managed object but does not directly
 * implement <code>OaiRepository</code>. It only provides access (by name) to OAI
 * repositories. Theoretically you could present the server with a
 * <code>ManagedRepository</code> object that is neither a
 * <code>StandAloneRepository</code> nor a <code>RepositoryGroup</code>, but the server
 * startup code will prevent this (causing a {@link RepositoryInitializationException}).
 *
 * @author Ayco Holleman
 */
public interface ManagedRepository {

  /**
   * Returns the name by which the repository (group) is to be identified. The name serves
   * two purposes:
   * <ol>
   * <li>It corresponds to the first path element after the base URL of an OAIPMH request. Thus it enables the OAIPMH server to route a
   * request to a repository.
   * <li>It corrersponds to a subsection in the server configuration file dedicated to the configuration for the repository, thus enabling
   * the OAIPMH server to configure the repository.
   * </ol>
   *
   * @return
   */
  String getName();

  /**
   * The a <code>BuildInfo</code> instance providing information about the current version
   * of the repository.
   *
   * @return
   */
  BuildInfo getBuildInfo();

  /**
   * Configures the repository (group) using the provided configuration data. The provided
   * <code>Map</code> corresponds to an entry in the server configuration file with the
   * same {@link #getName() name} as the repository. The OAIPMH API and server have no
   * opinion about how an OAI repository should configure itself. Hence the server can
   * only pass a raw <code>Map</code> object to the repository. However, if the map is in
   * fact structured like the {@link RepositoryConfig} or {@link DbRepositoryConfig}
   * class, the API and server can provide more services to the repository. The conversion
   * of the map to a {@link RepositoryConfig} or {@link DbRepositoryConfig} instance is
   * left to implementations of the API, but implementations can use
   * {@link OaiUtil#parseConfig(Map, Class) OaiUtil.parseConfig} to do the conversion.
   *
   * @param config
   */
  void configure(Map<String, Object> config) throws RepositoryInitializationException;

  /**
   * Provides health checks probing the state of the repository. The default
   * implementation returns an empty list. This method is called once, just after
   * {@link #configure(Map) configure}.
   *
   * @return
   */
  default List<SimpleHealthCheck> getHealthChecks() {
    return Collections.emptyList();
  }

  /**
   * Called just once for any instance, just before it goes out of scope.
   */
  void tearDown();

}
