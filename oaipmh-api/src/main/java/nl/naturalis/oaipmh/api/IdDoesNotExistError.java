package nl.naturalis.oaipmh.api;

import org.openarchives.oai._2.OAIPMHerrorType;

import java.util.function.Supplier;

import static org.openarchives.oai._2.OAIPMHerrorcodeType.ID_DOES_NOT_EXIST;

/**
 * Convenience class narrowing the JAXB {@code OAIPMHerrorType} class to one for
 * IdDoesNotExist errors.
 *
 * @author Ayco Holleman
 */
public class IdDoesNotExistError extends OAIPMHerrorType {

  public static Supplier<OaiProtocolException> idDoesNotExist(String id) {
    return () -> new OaiProtocolException(new IdDoesNotExistError(id));
  }

  public IdDoesNotExistError(String id) {
    super();
    this.code = ID_DOES_NOT_EXIST;
    this.value = "No such id: \"" + id + "\"";
  }

  public IdDoesNotExistError(OaiRequest request) {
    this(request.getIdentifier());
  }

}
