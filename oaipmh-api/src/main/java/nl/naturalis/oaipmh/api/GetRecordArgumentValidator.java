package nl.naturalis.oaipmh.api;

import org.openarchives.oai._2.VerbType;

import java.util.EnumSet;

import static nl.naturalis.oaipmh.api.Argument.IDENTIFIER;
import static nl.naturalis.oaipmh.api.Argument.METADATA_PREFIX;

/**
 * An {@link ArgumentValidator} for the {@link VerbType#LIST_RECORDS LIST_RECORDS}
 * protocol request.
 *
 * @author Ayco Holleman
 */
final class GetRecordArgumentValidator extends ArgumentValidator {

  public GetRecordArgumentValidator() { }

  @Override
  protected EnumSet<Argument> getRequiredArguments() {
    return EnumSet.of(METADATA_PREFIX, IDENTIFIER);
  }

  @Override
  protected EnumSet<Argument> getOptionalArguments() {
    return EnumSet.noneOf(Argument.class);
  }

}
