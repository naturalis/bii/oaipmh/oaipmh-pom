package nl.naturalis.oaipmh.api;

import java.util.Objects;

public class DatabaseConfig {

  // Connection properties:
  private DatabaseType type;
  private String host;
  private Integer port;
  private String user;
  private String password;
  private String name;

  // Connection pool properties:
  private Integer minPoolSize = 0;
  private Integer maxPoolSize = 2;

  // Query limits
  private Integer fetchSize = 10000;

  /**
   * Returns the database type (postgreSQL, MySQL, etc). Although you <i>could</i>
   * configure this in the server configuration file (oaipmh-server.yaml), it's preferable
   * if repositories just hard code this in
   * {@link RepositoryGroup#configure(java.util.Map) RepositoryGroup.configure} c.q.
   * {@link StandAloneRepository#configure(java.util.Map) StandAloneRepository.configure}.
   * In practice the database hardly ever changes, so it would just cause clutter in the
   * configuration file.
   *
   * @return
   */
  public DatabaseType getType() {
    return type;
  }

  public void setType(DatabaseType type) {
    this.type = type;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getMinPoolSize() {
    return minPoolSize;
  }

  public void setMinPoolSize(Integer minPoolSize) {
    this.minPoolSize = minPoolSize;
  }

  public int getMaxPoolSize() {
    return maxPoolSize;
  }

  public void setMaxPoolSize(Integer maxPoolSize) {
    this.maxPoolSize = maxPoolSize;
  }

  /**
   * Returns the size of the result set chunks retrieved and loaded into application
   * memory by the JDBC driver. Too high and you'll get an OutOfMemoryError! Passed on to
   * {@code java.sql.Statement.setFetchSize}.
   */
  public Integer getFetchSize() {
    return fetchSize;
  }

  public void setFetchSize(Integer fetchSize) {
    this.fetchSize = fetchSize;
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, host, name, user, port);
  }

  /**
   * This <code>equals</code> implementation only compares the type, host, port, name and
   * user of the database, as they suffice to uniquely identify a database connection.
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    DatabaseConfig other = (DatabaseConfig) obj;
    return type == other.type && Objects.equals(host, other.host) && Objects.equals(name,
          other.name)
          && Objects.equals(user, other.user) && Objects.equals(port, other.port);
  }

}
