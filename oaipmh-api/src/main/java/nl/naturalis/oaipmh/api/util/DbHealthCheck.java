package nl.naturalis.oaipmh.api.util;

import java.sql.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import nl.naturalis.oaipmh.api.DatabaseConfig;

/**
 * Generic database health check, likely sufficient for most repositories.
 * 
 * @author Ayco Holleman
 *
 */
public class DbHealthCheck implements SimpleHealthCheck {

  private static final Logger logger = LoggerFactory.getLogger(DbHealthCheck.class);

  private final String repo;
  private final boolean healthy;
  private final String message;
  private final Throwable error;

  public DbHealthCheck(DatabaseConfig cfg, String repo) {
    logger.info("Testing database connection for " + repo);
    this.repo = repo;
    try (Connection conn = Database.withConfig(cfg).connect()) {
      // ...
    } catch (Exception e) {
      healthy = false;
      error = e;
      message = String.format("Unable to connect to %s! %s", cfg.getName(), e.getMessage());
      return;
    }
    logger.info("OK");
    healthy = true;
    message = null;
    error = null;
  }

  @Override
  public String getName() {
    return repo + " database";
  }

  @Override
  public boolean isHealthy() {
    return healthy;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public Throwable getError() {
    return error;
  }

}
