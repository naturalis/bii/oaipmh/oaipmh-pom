package nl.naturalis.oaipmh.api;

import org.klojang.check.Check;
import org.klojang.util.ArrayMethods;
import org.klojang.util.StringMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static nl.naturalis.oaipmh.api.BadResumptionTokenError.invalidNumberOfElementsInToken;
import static org.klojang.check.CommonChecks.eq;
import static org.klojang.check.CommonProperties.length;
import static org.klojang.util.CollectionMethods.implode;
import static org.klojang.util.ObjectMethods.ifNull;
import static org.klojang.util.ObjectMethods.isEmpty;

/**
 * Default implementation of {@link ResumptionTokenParser} and
 * {@link ResumptionTokenGenerator}. This implementation is probably sufficient for most
 * {@link StandAloneRepository OAI repository} implementations. It encodes the following
 * information into the resumption token:
 *
 * <ol>
 *   <li>The {@link OaiRequest#getFrom() from date}
 *   <li>The {@link OaiRequest#getUntil() until date}
 *   <li>The requested {@link OaiRequest#getSet() data set}
 *   <li>The requested {@link OaiRequest#getPage() page}
 *   <li>The {@link OaiRequest#getMetadataPrefix() metadataPrefix}
 * </ol>
 *
 * @author Ayco Holleman
 */
public class ResumptionToken implements ResumptionTokenParser, ResumptionTokenGenerator {

  private static final Logger logger = LoggerFactory.getLogger(ResumptionToken.class);

  /*
   * Delimiter between resumption token parts. All parts are converted to base-36
   * numbers, containing digits (0-9) and lowercase letters (a-z), so these
   * characters must not be used as delimiter.
   */
  private static final String PART_SEPARATOR = "A";

  private static final String DATASET_SEPARATOR = "#";

  private static final int RADIX = 36;

  private static final int FROM_PART = 0;
  private static final int UNTIL_PART = 1;
  private static final int SET_PART = 2;
  private static final int METADATA_PREFIX_PART = 3;
  private static final int PAGE_PART = 4;
  private static final int PAGE_SIZE_PART = 5;
  private static final int QUERY_ID_PART = 6;

  public ResumptionToken() { }

  @Override
  public OaiRequest parseResumptionToken(String resumptionToken)
        throws OaiProtocolException {
    OaiRequest request = new OaiRequest();
    request.setResumptionToken(resumptionToken);
    parseResumptionToken(request);
    return request;
  }

  @Override
  public void parseResumptionToken(OaiRequest request) throws OaiProtocolException {
    if (request.getResumptionToken() == null) {
      logger.debug("No resumption token in request");
      return;
    }
    logger.debug("Parsing resumption token");
    String[] slices = request.getResumptionToken().split(PART_SEPARATOR);
    Check.that(slices).has(length(), eq(), 7, invalidNumberOfElementsInToken());
    request.setFrom(decodeToInstant(slices[FROM_PART]));
    request.setUntil(decodeToInstant(slices[UNTIL_PART]));
    request.setSet(decodeToStringList(slices[SET_PART]));
    request.setMetadataPrefix(decodeToString(slices[METADATA_PREFIX_PART]));
    request.setPage(decodeToInt(slices[PAGE_PART]));
    String pageSize = decodeToString(slices[PAGE_SIZE_PART]);
    if (!pageSize.equals("0")) {
      request.addCustomParameter("__pageSize", List.of(pageSize));
    }
    String queryId = decodeToString(slices[QUERY_ID_PART]);
    if (!queryId.equals("?")) {
      request.addCustomParameter("__queryId", List.of(queryId));
    }
  }

  @Override
  public String nextResumptionToken(OaiRequest request) {
    logger.debug("Generating resumption token");
    String[] parts = new String[7];
    parts[FROM_PART] = encode(request.getFrom());
    parts[UNTIL_PART] = encode(request.getUntil());
    parts[SET_PART] = encodeStringList(request.getSet());
    parts[METADATA_PREFIX_PART] = encode(request.getMetadataPrefix());
    parts[PAGE_PART] = Integer.toString(request.getPage() + 1, RADIX);
    int pageSize = request.getPageSize();
    parts[PAGE_SIZE_PART] = encode(String.valueOf(pageSize));
    String queryId;
    if (request.isStayAlive() && request.getQueryId() != null) {
      queryId = request.getQueryId();
    } else {
      queryId = "?";
    }
    parts[QUERY_ID_PART] = encode(queryId);
    return ArrayMethods.implode(parts, PART_SEPARATOR);
  }

  private static String encode(String s) {
    if (!StringMethods.isBlank(s)) {
      byte[] bytes = s.getBytes(StandardCharsets.UTF_8);
      return new BigInteger(bytes).toString(RADIX);
    }
    return StringMethods.EMPTY_STRING;
  }

  private static String encodeStringList(List<String> s) {
    if (!isEmpty(s)) {
      return encode(implode(s, DATASET_SEPARATOR));
    }
    return StringMethods.EMPTY_STRING;
  }

  private static String encode(Instant instant) {
    if (instant != null) {
      return Long.toString(instant.getEpochSecond(), RADIX);
    }
    return StringMethods.EMPTY_STRING;
  }

  private static String decodeToString(String s) {
    if (!isEmpty(s)) {
      BigInteger bi = new BigInteger(s, RADIX);
      return new String(bi.toByteArray(), StandardCharsets.UTF_8);
    }
    return null;
  }

  private static List<String> decodeToStringList(String s) {
    if (!isEmpty(s)) {
      BigInteger bi = new BigInteger(s, RADIX);
      String decoded = new String(bi.toByteArray(), StandardCharsets.UTF_8);
      return Arrays.asList(decoded.split(DATASET_SEPARATOR));
    }
    return null;
  }

  private static Instant decodeToInstant(String s) throws OaiProtocolException {
    if (!isEmpty(s)) {
      try {
        return Instant.ofEpochSecond(Long.parseLong(s, RADIX));
      } catch (Exception e) {
        String fmt = "Failed to extract date from resumption token (\"%s\"): %s";
        throw BadResumptionTokenError.because(String.format(fmt, s, e));
      }
    }
    return null;
  }

  private static int decodeToInt(String s) throws OaiProtocolException {
    if (!isEmpty(s)) {
      try {
        return Integer.parseInt(s, RADIX);
      } catch (Exception e) {
        String fmt = "Failed to extract number from resumption token (\"%s\"): %s";
        throw BadResumptionTokenError.because(String.format(fmt, s, e));
      }
    }
    return 0;
  }

}
