/**
 * Provides an API that OAI repositories can implement to have their data exposed by the Naturalis OAI-PMH server. Besides providing an API,
 * this library comes with useful tools for OAI repository development, e.g.:
 * <ul>
 * <li>ready-made JAXB materializations of the ABCD, GGBN and OAI_DC schemas
 * <li>utilities for checking the validaty of of an OAI-PMH request, for converting to and from OAIPMH-compliant date formats, etc.
 * <li>database connection pooling
 * </ul>
 * 
 * @author Ayco Holleman
 *
 */
package nl.naturalis.oaipmh.api;
