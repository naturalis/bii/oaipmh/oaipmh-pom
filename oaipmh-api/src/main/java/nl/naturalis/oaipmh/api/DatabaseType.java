package nl.naturalis.oaipmh.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.klojang.check.Check;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

/**
 * Symbolic constants for the various databases.
 *
 * @author Ayco Holleman
 */
public enum DatabaseType {

  MARIADB("MariaDB", "jdbc:mariadb://{host}:{port}/{db}", 3306),
  MYSQL("MySQL", null, 0),
  ORACLE("Oracle", "jdbc:oracle:thin:@{host}:{port}:{db}", 1523),
  POSTGRES("PostgreSQL", "jdbc:postgresql://{host}:{port}/{db}", 5432),
  SQL_SERVER("SQL Server", null, 0);

  private static final String NOT_SUPPORTED = "Database not supported: ";

  @JsonCreator
  public static DatabaseType parse(String name) {
    Check.notNull(name, "name");
    String s = name.replaceAll("[_-]", "").toLowerCase();
    switch (s) {
      case "mysql":
        return MYSQL;
      case "mariadb":
        return MARIADB;
      case "postgres":
      case "postgresql":
        return POSTGRES;
      case "oracle":
        return ORACLE;
      case "sqlserver":
      case "mssqlserver":
        return SQL_SERVER;
    }
    String msg = String.format("%s\"%s\". Supported databases: %s",
        NOT_SUPPORTED,
        name,
        getNames());
    throw new IllegalArgumentException(msg);
  }

  private static String getNames() {
    return Arrays.stream(values()).map(DatabaseType::getName).collect(joining(", "));
  }

  private final String name;
  private final String url;
  private final int port;

  DatabaseType(String name, String dsn, int port) {
    this.name = name;
    this.url = dsn;
    this.port = port;
  }

  public String getName() {
    return name;
  }

  public String getJdbcUrlTemplate() {
    if (url == null) {
      throw new OaiRuntimeException(NOT_SUPPORTED + name);
    }
    return url;
  }

  public int getDefaultPort() {
    if (port == 0) {
      throw new OaiRuntimeException(NOT_SUPPORTED + name);
    }
    return port;
  }
}
