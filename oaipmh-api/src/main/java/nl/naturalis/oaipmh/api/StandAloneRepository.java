package nl.naturalis.oaipmh.api;

/**
 * An extension of {@link OaiRepository} that specifies the extra capacities an OAI
 * repository needs to be plugged in directly into the OAIPMH server, rather than as part
 * of a {@link RepositoryGroup}.
 *
 * @author Ayco Holleman
 */
public interface StandAloneRepository extends ManagedRepository, OaiRepository {

}
