package nl.naturalis.oaipmh.api.util;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.PropertyException;
import nl.naturalis.oaipmh.api.OaiRepository;
import nl.naturalis.oaipmh.api.OaiRuntimeException;
import org.klojang.check.Check;
import org.openarchives.oai._2.OAIPMHtype;
import org.openarchives.oai._2_0.oai_dc.OaiDcType;
import org.purl.dc.elements._1.ElementType;

import java.io.OutputStream;
import java.util.*;

import static jakarta.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static jakarta.xml.bind.Marshaller.JAXB_SCHEMA_LOCATION;
import static java.util.stream.Collectors.joining;

/**
 * Helps OAI repositories to stream the OAI-PMH they generate. This will often come down
 * to marshalling JAXB objects to the provided output stream, and that requires some
 * non-trivial preparations, which can be carried out using this class. This class is not
 * thread-safe and needs to be externally synchronized.
 *
 * @author Ayco Holleman
 */
public final class OaiWriter {

  private static final List<String> CORE_JAXB_PACKAGES = Arrays.asList(
        OAIPMHtype.class.getPackageName(),
        OaiDcType.class.getPackageName(),
        ElementType.class.getPackageName());

  private static final String OAI_NAMESPACE = "http://www.openarchives.org/OAI/2.0/";
  private static final String OAI_SCHEMA = "http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd";

  private static final String OAI_DC_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/";
  private static final String OAI_DC_SCHEMA = "http://www.openarchives.org/OAI/2.0/oai_dc.xsd";

  private final Set<String> jaxbPackages = new LinkedHashSet<>(4);
  private final Map<String, String> schemas = new LinkedHashMap<>(4);

  /*
   * It's not clear how expensive it is to create a marshaller, so we try to make it survive a single write action.
   */
  private Marshaller marshaller;

  public OaiWriter() {
    jaxbPackages.addAll(CORE_JAXB_PACKAGES);
    schemas.put(OAI_NAMESPACE, OAI_SCHEMA);
  }

  /**
   * Restores the writer to its default state, as if the no-arg constructor was just
   * called. So no extra JAXB packages will be in the JAXB context
   */
  public void reset() {
    jaxbPackages.clear();
    jaxbPackages.addAll(CORE_JAXB_PACKAGES);
    schemas.clear();
    schemas.put(OAI_NAMESPACE, OAI_SCHEMA);
    marshaller = null;
  }

  /**
   * Adds a package containing xjc-generated (JAXB) classes. The following packages are
   * automatically added:
   * <ol>
   * <li>org.openarchives.oai._2 (OAI-PMH schema)
   * <li>org.openarchives.oai._2_0.oai_dc (oai_dc container)
   * <li>org.purl.dc.elements._1 (DarwinCore)
   * </ol>
   * If you implement an {@link OaiRepository} using additional XML schemas and materialize them using JAXB, you must add the extra
   * xjc-generated packages used to generate the final OAI-PMH output.
   *
   * @param pkg
   */
  public void addJaxbPackage(String pkg) {
    Check.notNull(pkg, "pkg");
    if (jaxbPackages.add(pkg)) {
      marshaller = null;
    }
  }

  /**
   * Expands the {@code xsi:schemaLocation} attribute of the &lt;OAI-PMH&gt; root element
   * with the specified namespace-location pair. The schema location of the XSD for
   * OAI-PMH is automatically added to the root element. Therefore a plain
   * {@code OaiWriter} is sufficient to write a valid
   * {@link OaiUtil#createOAIPMHSkeleton(nl.naturalis.oaipmh.api.OaiRequest) OAI-PMH
   * skeleton} and, more importantly, to write OAI-PMH that only reports an error. The XSD
   * location for oai_dc, though common, is not automatically added since you might be
   * serving a request for a different metadata format. However, you can simply add the
   * XSD location for oai_dc by calling {@link #addOaiDcSchemaLocation()}.
   *
   * @param xmlNamespace The XML namespace
   * @param schemaLocation The XSD location
   */
  public void setSchemaLocation(String xmlNamespace, String schemaLocation) {
    Check.notNull(xmlNamespace, "xmlNamespace");
    Check.notNull(schemaLocation, "schemaLocation");
    String s = schemas.get(xmlNamespace);
    if (s == null || !s.equals(schemaLocation)) {
      schemas.put(xmlNamespace, schemaLocation);
      if (marshaller != null) {
        String schemaLocations = schemas.entrySet().stream().map(e -> e.getKey()
              + ' '
              + e.getValue()).collect(joining(" "));
        try {
          marshaller.setProperty(JAXB_SCHEMA_LOCATION, schemaLocations);
        } catch (PropertyException exc) {
          throw new OaiRuntimeException(exc);
        }
      }
    }
  }

  /**
   * Adds the XSD location of the oai_dc namespace. See
   * {@link #setSchemaLocation(String, String)}.
   */
  public void addOaiDcSchemaLocation() {
    setSchemaLocation(OAI_DC_NAMESPACE, OAI_DC_SCHEMA);
  }

  /*
   * Writes OAI-PMH (set via {@link #setOAIPMH(OAIPMHtype) setRootElement}) to the
   * specified output stream.
   *
   * @return
   * @throws JAXBException
   */
  public void write(OAIPMHtype oaipmh, OutputStream out) {
    write(oaipmh, out, false);
  }

  /*
   * Writes OAI-PMH (set via {@link #setOAIPMH(OAIPMHtype) setRootElement}) to the
   * specified output stream.
   *
   * @return
   * @throws JAXBException
   */
  public void write(OAIPMHtype oaipmh, OutputStream out, boolean pretty) {
    try {
      if (marshaller == null) {
        JAXBContext ctx = JAXBContext.newInstance(jaxbPackages.stream()
              .collect(joining(":")));
        String schemaLocations = schemas.entrySet()
              .stream()
              .map(e -> e.getKey() + ' ' + e.getValue())
              .collect(joining(" "));
        marshaller = ctx.createMarshaller();
        marshaller.setProperty(JAXB_SCHEMA_LOCATION, schemaLocations);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
      }
      marshaller.setProperty(JAXB_FORMATTED_OUTPUT, pretty);
      marshaller.marshal(oaipmh, out);
    } catch (JAXBException e) {
      throw new OaiRuntimeException(e);
    }
  }

}
