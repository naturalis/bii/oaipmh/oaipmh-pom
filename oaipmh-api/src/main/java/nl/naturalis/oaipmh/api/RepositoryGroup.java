package nl.naturalis.oaipmh.api;

import java.util.List;
import java.util.Optional;

/**
 * Represents a group of OAI repositories that share a single configuration object and, most likely, one or more resources like a database
 * connection.
 *
 * @author Ayco Holleman
 */
public interface RepositoryGroup extends ManagedRepository {

  /**
   * Returns the names of the repositories in this {@code RepositoryGroup}.
   * 
   * @return
   */
  List<String> getRepositoryNames();

  /**
   * Returns an <code>Optional</code> containing the OAI repository identified by the provided name or an empty <code>Optional</code> if the
   * repository group does not contain a repository with that name.
   * 
   * @param <U>
   * @param name
   * @return
   */
  Optional<OaiRepository> getRepository(String name);

}
