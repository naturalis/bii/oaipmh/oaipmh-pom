package nl.naturalis.oaipmh.api.util;

import com.google.common.util.concurrent.MoreExecutors;
import org.klojang.check.Check;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.klojang.check.CommonChecks.NULL;
import static org.klojang.check.CommonChecks.notNull;
import static org.klojang.check.CommonExceptions.STATE;

/**
 * Provides access to a thread pool that all OAI repositories <i>should</i> use if they
 * require multi-threaded operations. They should not create their own thread pool
 * (because that would be selfish) and they also should not call
 * {@link SharedThreadPool#start(int) start} or {@link SharedThreadPool#stop() stop}
 * themselves. The OAIPMH server will start the thread pool during startup and stop it
 * during shutdown. In between repositories can call
 * {@link SharedThreadPool#getPool() getPool} any number of times.
 *
 * @author Ayco Holleman
 */
public final class SharedThreadPool {

  private static final Logger logger = LoggerFactory.getLogger(SharedThreadPool.class);

  private static SharedThreadPool instance;

  /**
   * Starts the thread pool. Should not be called by repository implementations.
   *
   * @param size
   */
  public static void start(int size) {
    instance = new SharedThreadPool(size);
  }

  /**
   * Stops the thread pool. Should not be called by repository implementations.
   */
  public static void stop() {
    instance.pool.shutdownNow();
  }

  /**
   * Provides access to the thread pool.
   *
   * @return
   */
  public static ExecutorService getPool() {
    Check.on(STATE, instance).is(notNull(), "Thread pool not created yet");
    return instance.pool;
  }

  private final ExecutorService pool;

  private SharedThreadPool(int size) {
    Check.on(STATE, instance).is(NULL(), "Thread pool already created");
    if (size == 0) {
      logger.warn("Multi-threading disabled");
      pool = MoreExecutors.newDirectExecutorService();
    } else {
      logger.info("Creating thread pool");
      pool = Executors.newFixedThreadPool(size);
      logger.info("Thread pool ready");
    }
  }

}
