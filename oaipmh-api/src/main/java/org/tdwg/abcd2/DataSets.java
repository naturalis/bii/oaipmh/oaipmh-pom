//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.02.07 at 09:07:12 PM CET 
//


package org.tdwg.abcd2;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DataSet" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GUID" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
 *                   &lt;element name="ID" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
 *                   &lt;element name="ResourceURIs" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence maxOccurs="unbounded"&gt;
 *                             &lt;element name="ResourceURI" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="TechnicalContacts" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="TechnicalContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ContentContacts"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="ContentContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="DataCenter" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
 *                   &lt;element name="OtherProviders" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="OtherProvider" type="{http://www.tdwg.org/schemas/abcd/3.0}String" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="Metadata" type="{http://www.tdwg.org/schemas/abcd/3.0}ContentMetadata"/&gt;
 *                   &lt;element name="Units"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="Unit" type="{http://www.tdwg.org/schemas/abcd/3.0}Unit" maxOccurs="unbounded"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="DataSetExtension" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataSet"
})
@XmlRootElement(name = "DataSets")
public class DataSets {

    @XmlElement(name = "DataSet", required = true)
    protected List<DataSets.DataSet> dataSet;

    /**
     * Gets the value of the dataSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DataSets.DataSet }
     * 
     * 
     */
    public List<DataSets.DataSet> getDataSet() {
        if (dataSet == null) {
            dataSet = new ArrayList<DataSets.DataSet>();
        }
        return this.dataSet;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GUID" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
     *         &lt;element name="ID" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
     *         &lt;element name="ResourceURIs" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence maxOccurs="unbounded"&gt;
     *                   &lt;element name="ResourceURI" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="TechnicalContacts" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="TechnicalContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ContentContacts"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="ContentContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="DataCenter" type="{http://www.tdwg.org/schemas/abcd/3.0}String" minOccurs="0"/&gt;
     *         &lt;element name="OtherProviders" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="OtherProvider" type="{http://www.tdwg.org/schemas/abcd/3.0}String" maxOccurs="unbounded"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="Metadata" type="{http://www.tdwg.org/schemas/abcd/3.0}ContentMetadata"/&gt;
     *         &lt;element name="Units"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="Unit" type="{http://www.tdwg.org/schemas/abcd/3.0}Unit" maxOccurs="unbounded"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="DataSetExtension" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "guid",
        "id",
        "resourceURIs",
        "technicalContacts",
        "contentContacts",
        "dataCenter",
        "otherProviders",
        "metadata",
        "units",
        "dataSetExtension"
    })
    public static class DataSet {

        @XmlElement(name = "GUID")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String guid;
        @XmlElement(name = "ID")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String id;
        @XmlElement(name = "ResourceURIs")
        protected DataSets.DataSet.ResourceURIs resourceURIs;
        @XmlElement(name = "TechnicalContacts")
        protected DataSets.DataSet.TechnicalContacts technicalContacts;
        @XmlElement(name = "ContentContacts", required = true)
        protected DataSets.DataSet.ContentContacts contentContacts;
        @XmlElement(name = "DataCenter")
        @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
        @XmlSchemaType(name = "normalizedString")
        protected String dataCenter;
        @XmlElement(name = "OtherProviders")
        protected DataSets.DataSet.OtherProviders otherProviders;
        @XmlElement(name = "Metadata", required = true)
        protected ContentMetadata metadata;
        @XmlElement(name = "Units", required = true)
        protected DataSets.DataSet.Units units;
        @XmlElement(name = "DataSetExtension")
        protected Object dataSetExtension;

        /**
         * Gets the value of the guid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGUID() {
            return guid;
        }

        /**
         * Sets the value of the guid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGUID(String value) {
            this.guid = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getID() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setID(String value) {
            this.id = value;
        }

        /**
         * Gets the value of the resourceURIs property.
         * 
         * @return
         *     possible object is
         *     {@link DataSets.DataSet.ResourceURIs }
         *     
         */
        public DataSets.DataSet.ResourceURIs getResourceURIs() {
            return resourceURIs;
        }

        /**
         * Sets the value of the resourceURIs property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataSets.DataSet.ResourceURIs }
         *     
         */
        public void setResourceURIs(DataSets.DataSet.ResourceURIs value) {
            this.resourceURIs = value;
        }

        /**
         * Gets the value of the technicalContacts property.
         * 
         * @return
         *     possible object is
         *     {@link DataSets.DataSet.TechnicalContacts }
         *     
         */
        public DataSets.DataSet.TechnicalContacts getTechnicalContacts() {
            return technicalContacts;
        }

        /**
         * Sets the value of the technicalContacts property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataSets.DataSet.TechnicalContacts }
         *     
         */
        public void setTechnicalContacts(DataSets.DataSet.TechnicalContacts value) {
            this.technicalContacts = value;
        }

        /**
         * Gets the value of the contentContacts property.
         * 
         * @return
         *     possible object is
         *     {@link DataSets.DataSet.ContentContacts }
         *     
         */
        public DataSets.DataSet.ContentContacts getContentContacts() {
            return contentContacts;
        }

        /**
         * Sets the value of the contentContacts property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataSets.DataSet.ContentContacts }
         *     
         */
        public void setContentContacts(DataSets.DataSet.ContentContacts value) {
            this.contentContacts = value;
        }

        /**
         * Gets the value of the dataCenter property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDataCenter() {
            return dataCenter;
        }

        /**
         * Sets the value of the dataCenter property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDataCenter(String value) {
            this.dataCenter = value;
        }

        /**
         * Gets the value of the otherProviders property.
         * 
         * @return
         *     possible object is
         *     {@link DataSets.DataSet.OtherProviders }
         *     
         */
        public DataSets.DataSet.OtherProviders getOtherProviders() {
            return otherProviders;
        }

        /**
         * Sets the value of the otherProviders property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataSets.DataSet.OtherProviders }
         *     
         */
        public void setOtherProviders(DataSets.DataSet.OtherProviders value) {
            this.otherProviders = value;
        }

        /**
         * Gets the value of the metadata property.
         * 
         * @return
         *     possible object is
         *     {@link ContentMetadata }
         *     
         */
        public ContentMetadata getMetadata() {
            return metadata;
        }

        /**
         * Sets the value of the metadata property.
         * 
         * @param value
         *     allowed object is
         *     {@link ContentMetadata }
         *     
         */
        public void setMetadata(ContentMetadata value) {
            this.metadata = value;
        }

        /**
         * Gets the value of the units property.
         * 
         * @return
         *     possible object is
         *     {@link DataSets.DataSet.Units }
         *     
         */
        public DataSets.DataSet.Units getUnits() {
            return units;
        }

        /**
         * Sets the value of the units property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataSets.DataSet.Units }
         *     
         */
        public void setUnits(DataSets.DataSet.Units value) {
            this.units = value;
        }

        /**
         * Gets the value of the dataSetExtension property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDataSetExtension() {
            return dataSetExtension;
        }

        /**
         * Sets the value of the dataSetExtension property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDataSetExtension(Object value) {
            this.dataSetExtension = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="ContentContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contentContact"
        })
        public static class ContentContacts {

            @XmlElement(name = "ContentContact", required = true)
            protected List<ContactP> contentContact;

            /**
             * Gets the value of the contentContact property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contentContact property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContentContact().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ContactP }
             * 
             * 
             */
            public List<ContactP> getContentContact() {
                if (contentContact == null) {
                    contentContact = new ArrayList<ContactP>();
                }
                return this.contentContact;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="OtherProvider" type="{http://www.tdwg.org/schemas/abcd/3.0}String" maxOccurs="unbounded"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "otherProvider"
        })
        public static class OtherProviders {

            @XmlElement(name = "OtherProvider", required = true)
            @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
            @XmlSchemaType(name = "normalizedString")
            protected List<String> otherProvider;

            /**
             * Gets the value of the otherProvider property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the otherProvider property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOtherProvider().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getOtherProvider() {
                if (otherProvider == null) {
                    otherProvider = new ArrayList<String>();
                }
                return this.otherProvider;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence maxOccurs="unbounded"&gt;
         *         &lt;element name="ResourceURI" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "resourceURI"
        })
        public static class ResourceURIs {

            @XmlElement(name = "ResourceURI")
            @XmlSchemaType(name = "anyURI")
            protected List<String> resourceURI;

            /**
             * Gets the value of the resourceURI property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the resourceURI property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getResourceURI().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getResourceURI() {
                if (resourceURI == null) {
                    resourceURI = new ArrayList<String>();
                }
                return this.resourceURI;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="TechnicalContact" type="{http://www.tdwg.org/schemas/abcd/3.0}ContactP" maxOccurs="unbounded"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "technicalContact"
        })
        public static class TechnicalContacts {

            @XmlElement(name = "TechnicalContact", required = true)
            protected List<ContactP> technicalContact;

            /**
             * Gets the value of the technicalContact property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the technicalContact property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTechnicalContact().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ContactP }
             * 
             * 
             */
            public List<ContactP> getTechnicalContact() {
                if (technicalContact == null) {
                    technicalContact = new ArrayList<ContactP>();
                }
                return this.technicalContact;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="Unit" type="{http://www.tdwg.org/schemas/abcd/3.0}Unit" maxOccurs="unbounded"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "unit"
        })
        public static class Units {

            @XmlElement(name = "Unit", required = true)
            protected List<Unit> unit;

            /**
             * Gets the value of the unit property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the unit property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getUnit().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Unit }
             * 
             * 
             */
            public List<Unit> getUnit() {
                if (unit == null) {
                    unit = new ArrayList<Unit>();
                }
                return this.unit;
            }

        }

    }

}
