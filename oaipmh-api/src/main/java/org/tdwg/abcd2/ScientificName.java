//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.02.07 at 09:07:12 PM CET 
//


package org.tdwg.abcd2;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.NormalizedStringAdapter;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Name of the taxon identified, formed according to the different Codes of Nomenclature which apply to scientific names.
 * 
 * <p>Java class for ScientificName complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScientificName"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FullScientificName" type="{http://www.tdwg.org/schemas/abcd/3.0}String"/&gt;
 *         &lt;element name="NameAtomized" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="Bacterial" type="{http://www.tdwg.org/schemas/abcd/3.0}NameBacterial" minOccurs="0"/&gt;
 *                   &lt;element name="Botanical" type="{http://www.tdwg.org/schemas/abcd/3.0}NameBotanical" minOccurs="0"/&gt;
 *                   &lt;element name="Zoological" type="{http://www.tdwg.org/schemas/abcd/3.0}NameZoological" minOccurs="0"/&gt;
 *                   &lt;element name="Viral" type="{http://www.tdwg.org/schemas/abcd/3.0}NameViral" minOccurs="0"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScientificName", propOrder = {
    "fullScientificName",
    "nameAtomized"
})
@XmlSeeAlso({
    ScientificNameIdentified.class
})
public class ScientificName {

    @XmlElement(name = "FullScientificName", required = true)
    @XmlJavaTypeAdapter(NormalizedStringAdapter.class)
    @XmlSchemaType(name = "normalizedString")
    protected String fullScientificName;
    @XmlElement(name = "NameAtomized")
    protected ScientificName.NameAtomized nameAtomized;

    /**
     * Gets the value of the fullScientificName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullScientificName() {
        return fullScientificName;
    }

    /**
     * Sets the value of the fullScientificName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullScientificName(String value) {
        this.fullScientificName = value;
    }

    /**
     * Gets the value of the nameAtomized property.
     * 
     * @return
     *     possible object is
     *     {@link ScientificName.NameAtomized }
     *     
     */
    public ScientificName.NameAtomized getNameAtomized() {
        return nameAtomized;
    }

    /**
     * Sets the value of the nameAtomized property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScientificName.NameAtomized }
     *     
     */
    public void setNameAtomized(ScientificName.NameAtomized value) {
        this.nameAtomized = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="Bacterial" type="{http://www.tdwg.org/schemas/abcd/3.0}NameBacterial" minOccurs="0"/&gt;
     *         &lt;element name="Botanical" type="{http://www.tdwg.org/schemas/abcd/3.0}NameBotanical" minOccurs="0"/&gt;
     *         &lt;element name="Zoological" type="{http://www.tdwg.org/schemas/abcd/3.0}NameZoological" minOccurs="0"/&gt;
     *         &lt;element name="Viral" type="{http://www.tdwg.org/schemas/abcd/3.0}NameViral" minOccurs="0"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "bacterial",
        "botanical",
        "zoological",
        "viral"
    })
    public static class NameAtomized {

        @XmlElement(name = "Bacterial")
        protected NameBacterial bacterial;
        @XmlElement(name = "Botanical")
        protected NameBotanical botanical;
        @XmlElement(name = "Zoological")
        protected NameZoological zoological;
        @XmlElement(name = "Viral")
        protected NameViral viral;

        /**
         * Gets the value of the bacterial property.
         * 
         * @return
         *     possible object is
         *     {@link NameBacterial }
         *     
         */
        public NameBacterial getBacterial() {
            return bacterial;
        }

        /**
         * Sets the value of the bacterial property.
         * 
         * @param value
         *     allowed object is
         *     {@link NameBacterial }
         *     
         */
        public void setBacterial(NameBacterial value) {
            this.bacterial = value;
        }

        /**
         * Gets the value of the botanical property.
         * 
         * @return
         *     possible object is
         *     {@link NameBotanical }
         *     
         */
        public NameBotanical getBotanical() {
            return botanical;
        }

        /**
         * Sets the value of the botanical property.
         * 
         * @param value
         *     allowed object is
         *     {@link NameBotanical }
         *     
         */
        public void setBotanical(NameBotanical value) {
            this.botanical = value;
        }

        /**
         * Gets the value of the zoological property.
         * 
         * @return
         *     possible object is
         *     {@link NameZoological }
         *     
         */
        public NameZoological getZoological() {
            return zoological;
        }

        /**
         * Sets the value of the zoological property.
         * 
         * @param value
         *     allowed object is
         *     {@link NameZoological }
         *     
         */
        public void setZoological(NameZoological value) {
            this.zoological = value;
        }

        /**
         * Gets the value of the viral property.
         * 
         * @return
         *     possible object is
         *     {@link NameViral }
         *     
         */
        public NameViral getViral() {
            return viral;
        }

        /**
         * Sets the value of the viral property.
         * 
         * @param value
         *     allowed object is
         *     {@link NameViral }
         *     
         */
        public void setViral(NameViral value) {
            this.viral = value;
        }

    }

}
