package nl.naturalis.oaipmh.api;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ResumptionTokenTest {
  
  @Test
  public void test01() {
    String[] ss = new String[5];
    ss[0]="Hello";
    ss[1]="";
    ss[2]="";
    ss[3]="World";
    ss[4]="";
    String s = Arrays.stream(ss).collect(Collectors.joining(":"));
    assertEquals("Hello:::World:",s);
  }

  @Test
  public void testComposeAndDecompose() throws OaiProtocolException {
    OaiRequest request = new OaiRequest();
    request.setFrom(Instant.ofEpochSecond(1000000L));
    request.setUntil(Instant.ofEpochSecond(2000000L));
    request.setPage(4);
    request.setSet(List.of("FOO", "BAR"));
    request.setMetadataPrefix("oai_dc");
    String token = new ResumptionToken().nextResumptionToken(request);
    OaiRequest request2 = new ResumptionToken().parseResumptionToken(token);
    assertEquals("01", request.getFrom(), request2.getFrom());
    assertEquals("02", request.getUntil(), request2.getUntil());
    assertEquals("03", request.getSet(), request2.getSet());
    assertEquals("04", request.getMetadataPrefix(), request2.getMetadataPrefix());
    assertEquals("05", request.getPage() + 1, request2.getPage());
    assertEquals(token, request2.getResumptionToken());
  }

}
