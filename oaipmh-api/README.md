# oaipmh-api

A Java API and tool set for creating OAI repositories. An OAI repository is created
by implementing either the OaiRepository interface or the OaiRepositoryGroup
interface within the API. You would choose the OaiRepositoryGroup interface when
implementing closely related OAI repositories that share a single configuration
and/or heavy-weight resources like a database connection pool.
The [geneious component](../oaipmh-geneious/README.md) is implemented as an
OaiRepositoryGroup since it serves multiple objects from the same database (
specimens, DNA extracts and DNA sample plates).
The [medialib component](../oaipmh-medialib/README.md) is implemented as an
OaiRepository.

Note that the oaipmh-api project **is not** a frontend API or REST interface.
The [oaipmh-server](../oaipmh-server/README.md) library takes care of exposing the
XML produced by the OAI repositories through a REST interface. Instead, the
oaipmh-api project provides a backend API (service provider interface) that
interested parties can implement in order to get exposed via the OAI-PMH server.

The oaipmh-api also provides tools for the generation OAI-PMH XML:

- It provides repository implementations with an OAIPMH-skeleton - the outermost (
  repository-independent) XML elements of the OAI-PMH.
- It provides JAXB classes for various required or ubiquitous XML schemas, so you can
  relatively easily stitch together the OAI-PMH output.
- OAI repositories pulling their data from a database can easily be configured to use
  a JDBC connection pool

