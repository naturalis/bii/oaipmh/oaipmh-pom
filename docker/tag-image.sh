#!/bin/bash

echo 'Tagging Docker image'

IMAGE_URL_BASE="${CI_REGISTRY}/naturalis/bii/oaipmh/oaipmh-server"
image="${IMAGE_URL_BASE}:${CI_COMMIT_SHA}"

docker pull "${image}"

if [[ -n ${CI_COMMIT_TAG+x} ]]; then
  echo "===> Adding image tag \"${CI_COMMIT_TAG}\""
  docker tag "${image}" "${IMAGE_URL_BASE}:${CI_COMMIT_TAG}"
  docker push "${IMAGE_URL_BASE}:${CI_COMMIT_TAG}"
  exit 0
fi

echo "===> Adding image tag \"${CI_COMMIT_REF_SLUG}\""
docker tag "${image}" "${IMAGE_URL_BASE}:${CI_COMMIT_REF_SLUG}"
docker push "${IMAGE_URL_BASE}:${CI_COMMIT_REF_SLUG}"

if [[ "${CI_COMMIT_REF_SLUG}" = "main" ]]; then
  echo "===> Adding image tag \"latest\""
  docker tag "${image}" "${IMAGE_URL_BASE}:latest"
  docker push "${IMAGE_URL_BASE}:latest"
fi
