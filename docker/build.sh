#!/bin/bash

set -e

############################################################
# HACK - delete as soon as possible! When klojang-jdbc 1.1.4
# is stable and on Maven Central
############################################################
rm -rf '/home/ayco/git-repos/naturalis/oaipmh/oaipmh-pom/docker/maven-repo/org/klojang'
cp -R '/home/ayco/.m2/repository/org/klojang' '/home/ayco/git-repos/naturalis/oaipmh/oaipmh-pom/docker/maven-repo/org'
# END HACK

# Executes a local build. This script is not part of the Gitlab
# pipeline, but rather mimics its Java and Docker build phases.

# Are we running inside a Gitlab pipeline?
GITLAB_PIPELINE=true
[[ -z ${CI_JOB_ID+x} ]] && GITLAB_PIPELINE=false
export GITLAB_PIPELINE

if [[ ${GITLAB_PIPELINE} == true ]]; then
  BASE_DIR="${CI_PROJECT_DIR}"
else
  BASE_DIR=$(dirname "${0}")/../
  BASE_DIR=$(realpath "${BASE_DIR}")
fi
export BASE_DIR

if [[ ${GITLAB_PIPELINE} == false ]]; then
  f="${BASE_DIR}/docker/build.env"
  if [[ ! -f "${f}" ]]; then
    echo 'Missing file docker/build.env'
    echo 'docker/build.env'
    exit 1
  fi
  echo 'Setting up environment for locally executed build ...'
  source "${f}"
  export CI_COMMIT_REF_SLUG="$(git rev-parse --abbrev-ref HEAD)"
  export CI_COMMIT_SHA="$(git log -n 1 --pretty=format:'%H')"
  export CI_COMMIT_SHORT_SHA="$(git log -n 1 --pretty=format:'%h')"
fi

source "${BASE_DIR}/docker/mvn-install.sh"
source "${BASE_DIR}/docker/create-version-file.sh"

echo '===> Executing docker login'
echo -n ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY} &> /dev/null

source "${BASE_DIR}/docker/create-image.sh"
source "${BASE_DIR}/docker/tag-image.sh"
