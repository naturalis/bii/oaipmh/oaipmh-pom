#!/bin/bash

# Generates version.properties inside docker/dependencies.
# This file is read by the application in order to display
# version-related info on the home page.

basedir=$(dirname "${0}")/../
basedir=$(realpath "${basedir}")

version_file="${basedir}/docker/dependencies/version.properties"

rm -f ${version_file}

branch="${CI_COMMIT_BRANCH}"
if [[ -z "${branch}" ]]; then
  branch="${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
  if [[ -z "${branch}" ]]; then
    branch="$(git branch --show-current)"
  fi
fi

echo "gitTag=$(git describe --abbrev=0)"                >> "${version_file}"
echo "gitBranch=${branch}"                              >> "${version_file}"
echo "gitCommit=$(git log -n 1 --pretty=format:'%h')"   >> "${version_file}"
echo "buildDate=$(date '+%Y-%m-%d %H:%M')"              >> "${version_file}"