#!/bin/bash

echo "Creating/pushing Docker image for commit ${CI_COMMIT_SHORT_SHA}"

IMAGE_URL_BASE="${CI_REGISTRY}/naturalis/bii/oaipmh/oaipmh-server"
image_url="${IMAGE_URL_BASE}:${CI_COMMIT_SHA}"
echo "===> Image URL: ${image_url}"

echo '===> Deleting previous version of image (if present)'
docker image rm -f ${image_url}

echo '===> Creating image'
docker build \
      --no-cache \
      --pull \
      --tag "${image_url}" \
      -f "${BASE_DIR}/docker/Dockerfile" "${BASE_DIR}/docker"

echo '===> Pushing image'
docker push "${image_url}"

echo '===> Image created and pushed'